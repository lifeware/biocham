:- module(
  molecules,
  [
    % Commands
    list_molecules/0,
    list_locations/0,
    list_neighborhood/0,
    draw_neighborhood/0,
    % Public API
    list_locations/1,
    enumerate_molecules/1,
    enumerate_molecules/2
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(objects).


:- devdoc('\\section{Commands}').

:- devdoc('There is something completely wrong with list_molecules command: it is defined with enumerate_all_molecules defined in nusmv.pl for including molecules from CTL queries ! Molecules are printed with meaningless indices in a format reminiscent to ODE rather than CRN. Furthermore enumerate_molecules created repetitions (using union instead of list_to_set), this is corrected but should be reworked.').



list_molecules :-
  biocham_command,
  doc('lists all the molecules of the current model.'),
  enumerate_all_molecules(L),
  write_numbered(L).


:- doc('When a molecule is written as compound@location, it represents the given
\\emph{compound} as located inside the compartment \\emph{location}.  In this
case for the numerical simulation, the concentration of the species is defined
as the number of molecules divided by the volume of the location. This volume
may be defined through a \emph{parameter} or a \emph{function}, by default it is
set to 1.').


list_locations(Locations) :-
  enumerate_all_molecules(L),
  maplist(get_location, L, Locs),
  sort(Locs, Locations).


list_locations :-
  biocham_command,
  doc('lists all the locations of the current model.'),
  list_locations(Locations),
  write_numbered(Locations).


list_neighborhood :-
  biocham_command,
  doc('lists all neighborhood relationships inferred from the model.'),
  compute_neighborhood,
  listing(molecules:neighbor/2).


draw_neighborhood :-
  biocham_command,
  doc('draws the graph of all neighborhood relationships inferred from the
    model.'),
  compute_neighborhood,
  new_graph,
  set_graph_name(neighborhood_graph),
  get_current_graph(GraphId),
  forall(
    neighbor(X, Y),
    (
      add_vertex(GraphId, X, XId),
      add_vertex(GraphId, Y, YId),
      add_edge(GraphId, XId, YId, EdgeId),
      set_attribute(EdgeId, dir = 'none')
    )
  ),
  draw_graph(GraphId).


:- devdoc('\\section{Public API}').

enumerate_molecules(Molecules) :-
  single_model(ModelId),
  enumerate_molecules(ModelId, Molecules).

enumerate_molecules(ModelId, Molecules) :-
  % TODO: Rely on the item mechanism when it will handle species
  findall(
    Molecule,
    identifier_kind(ModelId, Molecule, object),
    Mols
  ),
  % keeps order!!!
  list_to_set(Mols, Molecules).


:- devdoc('\\section{Private predicates}').


write_numbered(L) :-
  b_setval(write_numbered_counter, 0),
  write_numbered_aux(L).

write_numbered_aux([]).

write_numbered_aux([H | T]) :-
  b_getval(write_numbered_counter, C),
  CC is C + 1,
  b_setval(write_numbered_counter, CC),
  format('[~d] ~w~n', [C, H]),
  write_numbered_aux(T).


% store neighborhood information
:- dynamic(neighbor/2).


add_neighborhood([]).

add_neighborhood([(A, B, C, K) | L]) :-
  get_locations_from_kinetics(K, D),
  append([A, B, C, D], E),
  % remove stoichiometry if there is some
  maplist(nusmv:reactant_to_name, E, F),
  maplist(get_location, F, G),
  sort(G, H),
  subtract(H, [default], I),
  add_neighboor_pairs(I),
  add_neighborhood(L).


add_neighboor_pairs(L) :-
  findall(
    neighbor(A, B),
    (
      member(A-_, L),
      member(B-_, L),
      A @< B
    ),
    Pairs
  ),
  maplist(assertz, Pairs).


%! get_location(Species_list, Location_list)

get_location(_@L, L-Volume) :-
  !,
  get_volume(L, Volume).

get_location(_, default_volume-Volume) :-
  get_volume(default_volume, Volume).


%! get_volume(Location, Volume)
%
% Return the volume of a given location

get_volume(Location, Volume) :-
  (
    item([kind:parameter, key:Location, item:parameter(Location=Volume)])
  ->
    true
  ;
    item([kind:function, item:function(Location=Volume)])
  ->
    true
  ;
    Volume = 1
  ).


compute_neighborhood :-
  retractall(neighbor(_, _)),
  enumerate_reactions(Reactions),
  enumerate_influences(Influences),
  append(Reactions, Influences, Rules),
  add_neighborhood(Rules).


enumerate_reactions(Reactions) :-
  findall(
    (Reactants, Inhibitors, Products, Kinetics),
    (
      item([kind: reaction, item: Item]),
      reaction(Item, [
        reactants: Reactants,
        inhibitors: Inhibitors,
        products: Products,
        kinetics: Kinetics
      ])
    ),
    Reactions
  ).


enumerate_influences(Influences) :-
  findall(
    (PositiveInputs, NegativeInputs, [Output], Kinetics),
    (
      item([kind: influence, item: Item]),
      influence(Item, [
        positive_inputs: PositiveInputs,
        negative_inputs: NegativeInputs,
        kinetics: Kinetics,
        output: Output
      ])
    ),
    Influences
  ).


get_locations_from_kinetics(K, L) :-
  findall(
    M@Loc,
    sub_term(M@Loc, K),
    L
  ).
