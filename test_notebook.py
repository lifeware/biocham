#!python3
"""
A simple script that allows to compare the results of biocham notebook across successive
versions.
Two main functions are useful:
    - pickle(FileName) that keep in store the output of the notebook 
    - compare(FileName) that compare the output to the last pickle
"""
from os.path import dirname
import re
from subprocess import run, PIPE
import sys

notebook_index = [
    "library/examples/C2-19-Biochemical-Programming/TD1_lotka_volterra.ipynb"
    ]

### Tool functions ###

def execute(notebook, output=None, clean=True):
    """Main command to execute a notebook and convert it to rst"""
    command_head = ["jupyter", "nbconvert", "--execute",
        "--ExecutePreprocessor.timeout=10",
        "--ExecutePreprocessor.interrupt_on_timeout=True"]
    output_f = ["--output", output] if output else []
    command_tail = ["--to", "rst", notebook]
    command = command_head + output_f +command_tail
    print("Executing: " + notebook)
    run(command, stderr=PIPE)
    # Clean picture folder
    if output:
        files = dirname(notebook)+"/tempo_files"
    else:
        files = notebook.replace(".ipynb", "_files")
    run(["rm", "-r", files])

def analyse(diff_output):
    """Filter the output of diff, and signal if everything is fine"""
    no_diff = True
    for line in diff_output.split('\n'):
        if not line: continue
        if re.match("\d*c\d*", line): continue
        if re.search("</div>", line): continue
        if re.search("^---", line): continue
        if re.search("image", line): continue
        no_diff = False
        print(line)
    if no_diff: print("No difference detected !")

def curate(fname):
    """Return a sed command enclosed in an anonymous shell"""
    return f"<(sed 's/[0-9]/0/g; s/id=.*/id/g' {fname})"

### Main functions ###

def pickle(notebook):
    """Execute and save the output of a notebook as rst file"""
    execute(notebook)

def compare(notebook):
    """Compare the output of a notebook to its previous execution (see pickle)"""
    execute(notebook, "tempo.rst")
    # Compare to previous execution
    touchstone = notebook.replace("ipynb","rst")
    proof = dirname(notebook) + "/tempo.rst"
    result = run(["diff", touchstone, proof], stdout=PIPE, stderr=PIPE, universal_newlines=True)
    analyse(result.stdout)
    # Clean the board
    run(["rm", proof])

################ 
##### MAIN #####
################ 
if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Use 'pickle' to store the state of the current notebook or 'compare' to make a test")
    elif sys.argv[1] == "pickle":
        for notebook in notebook_index:
            pickle(notebook)
    elif sys.argv[1] == "compare":
        for notebook in notebook_index:
            compare(notebook)
