:- use_module(library(plunit)).

:- begin_tests(parameters, [setup((clear_model, reset_options))]).

test(
  'set_parameter',
  [true(Parameters == [parameter(a = 1), parameter(b = 3)])]
) :-
  clear_model,
  command(parameter(a = 1, b = 2)),
  command(parameter(b = 3)),
  all_items([kind: parameter], Parameters).

test(
  'delete_parameter',
  [true(Parameters == [parameter(k = 1)])]
) :-
  clear_model,
  command(parameter(k=1, l=2)),
  command(delete_parameter(l)),
  all_items([kind: parameter], Parameters).

test(
    'namespace bookkeeping', [error(already_defined_with_another_kind(_, _, _))]
  ) :-
  clear_model,
  command(parameter(k = 1)),
  command(k => b).

test(
    'namespace bookkeeping', [error(already_defined_with_another_kind(_, _, _))]
  ) :-
  clear_model,
  command(k => b),
  command(parameter(k = 1)).

:- end_tests(parameters).
