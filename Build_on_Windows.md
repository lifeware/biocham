# Building Biocham on Windows 10

Biocham may be built on Windows 10 using the Windows Subsystem for Linux.

## Steps

First, install WSL as per the [instructions](https://docs.microsoft.com/en-us/windows/wsl/install-win10), then install the Ubuntu distribution from the Windows store.


Run Ubuntu and obtain the Biocham sources (e.g. through git)


Then, run `./install.sh` from the source directory to install the project's dependencies and build Biocham.

Follow the instructions given by the install script (e.g. `source ~/.profile` at the end to update the path).

## Running Biocham

As with linux, run `./biocham --notebook` for instance to open a Jupyter notebook that talks to a Biocham kernel.

## Notes

WSL is apparently available on recent releases of Windows Server, but is untested on our side.
