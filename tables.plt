:- use_module(library(plunit)).

:- begin_tests(tables, [setup((clear_model, reset_options))]).

test(
  'add_table',
  [true(Tables == [table])]
) :-
  clear_model,
  add_table(table, [row(1, 2)]),
  all_items([kind: table], Tables).

test(
  'get_table_data',
  [true(Data == [row(1, 2)])]
) :-
  clear_model,
  add_table(table, [row(1, 2)]),
  get_table_data(Data).

test(
  'delete_column',
  [
    true(Data1-Data2 == Sol-Sol)
  ]
) :-
  clear_model,
  load_table('library/examples/tests/table.csv'),
  command(delete_column(1)),
  get_table_data(Data1),
  load_table('library/examples/tests/table.csv'),
  command(delete_column(a)),
  get_table_data(Data2),
  Sol = [row(0, 10), row(1, 7), row(2, 5), row(3, 3), row(4, 0)].

test(
  'manipulation',
  [
    true(H1-H2 == [time, a, b]-[time, my_column, b])
  ]
) :-
  load_table('library/examples/tests/table.csv'),
  get_table_headers(H1),
  rename_column(a, my_column),
  get_table_headers(H2).



test(
  'sanitizing',
  []
) :-
  tables:sanitize_ssr('1.', 1),
  tables:sanitize_ssr('1.e-3', 0.001).

:- end_tests(tables).
