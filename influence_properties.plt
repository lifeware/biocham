:- use_module(library(plunit)).

:- begin_tests(stable, [setup((clear_model, reset_options))]).

test(
  'list_bernot_stable',
  [
    setup(load('library:examples/circadian_cycle/bernot_comet.bc')),
    cleanup(clear_model)
  ]
) :-
  with_output_to(atom(_), list_stable_states),
  findall(
    S,
    influence_properties:stable_state(S),
    []
  ).

test(
  'list_kaufman_stable',
  [
    setup(load('library:examples/p53/kaufman.bc')),
    cleanup(clear_model)
  ]
) :-
  with_output_to(atom(_), list_stable_states),
  findall(
    S,
    influence_properties:stable_state(S),
    [_, _, _, _]
  ).

test(
  'list_stable_states',
  [
    setup(load('library/examples/cell_cycle/Tyson_1991.bc')),
    cleanup(clear_model)
  ]
) :-
  with_output_to(atom(_), list_stable_states).

:- end_tests(stable).

:- begin_tests(tscc).

test(
  'list_bernot_tscc',
  [
    setup(load('library:examples/circadian_cycle/bernot_comet.bc')),
    cleanup(clear_model)
  ]
) :-
  with_output_to(atom(Output), with_option(boolean_state_display: vector, list_tscc_candidates)),
  sub_atom(Output, _, _, _, '[G-1,L-1,PC-1] terminal'),
  !.

test(
  'list_kaufman_tscc',
  [
    setup(load('library:examples/p53/kaufman.bc')),
    cleanup(clear_model)
  ]
) :-
  with_output_to(atom(Output), with_option(boolean_state_display: vector, list_tscc_candidates)),
  sub_atom(Output, Before, _, After, '[C-1,D-1,N-1,P-0] terminal'),
  !,
  \+ sub_atom(Before, _, _, _, '] terminal'),
  \+ sub_atom(After, _, _, _, '] terminal').

:- end_tests(tscc).

