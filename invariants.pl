:- module(
  invariants,
  [
    find_pinvar/0,
    find_pinvar/1,
    find_all_pinvar/0,
    find_tinvar/0,
    find_tinvar/1,
    find_all_tinvar/0
  ]
).

:- reexport(library(clpfd)).
:- use_module(library(lists)).

:- use_module(arithmetic_rules).


% avoid useless symmetrical rules
:- dynamic(invar_done/2).
% store one in, one out places/transitions
:- dynamic(singular/1).
% store alternative paths
:- dynamic(path/2).
% store variables searched for invariants
:- dynamic(vars/1).
% store base
:- dynamic(base_mol/1).

%% find_all_pinvar
%
% find the complete set of minimal P-invariants

find_all_pinvar :-
   find_pinvar(sup).


find_pinvar :-
   find_pinvar(4).


%! find_pinvar(+ForcedMax) is det.
%
% find a set of minimal P-invariants with a given max value..
find_pinvar(ForcedMax) :-
   find_invar_aux(ForcedMax, 'is_place', 'is_transition', '#=').


%% find_all_tinvar
%
% find the complete set of minimal T-invariants

find_all_tinvar :-
   find_tinvar(sup).


find_tinvar :-
   find_tinvar(4).


%! find_tinvar(+ForcedMax) is det.
%
% find a set of minimal T-invariants with a given max value..
find_tinvar(ForcedMax) :-
   find_invar_aux(ForcedMax, 'is_transition', 'is_place', '#=').


find_invar_aux(ForcedMax, Type, OtherType, Operator) :-
   reaction_graph,
   findall(
      (Ins, Outs),
      (
         Goal =.. [OtherType, T],
         Goal,
         findall(
            (W, P),
            arc(P, T, W),
            Ins
         ),
         findall(
            (W, P),
            arc(T, P, W),
            Outs
         )
      ),
      L
   ),
   mark_singular(Type),
   (
      find_invar(L, ForcedMax, Operator),
      !
   ;
      write('No complex invariant found\n')
   ),
   % singletons
   vars(Vars),
   write_list_to_solution_if(
      (
         Goal2 =.. [Type, P],
         Goal2,
         \+(member(P, Vars)),
         % can be added by normalized_path
         \+(base_mol([P])),
         assertz(base_mol([P]))
      ),
      [P]
   ).


arc(A, B, C) :-
   get_current_graph(GraphId),
   item([parent: GraphId, item: (A -> B), id: Id]),
   (
      get_attribute(Id, stoichiometry = S)
   ->
      normalize_number(S, C),
      (
        float(C)
      ->
        throw(error('Non-integer stoichiometry is not supported.'))
      ;
        true
      )
   ;
      C = 1
   ).


is_type(P, Type) :-
   get_current_graph(GraphId),
   item([parent: GraphId, item: P, kind: vertex, id: Id]),
   get_attribute(Id, kind=Type).


is_place(P) :-
   is_type(P, place).

is_transition(T) :-
   is_type(T, transition).


%% mark_singular(+Type)
%
% mark singular places/transitions (depending on Type)
% cf. Soliman WCB'08 or MATHMOD'09
mark_singular(Type) :-
  retractall(singular(_)),
  (
    Goal =.. [Type, E],
    Goal,
    % weight 1 necessary?
    findall(Before, arc(Before, E, 1), [_]),
    findall(After, arc(E, After, 1), [_]),
    assertz(singular(E)),
    fail
  ;
    debugging(invariants)
  ->
    listing(singular/1)
  ;
    true
  ).


%% find_invar(+IOList, +ForcedMax, +Operator)
%
% find invariants for a list of weighted (In, Out) and a given max value
% Operator defines the relation (=, =<, >=) between left and right side for
% sub/sur-invariants

find_invar(IOList, ForcedMax, Operator) :-
  retractall(invar_done(_, _)),
  retractall(base_mol(_)),
  remove_common(IOList, UIOList),
  % add constraints X.M=0
  get_constraints(UIOList, [], [], Vars, VarList, 1, MaxDomain1, Operator),
  retractall(vars(_)),
  nb_setval(base, []),
  assertz(vars(Vars)),
  (
    ForcedMax == sup
  ->
    MaxDomain = MaxDomain1
  ;
    MaxDomain is min(MaxDomain1, ForcedMax)
  ),
  % add a large upper bound for minimal invariants
  VarList ins 0..MaxDomain,
  build_sum(VarList, Sum),
  !,
  % avoid the trivial solution
  Sum #> 0,
  find_paths,
  (
    debugging(invariants)
  ->
    listing(path/2)
  ;
    true
  ),
  % zero the alternative of each path
  zero_paths(Vars, VarList),
  % reorder vars by neighborhood
  % FIXME compare with reverse(VarList, ListVar),
  reorder(Vars, VarList, ListVar),
  (
    repeat,
    (
      get_base_constr(VarList),
      % does NOT guarantee a base e.g.
      % add_rule(2*Y + Z => 3*X). for labeling not ff
      % my_labeling(VarList)
      label(ListVar)
    ->
      nb_getval(base, B),
      (
        MaxDomain > 1
      ->
        make_base(B, VarList, BB)
      ;
        BB = [VarList | B]
      ),
      nb_setval(base, BB),
      debug(invariants, "new base: ~w", [BB]),
      fail
    ;
      retractall(base_mol(_)),
      expand_base(Vars),
      nb_setval(nb, 0),
      write_list_to_solution_if(
        (
          base_mol(Mol),
          nb_getval(nb, N),
          NN is N + 1,
          nb_setval(nb, NN),
          sort(Mol, SMol)
        ),
        SMol
      ),
      nb_getval(nb, LB),
      format("~w complex invariant(s)~n", [LB])
    )
  ->
    true
  ).


%% expand_base(+Vars)
%
% unfolds symmetries in the current base of invariants, using the list of
% variables Vars
expand_base(Vars) :-
  nb_getval(base, B),
  member(VL, B),
  to_mols(VL, Vars, Mol),
  assertz(base_mol(Mol)),
  debug(invariants, "base_mol(~w)", [Mol]),
  fail.

expand_base(_) :-
   normalized_path(E, L),
   debug(invariants, "Found normalized_path(~w, ~w)", [E, L]),
   findall(
      [E | MMol],
      (
         base_mol(Mol),
         subset(L, Mol),
         subtract(Mol, L, MMol)
      ),
      NewBases
   ),
   assert_to_base(NewBases),
   debug(invariants, "asserting base_mol for all ~w", [NewBases]),
   fail.

expand_base(_).


%% normalized_path(-E, +P)
%
% get a path starting from E and such that it follows other pathes to their
% end.
normalized_path(E, P) :-
   path(E, L),
   norm_path_rec(L, P).


norm_path(E, P) :-
   path(E, L),
   !,
   norm_path_rec(L, P).

norm_path(E, [E]).


norm_path_rec([], []).

norm_path_rec([H | T], L) :-
   norm_path(H, P),
   append(P, Q, L),
   norm_path_rec(T, Q).


%% assert_to_base(+L)
%
% add to the base the list of molecules L
assert_to_base([]).
assert_to_base([H | T]) :-
   assertz(base_mol(H)),
   assert_to_base(T).


%% find_paths, find_paths(CurrentLength, MaximumLength)
%
% find parallel singular paths (of length between CurrentLength and
% MaximumLength
find_paths :-
   retractall(path(_, _)),
   find_paths(1, 8).


find_paths(Length, _Maxlength) :-
   singular(E),
   arc(Before, E, 1),
   arc(E, After, 1),
   (
      get_path(Before, After, E, Length, Path)
   ->
      retract(singular(E)),
      assertz(path(E, Path)),
      fail
   ).

find_paths(Length, MaxLength) :-
   (
      Length < MaxLength
   ->
      NewLength is Length + 1,
      find_paths(NewLength, MaxLength)
   ;
      true
   ).


%% get_path(Before, After, Current, Length, Path)
%
% look for a path
get_path(A, B, E, 1, [C]) :-
   !,
   arc(A, C, 1),
   C \= E,
   singular(C),
   arc(C, B, 1).

get_path(A, B, E, N, [C | L]) :-
   arc(A, C, 1),
   C \= E,
   singular(C),
   arc(C, D, 1),
   NN is N - 1,
   get_path(D, B, E, NN, L).


%% zero_paths(+Vars, ?VarList), zero_rec(+Path, +Vars, ?VarList)
%
% force all components of a pth to be equal to 0
zero_paths(Vars, VarList) :-
   findall(E, path(E, _), L),
   zero_rec(L, Vars, VarList).


zero_rec([], _, _).

zero_rec([M | L], Vars, VarList) :-
   nth1(N, Vars, M),
   !,
   nth1(N, VarList, V),
   V = 0,
   zero_rec(L, Vars, VarList).

zero_rec([_ | L], Vars, VarList) :-
   zero_rec(L, Vars, VarList).


%% get_constraints(+L, -Vars, -VarList, -NVars, -NVarList, -MaxDomain,
%%                 -NMaxDomain, +Operator)
%
% From a list of rules (KR, KL) get involved molecules, associate a variable
% list and the product of highest coefficients (for upper bound)

get_constraints([], Vars, VarList, Vars, VarList, MaxDomain, MaxDomain, _Op).

% if handling equality, remove symmetrical rules
get_constraints([(KL, KR) | L], Vars, VarList, NVars, NVarList, MaxDomain, NMaxDomain, '#=') :-
   invar_done(KR, KL),
   !,
   get_constraints(L, Vars, VarList, NVars, NVarList, MaxDomain, NMaxDomain, '#=').

get_constraints([(KL, KR) | L], Vars, VarList, NVars, NVarList, MaxDomain, NMaxDomain, Operator) :-
   assertz(invar_done(KL, KR)),
   make_sum(KL, SL, Vars, VarList, Vars1, VarList1, Max1),
   make_sum(KR, SR, Vars1, VarList1, Vars2, VarList2, Max2),
   % set upper bound
   MaxDomain2 is MaxDomain * max(max(1, Max1), max(1, Max2)),
   Constr =.. [Operator, SL, SR],
   Constr,
   % add constraint X.M = O
   get_constraints(L, Vars2, VarList2, NVars, NVarList, MaxDomain2,
      NMaxDomain, Operator).


%% make_sum(+L, -Sum, +Vars, +VarList, -Vars1, -VarList1, -Max)
%
% get molecule and variable list with maximum coeff
make_sum([], 0, V, VL, V, VL, 0).

make_sum([(S, M)], VV, Vars, VarList, Vars1, VarList1, S) :-
   (
      S = 1
   ->
      VV = V
   ;
      VV = S * V
   ),
   (
      nth1(N, Vars, M)
   ->
      nth1(N, VarList, V),
      Vars1 = Vars,
      VarList1 = VarList
   ;
      V in 0..sup,
      Vars1 = [M | Vars],
      VarList1 = [V | VarList]
   ),
   !.

make_sum([(S, M) | L], VV + Sum, Vars, VarList, Vars1, VarList1, Max) :-
   (
      S = 1
   ->
      VV = V
   ;
      VV = S * V
   ),
   (
      nth1(N, Vars, M)
   ->
      nth1(N, VarList, V),
      Vars2 = Vars,
      VarList2 = VarList
   ;
      V in 0..sup,
      Vars2 = [M | Vars],
      VarList2 = [V | VarList]
   ),
   make_sum(L, Sum, Vars2, VarList2, Vars1, VarList1, Max1),
   Max is Max1 + S,
   !.


%% to_mols(+ValueList, +VarList, -MolList)
%
% replace instantiated variables by the corresponding named and stoechiometric
% object (a conservation law for a P-inv)

to_mols([], [], []).

to_mols([0 | T], [_ | Vars], Mols) :-
   !,
   to_mols(T, Vars, Mols).

to_mols([1 | T], [V | Vars], [V | Mols]) :-
   !,
   to_mols(T, Vars, Mols).

to_mols([N | T], [V | Vars], [N * V | Mols]) :-
   to_mols(T, Vars, Mols).

%% get_base_constr(?VarList)
%
% Add constraints on the variables that no base vector should be overlapped

get_base_constr(VarList) :-
   nb_getval(base, B),
   get_base_constr_rec(B, VarList).


get_base_constr_rec([], _).

get_base_constr_rec([B | BL], VarList) :-
   build_prod(B, VarList, _Prod, Supp, _NotSupp),
   element(_, Supp, 0),
   get_base_constr_rec(BL, VarList).


%% build_prod(ValueList, ?VarList, -Prod, -S, -NS) :-
%
% builds the product of the variables, forgetting stoichiometry
% records list of the variables participating in the product in S
% and not participating in the product in NS
build_prod([], [], 1, [], []).

build_prod([0 | B], [V | VarList], Prod, S, [V | NS]) :-
   !,
   build_prod(B, VarList, Prod, S, NS).

build_prod([_ | B], [V | VarList], Prod * V, [V | S], NS) :-
   build_prod(B, VarList, Prod, S, NS).


%% make_base(+B,  +V, -BB)
%
% remove in B vectors subsumed by V, then add it and return BB
make_base(T,  H, [H | T]) :-
   % if new vector has max value 1, cannot subsume an earlier vect
   % FIXME if MaxDomain=1 this check is useless...
   max_list(H, 1),
   !.

make_base([], V, [V]).

make_base([H | T], V, B) :-
   (
      bigger_support(H, V)
   ->
      make_base(T, V, B)
   ;
      B = [H | B1],
      make_base(T, V, B1)
   ).


%% bigger_support(V1, V2)
%
% succeeds if V1 has a bigger support (non-null components) than V2
bigger_support([], []).

bigger_support([_ | V], [0 | B]) :-
   !,
   bigger_support(V, B).

bigger_support([N | V], [_ | B]) :-
   !,
   N > 0,
   bigger_support(V, B).


%% remove_common(+IOList, -UIOList)
%
% Remove (stoechiometrically) catalyzers in a list of (I, O) pairs
% eauch pair is a list of stoichiometric pairs
remove_common([], []).

remove_common([(I, O) | IOList], [(UI, UO) | UIOList]) :-
   remove_common(I, O, UI, UO),
   remove_common(IOList, UIOList).


remove_common([], KR, [], KR).

remove_common([(SL, M) | KL], KR, UKL, UKR) :-
   (
      select((SR, M), KR, KR1)
   ->
      (
         SL > SR
      ->
         SL2 is SL - SR,
         UKL = [(SL2, M) | KL1],
         remove_common(KL, KR1, KL1, UKR)
      ;
         (
            SL < SR
         ->
            SR2 is SR - SL,
            UKR = [(SR2, M) | KR2],
            remove_common(KL, KR1, UKL, KR2)
         ;
            remove_common(KL, KR1, UKL, UKR)
         )
      )
   ;
      UKL = [(SL, M) | KL1],
      remove_common(KL, KR, KL1, UKR)
   ).


%% reorder(+Values, ?VarList1, ?VarList2)
%
% reorders VarList1 into VarList2 s.t. most neighbors (computed using Values)
% are taken first
reorder(Vars, L1, L2) :-
   % initialize neighbors to 0 for everyone
   make_keylist(Vars, KV),
   reorder_aux(KV, L1, L2).


%% reorder_aux(+KeyValues, ?VarList1, ?VarList2)
%
% same as above but with a Key-Value list representing the current neighbors
reorder_aux([], [], []).

reorder_aux(KL, VL, [V | L]) :-
   keysort(KL, SKL),
   % sort is ascending, so biggest is last...
   last(SKL, _-H),
   nth1(N, KL, _-H),
   % get corresponding variable
   nth1(N, VL, V),
   remove_nth(N, KL, KT),
   remove_nth(N, VL, VT),
   update_neighbors(KT, H, KTT),
   reorder_aux(KTT, VT, L).


%% make_keylist(+List, -KeyList)
%
% transforms List into a Key-Value pairs list, with all keys equal to 0
make_keylist([], []).

make_keylist([H | T], [0-H | TT]) :-
   make_keylist(T, TT).


%% update_neighbors(+L, A, -LL)
%
% udates the Key-value list L into LL adding neighbors of A
update_neighbors([], _, []).
update_neighbors([N-A | L], B, [M-A | LL]) :-
   (
      (
         arc(A, X, _),
         arc(B, X, _)
      ;
         arc(A, X, _),
         arc(X, B, _)
      ;
         arc(X, A, _),
         arc(B, X, _)
      ;
         arc(X, A, _),
         arc(X, B, _)
      )
   ->
      M is N + 1
   ;
      M = N
   ),
   update_neighbors(L, B, LL).


write_list_to_solution_if(Goal, List) :-
   forall(
      Goal,
      (
         reaction_editor:list_to_solution(List, Solution),
         write_term(Solution, [quoted(false)]),
         nl
      )
   ).


%% build_sum(+List, ?Sum)
%
% builds the sum of a list

build_sum([], 0).
build_sum([A], A).
build_sum([H1, H2 | T], S):-
   S #= H1 + S2,
   build_sum([H2 | T], S2).


%% remove_nth(+N, +L, -LL)
%
% remove Nth element of L and obtain LL
remove_nth(1, [_ | T], T).

remove_nth(N, [H | T], [H | L]) :-
   N > 1,
   M is N - 1,
   remove_nth(M, T, L).
