:- module(
  sbml_files,
  [
    % Biocham Command
    add_note/2,
    read_note/1,
    read_annotation/1,
    % Internal API
    load_sbml/1,
    add_sbml/1,
    add_sbml_file/1,
    export_sbml/1,
    download_curated_biomodel/1
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(objects).
:- use_module(kinetics).
:- use_module(arithmetic_rules).
:- use_module(reaction_editor).

:- use_module(library(http/http_open)).
:- use_module(library(http/http_client)).
:- use_module(library(http/http_json)).
:- use_module(library(sgml_write)).
:- use_module(library(pcre)).

prolog:message(error(sbml_errors, Message)) -->
  ['SBML Error: ~p'-[Message]].

prolog:message(sbml_warnings(Message)) -->
  ['SBML Warning: ~p'-[Message]].

prolog:message(object_not_found(Object)) -->
  ['No object named ~w was founded.'-[Object]].


:- devdoc('These commands have been modified not to fail on errors which may be just warning.').

:- devdoc('\\section{Commands}').

:- initial(option(use_sbml_id: no)).

load_sbml(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    acts as \\command{load_biocham/1}, this import the species, reactions, parameters,
    compartments, events and initial states from the provided SBML .xml file. By default,
    we use the sbml Names of the species as biocham identifier. When this leads to naming
    conflicts, we suffix these names with the sbml Id.  The option use_sbml_id (default:
    no) allows to bypass this naming and simply use the id as identifier.
    Rq: Notes and annotations are imported but are not accessible for the user.
  '),
  option(use_sbml_id, yesno, _Use_sbml_id, "Use the sbml_id for the import of all sbml object instead of their names"),
  load_all('xml', InputFile).


add_sbml(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    acts as \\command{add_biocham/1} but importing reactions, parameters and
    initial state (and only that!) from an SBML .xml file.
  '),
  add_all('xml', InputFile).


:- devdoc('\\section{Public API}').


models:add_file_suffix('xml', add_sbml_file).


add_sbml_file(Filename) :-
  automatic_suffix(Filename, '.xml', read, FilenameXML),
  setup_call_cleanup(
    readSBML(FilenameXML, SBML),
    add_sbml_document(SBML),
    sbmlDocument_free(SBML)
  ).


add_sbml_document(SBML) :-
  sbmlDocument_printErrors(SBML, user_error),
  sbmlDocument_getNumErrors(SBML, Errors),
  (
    Errors > 0
  ->
    throw(error(sbml_errors, "The C-API found errors while loading the document."))
  ;
    true
  ),
  sbmlDocument_getModel(SBML, Model),
  add_sbml_model(Model).


:- dynamic(dangling_parameter/1).

add_sbml_model(Model) :-
  retractall(dangling_parameter(_)),
  add_compartments(Model),
  initialize_biochamName(Model),
  single_model(Model_id),
  load_annotation_and_notes(Model, Model_id),
  add_global_parameters(Model),
  add_assignment_rules(Model),
  add_function_definitions(Model),
  add_events(Model),
  add_species(Model),
  add_initialAssignments(Model),
  add_reactions(Model),
  add_rateRules(Model),
  add_algebraic_rules(Model),
  % Check if their is some reminding pbm
  (
    dangling_parameter(_)
  ->
    print_message(warning, sbml_warnings("A parameter seems undefined, we put it to NaN."))
  ;
    true
  ).


%! initialize_biochamName(+Model)
%
% Scan a model to initialize the speciesId_biochamName dict

:- dynamic(speciesId_biochamName/2).

initialize_biochamName(Model) :-
  retractall(speciesId_biochamName(_, _)),
  findall(
    NameatV,
    (
      model_species(Model, Species),
      species_getName_or_Id(Species, Name),
      add_volume_if_needed(Species, Name, NameatV)
    ),
    ListNames
  ),
  debug(sbml, "Names list: ~w", [ListNames]),
  ( 
    ( % check the option
      get_option(use_sbml_id, Use_sbml_id),
      Use_sbml_id = yes
    ; % to catch all problematic models, complete it if necessary
      member('', ListNames)
    )
  ->
    debug(sbml, "using ids", []),
    forall(
      model_species(Model, Species),
      (
        species_getId(Species, Id),
        add_volume_if_needed(Species, Id, IdatV),
        assertz(speciesId_biochamName(Id, IdatV))
      )
    )
  ; % This should be the main case
    is_set(ListNames)
  ->
    debug(sbml, "using names", []),
    forall(
      model_species(Model, Species),
      (
        species_getId(Species, Id),
        species_getName_or_Id(Species, Name),
        add_volume_if_needed(Species, Name, NameatV),
        assertz(speciesId_biochamName(Id, NameatV))
      )
    )
  ; % Create a set of unique names
    debug(sbml, "using name_ids", []),
    forall(
      model_species(Model, Species),
      (
        species_getId(Species, Id),
        species_getName_or_Id(Species, Name),
        concat_atom([Name, '__', Id], BcName),
        add_volume_if_needed(Species, BcName, BcNameatV),
        assertz(speciesId_biochamName(Id, BcNameatV))
      )
    )
  ),
  debug(sbml, "end name initialization", []).


%! add_volume_if_needed(+Species, +Name, -NameAtVolume)
%
%

add_volume_if_needed(Species, Name, NameAtVolume) :-
  (
    single_compartment(_)
  ->
    NameAtVolume = Name
  ;
    species_getCompartment(Species, Comp),
    NameAtVolume = Name@Comp
  ).



%! load_annotation_and_notes(+Sbml_object, +Biocham_id)
%
% Fetch and store the sbml annotations and notes if any

load_annotation_and_notes(Sbml_object, Biocham_id) :-
  debug(sbml, "loading annotations for ~w (sbml:~w)", [Biocham_id, Sbml_object]),
  ( % Annotations
    object_getAnnotation(Sbml_object, Annotation_string),
    debug(sbml, "> annotations: ~w", [Annotation_string])
  ->
    open_string(Annotation_string, Stream_annotation),
    load_xml(Stream_annotation, Annotation, []),
    set_annotation(Biocham_id, sbml_annotation, Annotation)
  ;
    true
  ),
  ( % Notes
    object_getNotes(Sbml_object, Notes_string)
  ->
    set_annotation(Biocham_id, sbml_notes, Notes_string)
  ;
    true
  ),
  debug(sbml, "> loaded", []).


%! add_note(+Biocham_object, +Note)
%
% Allows to add note for SBML export

add_note(Object, Note) :-
  biocham_command,
  (
    name_kind_id(Object, _Kind, Biocham_id)
  ->
    (
      get_annotation(Biocham_id, biocham_notes, Prec)
    ->
      string_concat(Prec, "\n", Tempo),
      string_concat(Tempo, Note, VNote)
    ;
      VNote = Note
    ),
    set_annotation(Biocham_id, biocham_notes, VNote)
  ;
    print_message(warning, object_not_found(Object))
  ).



%! read_note(+Biocham_object)
%
% Print the note as imported from SBML

read_note(Object) :-
  biocham_command,
  read_annotation(Object, [sbml_notes, biocham_notes]).


%! read_annotation(+Biocham_object)
%
% Print the annotations as imported from SBML

read_annotation(Object) :-
  biocham_command,
  read_annotation(Object, [sbml_annotation]).


% read_annotation(+Object, +Kind_list)
%
% Subroutine for read_note and read_annotation

read_annotation(Object, Kind_list) :-
  (
    name_kind_id(Object, _Kind, Biocham_id)
  ->
    forall( 
      (
        get_annotation(Biocham_id, Kind, Note),
        member(Kind, Kind_list)
      ),
      writeln(Note)
    )
  ;
    print_message(warning, object_not_found(Object))
  ).


:- dynamic(has_only_substance_units/1).

:- dynamic(is_boundary/1).

add_species(Model) :-
  debug(sbml, "Adding species", []),
  single_model(ModelId),
  retractall(has_only_substance_units(_)),
  retractall(is_boundary(_)),
  forall(
    model_species(Model, Species),
    (
      debug(sbml, "Looking at species: ~w", [Species]),
      species_id_and_initial(Species, IdSbml, Initial),
      speciesId_biochamName(IdSbml, BcName),
      debug(sbml, "Looking at species: ~w", [BcName]),
      add_item([parent: ModelId, id: ID_species, kind: species, item: BcName]),
      set_annotation(ID_species, sbml_id, IdSbml),
      load_annotation_and_notes(Species, ID_species),
      species_constant_and_boundary(Species, _Constant, Boundary),
      (
        Boundary == true
      ->
        assertz(is_boundary(BcName))
      ;
        true
      ),
      (
        var(Initial)
      ->
        ( % already a function from an assignment rule
          identifier_kind(ModelId, BcName, function)
        ->
          true
        ; % boundary species but considered as a Species due to rateRule
          Boundary == true,
          model_rateRule(Model, RateRule),
          rule_var_and_math(RateRule, IdSbml, _)
        ->
          true
        ; % boundary species considered as a function
          Boundary == true
        ->
          function([BcName = BcName])
        )
      ;
        (
          Initial = Amount / Compartment
        ->
          ( % if the compartment is defined through a parameter
            parameter_value(Compartment, Volume)
          ->
            true
          ; % or if it is defined through a function
            item([kind: function, item: function(Compartment = Volume)])
          ->
            true
          ; % or even if it is a species
            get_initial_state(Compartment, initial_state(Compartment=Volume))
          ),
          InitialC is Amount / Volume
        ;
          InitialC = Initial
        ),
        ( % boundary species but considered as a Species due to rateRule
          Boundary == true,
          model_rateRule(Model, RateRule),
          rule_var_and_math(RateRule, IdSbml, _)
        ->
          set_initial_concentration(BcName, InitialC)
        ;
          Boundary == true,
          \+(identifier_kind(ModelId, IdSbml, function)) % avoid redefining function
        ->
          function([BcName = InitialC])
        ;
          % already a function from an assignment rule
          identifier_kind(ModelId, BcName, function)
        ->
          true
        ;
          set_initial_concentration(BcName, InitialC)
        )
      ),
      (
        species_has_only_substance_units(Species)
      ->
        assertz(has_only_substance_units(BcName))
      ;
        true
      )
    )
  ),
  debug(sbml, "All species loaded", []).


add_initialAssignments(Model) :-
  debug(sbml, "Adding the initial assignments", []),
  model_getListOfInitialAssignments(Model, InitialAssignmentsList),
  forall(
    member_listof(InitialAssignment, InitialAssignmentsList),
    (
      initialAssignment_getSymbol(InitialAssignment, Symbol),
      initialAssignment_getMath(InitialAssignment, Math),
      debug(sbml, "Initial assignment: ~w = ~w", [Symbol, Math]),
      (
        parameter_value(Symbol, _),
        is_numeric(Math, Value),
        dangling_parameter(Symbol)
      ->
        set_parameter(Symbol, Value),
        retract(dangling_parameter(Symbol))
      ;
        speciesId_biochamName(Symbol, Species),
        is_numeric(Math, Value)
      ->
        set_initial_concentration(Species, Value)
      ;
        throw(error(sbml_errors, "InitialAssignment error!"))
      )
    )
  ).


add_global_parameters(Model) :-
  debug(sbml, "Adding global parameters", []),
  forall(
    model_parameter(Model, Parameter),
    (
      parameter_id_and_value(Parameter, Id, Value),
      debug(sbml, "Parameter ~w = ~w", [Id, Value]),
      (
        var(Value)
      ->
        assertz(dangling_parameter(Id)),
        Is_nan is nan,
        set_parameter(Id, Is_nan)
      ;
        model_rateRule(Model, RateRule),
        rule_var_and_math(RateRule, Id, _)
      ->
        initial_state([Id = Value])
      ;
        set_parameter(Id, Value)
      )
    )
  ).


add_assignment_rules(Model) :-
  forall(
    model_assignment_rule(Model, Rule),
    (
      rule_var_and_math(Rule, Var, Math),
      debug(sbml, "Assignment rule: ~w = ~w", [Var, Math]),
      (
        parameter_value(Var, _)
      ->
        delete_parameter([Var]),
        (
          dangling_parameter(Var)
        ->
          retract(dangling_parameter(Var))
        ;
          true
        )
      ;
        true
      ),
      (
        speciesId_biochamName(Var, Name)
      ->
        function([Name = Math])
      ;
        function([Var = Math])
      )
    )
  ).


add_function_definitions(Model) :-
  forall(
    model_function(Model, Function),
    (
      (
        function_name_args_body(Function, Name, Args, Body),
        check_function(Args, Body)
      ->
        Func =.. [Name | Args],
        function([Func = Body])
      ;
        throw(error(sbml_errors, "Invalid function definition."))
      )
    )
  ).


%! check_function(+Args, +Body)
%
% test if Body is a valid function definition

check_function(Args, Body) :-
  tree_leaves(Body, Putative_args),
  forall(
    member(Arg, Putative_args),
    (
      is_numeric(Arg)
    ;
      member(Arg, Args)
    )
  ).


add_events(Model) :-
  debug(sbml, "Start adding events", []),
  forall(
    model_event(Model, Event),
    (
      event_cond_assign(Event, Condition, Assignments, HasDelay),
      (
        HasDelay
      ->
        print_message(warning, sbml_warnings("An event has a Delay, Biocham does not support this (for now), so we ignore the delay!"))
      ;
        true
      ),
      debug(sbml, "Adding event: ~w", [Assignments]),
      add_event(Condition, Assignments)
    )
  ).


global_local_parameters([], _Rid, Kinetics, Kinetics).

global_local_parameters([Parameter | LocalParameters], Rid, LKinetics, GKinetics) :-
  parameter_id_and_value(Parameter, Id, Value),
  (
    var(Value)
  ->
    Value is nan,
    print_message(warning, sbml_warnings("A parameter seems undefined, we put it to NaN."))
  ;
    true
  ),
  global_id(Rid, Id, GlobalId),
  set_parameter(GlobalId, Value),
  substitute([Id], [GlobalId], LKinetics, Kinetics),
  global_local_parameters(LocalParameters, Rid, Kinetics, GKinetics).


global_id(Rid, Id, GlobalId) :-
  atomic_list_concat([Rid, Id], '__', GlobalId).


add_reactions(Model) :-
  debug(sbml, "Adding reactions", []),
  forall(
    model_reaction(Model, Reaction),
    add_sbml_reaction(Reaction)
  ).


add_rateRules(Model) :-
  debug(sbml, "Adding rateRules", []),
  single_model(ModelId),
  forall(
    model_rateRule(Model, RateRule),
    add_rateRule(ModelId, RateRule)
  ).


add_rateRule(ModelId, RateRule) :-
  rule_var_and_math(RateRule, Var, Math),
  debug(sbml, "RateRule: ~w -> ~w", [Var, Math]),
  expression_to_list(Math, Kinetic_list),
  forall(
    member(Kinetic, Kinetic_list), % /!\ Kinetic is negative for degradation
    (
      debug(sbml, "Monomial: ~w", [Kinetic]),
      (
        Kinetic = -Unsigned_kinetic
      ->
        (
          item([parent: ModelId, id: Id, kind: reaction, item: Reaction]),
          reaction(Reaction, Unsigned_kinetic, Reactants, Inhibitors, Products)
        ->
          delete_item(Id),
          append(Reactants, [1*Var], New_reactants),
          add_reaction(Unsigned_kinetic, New_reactants, Inhibitors, Products, false)
        ;
          add_reaction(Unsigned_kinetic, [1*Var], [], [], false)
        )
      ;
        Kinetic = Unsigned_kinetic,
        (
          item([parent: ModelId, id: Id, kind: reaction, item: Reaction]),
          reaction(Reaction, Unsigned_kinetic, Reactants, Inhibitors, Products)
        ->
          delete_item(Id),
          append(Products, [1*Var], New_products),
          add_reaction(Unsigned_kinetic, Reactants, Inhibitors, New_products, false)
        ;
          add_reaction(Unsigned_kinetic, [], [], [1*Var], false)
        )
      )
    )
  ).
  

add_algebraic_rules(Model) :-
  (
    model_algebraic_rule(Model, _Rule)
  ->
    print_message(warning, sbml_warnings("We find an algebraic rule, Biocham does not support this (for now), so we ignore it!"))
  ;
    true
  ).


add_units(Model) :-
  debug(sbml, "getting units…", []),
  (
    model_getTimeUnits(Model, Time)
  ->
    debug(sbml, "getTimeUnits(~w)", [Time]),
    set_units(time, Time)
  ;
    debug(sbml, "no timeUnits defined", [])
  ),
  (
    model_getSubstanceUnits(Model, Substance)
  ->
    debug(sbml, "getSubstanceUnits(~w)", [Substance]),
    set_units(substance, Substance)
  ;
    debug(sbml, "no substanceUnits defined", [])
  ),
  (
    model_getVolumeUnits(Model, Volume)
  ->
    debug(sbml, "getVolumeUnits(~w)", [Volume]),
    set_units(volume, Volume)
  ;
    debug(sbml, "no volumeUnits defined", [])
  ).


% Store SBML-defined compartments/locations
:- dynamic(compartment/1).
:- dynamic(single_compartment/1).

prolog:message(zero_dimension_compartment(Id)) -->
  [''-[Id]].

add_compartments(Model) :-
  retractall(compartment(_)),
  retractall(single_compartment(_)),
  forall(
    model_compartment(Model, Compartment),
    (
      compartment_volume(Compartment, Id, Dimension, Volume),
      assertz(compartment(Id)),
      (
        Dimension = 0.0
      ->
        print_message(warning, sbml_warnings("A compartment has a dimension of 0 which is not supported by Biocham, we set its volume to 1.")),
        Volume_corrected = 1
      ;
        Volume_corrected = Volume
      ),
      debug(sbml, "Compartement ~w, volume ~w", [Id, Volume]),
      ( % If the compartment is compeled to a rateRule its considered as a species
        model_rateRule(Model, RateRule),
        rule_var_and_math(RateRule, Id, _)
      ->
        initial_state([Id = Volume_corrected])
      ;
        set_parameter(Id, Volume_corrected)
      )
    )
  ),
  (
    findall(C, compartment(C), LoC),
    debug(sbml, "List of loaded compartments: ~w", [LoC]),
    LoC = [Single]
  ->
    assertz(single_compartment(Single)),
    debug(sbml, "Single compartment", []),
    (
      Single = default_volume
    ->
      true
    ;
      function([default_volume = Single])
    )
  ;
    true
  ).


scale_kinetics_to_concentrations(Kinetics, Scaled) :-
  (
    single_compartment(Compartment)
  ->
    term_morphism(
      sbml_files:scale_concentration(Compartment),
      Kinetics,
      ScaledNumber
    ),
    Scaled = ScaledNumber % / Compartment
  ;
    % FIXME I have no idea how to make a reasonable semantics for multiple
    % compartments
    Scaled = Kinetics
  ).


scale_concentration(Compartment, A, B) :-
  (
    has_only_substance_units(A)
  ->
    B = A * Compartment
  ;
    term_morphism(sbml_files:scale_concentration(Compartment), A, B)
  ).


add_sbml_reaction(SbmlReaction) :-
  debug(sbml, "Adding reaction: ~w", [SbmlReaction]),
  reaction_getReversible(SbmlReaction, Reversible),
  get_sbml_reactants(SbmlReaction, Reactants),
  debug(sbml, "Reactants: ~w", [Reactants]),
  get_sbml_modifiers(SbmlReaction, Modifiers),
  debug(sbml, "Modifiers: ~w", [Modifiers]),
  get_sbml_products(SbmlReaction, Products),
  debug(sbml, "Products: ~w", [Products]),
  reaction_kinetics(SbmlReaction, RawKinetics, LocalParameters),
  reaction_editor:format_kinetics(RawKinetics, Kinetics),
  debug(sbml, "Kinetics: ~w", [Kinetics]),
  get_reaction_id(SbmlReaction, Rid),
  global_local_parameters(LocalParameters, Rid, Kinetics, GKinetics),
  append(Reactants, Modifiers, AllReactants),
  append(Products, Modifiers, AllProducts),
  filter_out_boundaries(AllReactants, AllReactantsButBoundary),
  filter_out_boundaries(AllProducts, AllProductsButBoundary),
  debug(sbml, "Filtered reactants: ~w", [AllReactantsButBoundary]),
  debug(sbml, "Filtered products: ~w", [AllProductsButBoundary]),
  debug(sbml, "Kinetics: ~w", [GKinetics]),
  scale_kinetics_to_concentrations(GKinetics, ScaledKinetics),
  (
    Reversible = false
  ->
    FullKinetics = ScaledKinetics
  ;
    split_pos_neg_kinetics(ScaledKinetics, Positive, Negative),
    FullKinetics = Positive
  ),
  % TODO inhibitors
  reaction(Reaction, [
    kinetics: FullKinetics,
    reactants: AllReactantsButBoundary,
    products: AllProductsButBoundary
  ]),
  debug(sbml, "Adding reaction: ~w", [Reaction]),
  add_item([kind: reaction, item: Reaction]),
  (
    Reversible = true
  ->
    reaction(ReverseReaction, [
      kinetics: Negative,
      reactants: AllProductsButBoundary,
      products: AllReactantsButBoundary
    ]),
    add_item([kind: reaction, item: ReverseReaction])
  ;
    true
  ).


get_sbml_reactants(Reaction, Reactants) :-
  findall(
      Object,
      (
        reaction_reactant(Reaction, ReactantReference),
        speciesReference_object(ReactantReference, Object)
      ),
      Reactants
  ).


get_sbml_modifiers(Reaction, Modifiers) :-
  findall(
      1 * Species,
      (
        reaction_modifier(Reaction, ModifierReference),
        debug(sbml, "ModifierReference: ~w", [ModifierReference]),
        (
          modifierSpeciesReference_getSpecies(ModifierReference, Id),
          speciesId_biochamName(Id, Species)
        ->
          true
        ;
          throw(error(sbml_errors, "Invalid 'species' attribute value in SpeciesReference object"))
        )
      ),
      Modifiers
  ).


get_sbml_products(Reaction, Products) :-
  findall(
      Object,
      (
        reaction_product(Reaction, ProductReference),
        speciesReference_object(ProductReference, Object)
      ),
      Products
  ).


speciesReference_object(Reference, Stoichiometry * Species) :-
  debug(sbml, "spRef : ~w", [Reference]),
  (
    speciesReference_getSpecies(Reference, Id),
    speciesReference_getStoichiometry(Reference, FloatStoichiometry),
    speciesId_biochamName(Id, Species),
    debug(sbml, "spRef -> ~w", [Species])
  ->
    normalize_number(FloatStoichiometry, Stoichiometry)
  ;
    throw(error(sbml_errors, "Invalid 'species' attribute value in SpeciesReference object"))
  ).


split_pos_neg_kinetics(A - B, A, B) :-
  !.

split_pos_neg_kinetics(K*E, K*P, K*N) :-
  atomic(K),
  !,
  split_pos_neg_kinetics(E, P, N).

split_pos_neg_kinetics(E/K, P/K, N/K) :-
  atomic(K),
  !,
  split_pos_neg_kinetics(E, P, N).

split_pos_neg_kinetics(A, A, 0).


filter_out_boundaries([], []).

filter_out_boundaries([N * H | T], TT) :-
  (
    is_boundary(H)
  ->
    filter_out_boundaries(T, TT)
  ;
    TT = [N * H | TTT],
    filter_out_boundaries(T, TTT)
  ).

:- initial(option(output_to_library: no)).


download_curated_biomodel(Id) :-
  biocham_command,
  type(Id, integer),
  doc('Downloads to the current directory the SBML file for the corresponding
  (curated) biomodel with numeric ID \\argument{Id} (e.g. "5").'),
  option(output_to_library, yesno, ToLibrary, 'outputs to the
    library/biomodels subfolder'),
  format(atom(ModelId), 'BIOMD~|~`0t~d~10+', [Id]),
  atom_concat(ModelId, '.xml', OutFile),
  (
    ToLibrary == 'yes'
  ->
    atom_concat('library:biomodels/', OutFile, OutName),
    filename(OutName, FileName)
  ;
    FileName = OutFile
  ),
  % Using REST API https://www.ebi.ac.uk/biomodels/docs/#/
  format(
    atom(FirstUrl),
    'https://www.ebi.ac.uk/biomodels/model/files/~w?format=json',
    [ModelId]
  ),
  http_get(FirstUrl, FilesData, [json_object(dict)]),
  FilesData.main = [Main | _],
  DataFile = Main.name,
  format(
    atom(Url),
    '/biomodels/model/download/~w',
    [ModelId]
  ),
  setup_call_cleanup(
    (
      open(FileName, write, Out),
      http_open(
        [scheme(https), host('www.ebi.ac.uk'), path(Url), search([filename=DataFile])], In, []
      )
    ),
    copy_stream_data(In, Out),
    (
      close(In),
      close(Out)
    )
  ).


:- dynamic(sbml_id/2).


export_sbml(OutputFile) :-
  biocham_command,
  type(OutputFile, output_file),
  doc('
    exports the current model into an SBML \\texttt{.xml} file.
  '),
  retractall(sbml_id(_, _)),
  set_counter(sbml_ids, 0),
  automatic_suffix(OutputFile, '.xml', write, FilenameXML),
  Tree = [element(
    sbml,
    [
      xmlns ='http://www.sbml.org/sbml/level3/version1/core',
      level ='3',
      version ='1'
    ],
    [
      element(
        model,
        [id ='Model_generated_by_BIOCHAM'],
        Lists
      )
    ]
  )],
  debug(sbml, "Compartment", []),
  get_sbml_compartments_list(ListOfCompartments),
  single_model(Id_model),
  get_sbml_annotation_and_notes(Id_model, Notes_Annotation),
  debug(sbml, "Species", []),
  get_sbml_species_list(Species),
  make_sbml_list(Species, listOfSpecies, ListOfSpecies),
  debug(sbml, "Parameters", []),
  get_sbml_parameter_list(Parameters),
  make_sbml_list(Parameters, listOfParameters, ListOfParameters),
  debug(sbml, "Reactions", []),
  get_sbml_reaction_list(Reactions),
  make_sbml_list(Reactions, listOfReactions, ListOfReactions),
  debug(sbml, "Events", []),
  get_sbml_event_list(Events),
  make_sbml_list(Events, listOfEvents, ListOfEvents),
  debug(sbml, "Rules", []),
  get_sbml_rule_list(Rules),
  make_sbml_list(Rules, listOfRules, ListOfRules),
  setup_call_cleanup(
    open(FilenameXML, write, Stream, [encoding(utf8)]),
    (
      flatten([Notes_Annotation, ListOfCompartments, ListOfSpecies, ListOfParameters,
        ListOfReactions, ListOfEvents, ListOfRules], Lists),
      xml_write(Stream, Tree, [])
    ),
    close(Stream)
  ),!. % I don't know why this cut is needed but it avoids a choicepoint at export


make_sbml_list(L, Container, List) :-
  (
    L == []
  ->
    List = []
  ;
    List = element(Container, [], L)
  ).


%! get_sbml_compartments_list(Id, Notes_Annotation)
%
% Construct the list of the compartments of the model in the internal xml
% SWI-Prolog representation

get_sbml_compartments_list(ListOfCompartments) :-
  list_locations(My_locations),
  findall(
    Element,
    (
      member(Location-Volume, My_locations),
      Element = element(
        compartment,
        [id = Location, constant = 'false', size = Volume],
        [])
    ),
    List
  ),
  make_sbml_list(List, listOfCompartments, ListOfCompartments).


%! get_sbml_annotation_and_notes(Id, Notes_Annotation)
%
% provide the annotation/notes associated to a biocham item or [] if there is none

get_sbml_annotation_and_notes(Id, Notes_Annotation) :-
  ( % sbml annotation
    get_annotation(Id, sbml_annotation, Annotation)
  ->
    true
  ;
    Annotation = []
  ),
  ( % sbml notes
    get_annotation(Id, sbml_notes, String_notes)
  ->
    open_string(String_notes, Stream_notes),
    load_xml(Stream_notes, Notes, []),
    Notes = [element(notes, [], NoteList)],
    once(member(element(body,[_], SbmlNoteContent), NoteList))
  ;
    SbmlNoteContent = []
  ),
  ( % biocham notes
    get_annotation(Id, biocham_notes, _)
  ->
    findall(
        element(p, [], [Body]),
        get_annotation(Id, biocham_notes, Body),
        BiochamNoteContent
    )
  ;
    BiochamNoteContent = []
  ),
  append(SbmlNoteContent, BiochamNoteContent, All_notes),
  FormatedNotes = [element(notes,[],[
  element(body,[xmlns='http://www.w3.org/1999/xhtml'], All_notes)
  ])],
  append([FormatedNotes, Annotation], Notes_Annotation).

get_sbml_species_list(L) :-
  enumerate_molecules(M),
  get_initial_values(M, V),
  maplist(build_sbml_species, M, V, L).


build_sbml_species(Name, Conc, Species) :-
  debug(sbml, "Building: ~w", [Name]),
  item([kind:species, item: Name, id:Id_species]),
  ( % use the sbml_id if available
    get_annotation(Id_species, sbml_id, SId)
  ->
    assertz(sbml_id(Name, SId))
  ;
    count(sbml_ids, Id),
    atomic_list_concat(['s', Id], SId),
    assertz(sbml_id(Name, SId))
  ),
  (
    Name = Name_sbml@Comp
  ->
    true
  ;
    Name_sbml = Name,
    Comp = 'default_volume'
  ),
  get_sbml_annotation_and_notes(Id_species, Notes_Annotation),
  Species = element(
    species,
    [
      id = SId, compartment = Comp,
      initialConcentration = Conc, name = Name_sbml,
      hasOnlySubstanceUnits ='false',
      boundaryCondition ='false',
      constant ='false'
    ],
    Notes_Annotation
  ).


get_sbml_parameter_list(L) :-
  all_items([no_inheritance, kind: parameter], P),
  all_items([no_inheritance, kind: function], F),
  include(has_no_args, F, FF),
  append(P, FF, LL),
  ( % add default_volume if not present
      member(parameter(default_volume = _), LL)
  ->
      List = LL
  ;
      List = [parameter(default_volume=1)|LL]
  ),
  maplist(build_sbml_parameter, List, L).


build_sbml_parameter(Thing, Param) :-
  % count(sbml_ids, Id),
  (
    Thing = parameter(Name = Value)
  ->
    % atomic_list_concat(['p', Id], SId),
    sanitize_sbml(Name, SId),
    (
      item([kind: event, item: event(_Condition, Assignments)]),
      memberchk(Name = _, Assignments)
    ->
      Constant = 'false'
    ;
      Constant = 'true'
    ),
    Attrs = [
      value = Value,
      name = Name,
      constant = Constant
    ]
  ;
    Thing = function(Name = _Body),
    % atomic_list_concat(['f', Id], SId),
    sanitize_sbml(Name, SId),
    Attrs = [constant = 'false']
  ),
  assertz(sbml_id(Name, SId)),
  FullAttrs = [id = SId | Attrs],
  Param = element(
    parameter,
    FullAttrs,
    []
  ).


get_sbml_rule_list(L) :-
  all_items([no_inheritance, kind: function], F),
  % FIXME use functionDefinitions for the other ones
  include(has_no_args, F, FF),
  maplist(build_sbml_rule, [function(default_compartment=default_volume)| FF], L).


build_sbml_rule(function(Name = Body), Param) :-
  (
      sbml_id(Name, SId)
  ;
      Name = default_compartment,
      SId = default_compartment
  ),
  term_to_mathml(Body, Math),
  Param = element(
    assignmentRule,
    [
      variable = SId
    ],
    [element(
      math,
      [xmlns = 'http://www.w3.org/1998/Math/MathML'],
      [Math]
    )]
  ).


get_sbml_event_list(L) :-
  all_items([no_inheritance, kind: event], E),
  maplist(build_sbml_event, E, L).


build_sbml_event(event(Condition, AssignmentList), Event) :-
  term_to_mathml(Condition, Math),
  maplist(build_sbml_event_assignment, AssignmentList, MAssignmentList),
  Event = element(
    event,
    [
      useValuesFromTriggerTime = 'true'
    ],
    [
      element(
        trigger,
        [initialValue = 'false', persistent = 'false'],
        [
          element(
            math,
            [xmlns = 'http://www.w3.org/1998/Math/MathML'],
            [Math]
          )
        ]
      ),
      element(
        listOfEventAssignments,
        [],
        MAssignmentList
      )
    ]
  ).


build_sbml_event_assignment(Param = Value, Assignment) :-
  sbml_id(Param, SId),
  term_to_mathml(Value, MValue),
  Assignment = element(
    eventAssignment,
    [variable = SId],
    [
      element(
        math,
        [xmlns = 'http://www.w3.org/1998/Math/MathML'],
        [MValue]
      )
    ]
  ).


get_sbml_reaction_list(L) :-
  all_items([no_inheritance, kind: reaction], R),
  maplist(build_sbml_reaction, R, L).


build_sbml_reaction(Item, Reaction) :-
  count(sbml_ids, Id),
  atomic_list_concat(['r', Id], SId),
  reaction(
    Item,
    [name: Name, kinetics: Kinetics, reactants: Reactants, products: Products]
  ),
  (
    Name == ''
  ->
    SName = []
  ;
    SName = [Name]
  ),
  reaction_editor:factorize_solution(Reactants, FReactants),
  reaction_editor:factorize_solution(Products, FProducts),
  kinetics(FReactants, [], Kinetics, KineticsExpression),
  term_to_mathml(KineticsExpression, MKinetics),
  maplist(build_sbml_speciesref, FReactants, SReactants),
  make_sbml_list(SReactants, listOfReactants, ListOfReactants),
  maplist(build_sbml_speciesref, FProducts, SProducts),
  make_sbml_list(SProducts, listOfProducts, ListOfProducts),
  KineticLaw = element(
    kineticLaw,
    [],
    [
      element(
        math,
        [xmlns = 'http://www.w3.org/1998/Math/MathML'],
        [MKinetics]
      )
    ]
  ),
  item([id:ID_reaction, kind:reaction, item: Item]),
  get_sbml_annotation_and_notes(ID_reaction, Notes_Annotation),
  flatten([Notes_Annotation, ListOfReactants, ListOfProducts, KineticLaw], Content),
  Reaction = element(
    reaction,
    [id = SId, reversible = 'false', fast = 'false' | SName],
    Content
  ).


build_sbml_speciesref(S*M, SpeciesRef) :-
  !,
  sbml_id(M, SId),
  SpeciesRef = element(
    speciesReference,
    [
      species = SId,
      constant = 'true',
      stoichiometry = S
    ],
    []
  ).

build_sbml_speciesref(M, SpeciesRef) :-
  build_sbml_speciesref(1*M, SpeciesRef).


term_to_mathml(N, element(cn, [], [N])) :-
  number(N),
  !.

term_to_mathml(Name, element(ci, [], [SId])) :-
  sbml_id(Name, SId),
  !.

term_to_mathml([Name], element(ci, [], [SId])) :-
  sbml_id(Name, SId),
  !.

term_to_mathml(true, element(true, [], [])) :-
  !.

term_to_mathml(false, element(false, [], [])) :-
  !.

term_to_mathml('Time', element(csymbol, [
  encoding = 'text',
  definitionURL = 'http://www.sbml.org/sbml/symbols/time'
], ['Time'])) :-
  !.

term_to_mathml(if C then A else B, M) :-
  !,
  maplist(term_to_mathml, [A, B, C], [MA, MB, MC]),
  M = element(
    piecewise,
    [],
    [
      element(
        piece,
        [],
        [MA, MC]
      ),
      element(
        otherwise,
        [],
        [MB]
      )
    ]
  ).

term_to_mathml(min(A, B), M) :-
  !,
  term_to_mathml(if A <= B then A else B, M).

term_to_mathml(max(A, B), M) :-
  !,
  term_to_mathml(if A >= B then A else B, M).

term_to_mathml(C, element(apply, [], Contents)) :-
  C =.. [Functor | Args],
  functor_to_mathml(Functor, MFunctor),
  !,
  Contents = [element(MFunctor, [], []) | MArgs],
  maplist(term_to_mathml, Args, MArgs).


functor_to_mathml('*', times).

functor_to_mathml('/', divide).

functor_to_mathml('+', plus).

functor_to_mathml('-', minus).

functor_to_mathml('^', power).

functor_to_mathml(log, ln).

functor_to_mathml(exp, exp).

functor_to_mathml(sin, sin).

functor_to_mathml(cos, cos).

functor_to_mathml(or, or).

functor_to_mathml(and, and).

functor_to_mathml(not, not).

functor_to_mathml('=', eq).

functor_to_mathml('<', lt).

functor_to_mathml('<=', leq).

functor_to_mathml('>', gt).

functor_to_mathml('>=', geq).

% catchall, but maybe we should throw an error
functor_to_mathml(X, X).


has_no_args(function(Name = _Body)) :-
  atomic(Name),
  !.
 

