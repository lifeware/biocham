:- module(
  counters,
  [
    set_counter/2,
    count/2,
    peek_count/2
  ]
).

set_counter(CounterName, InitialValue) :-
  nb_setval(CounterName, InitialValue).


count(CounterName, Value) :-
  nb_getval(CounterName, Value),
  NextValue is Value + 1,
  nb_setval(CounterName, NextValue).


peek_count(CounterName, Value) :-
  nb_getval(CounterName, Value).
