:- module(
  kinetics,
  [
    kinetics/4,
   add_coefficient/2,
   is_kinetics_function/1
  ]
).

:- use_module(objects).
:- use_module(reaction_editor).

kinetics(Reactants, Inhibitors, Kinetics, Value) :-
  eval_kinetics(Reactants, Inhibitors, Kinetics, ValueToSimplify),
  simplify(ValueToSimplify, Value).


eval_kinetics(Reactants, _Inhibitors, single_reactant, Value) :-
  !,
  (
    Reactants = [Value]
  ->
    true
  ;
    throw(error(not_single_reactant_reaction))
  ).

eval_kinetics(Reactants, Inhibitors, product(Pattern in List, Expression), Value) :-
  !,
  (
    Pattern = S * O
  ->
    true
  ;
    S=1, O=Pattern %throw(error(invalid_pattern))
  ),
  (
    List = [reactants]
  ->
    make_product(Reactants, S, O, Expression, Value)
  ;
    (List = [inhibitors]
     ->
         maplist(add_coefficient, Inhibitors, CoeffInhibitors),
         make_product(CoeffInhibitors, S, O, Expression, Value)
     ;
     throw(error(invalid_list))
    )
  ).
  

eval_kinetics(Reactants, Inhibitors, sum(Pattern in List, Expression), Value) :-
  !,
  (
    Pattern = S * O
  ->
    true
  ;
    S=1, O=Pattern %throw(error(invalid_pattern))
  ),
  (
    List = [reactants]
  ->
    make_sum(Reactants, S, O, Expression, Value)
  ;
    (List = [inhibitors]
     ->
         maplist(add_coefficient, Inhibitors, CoeffInhibitors),
         make_sum(CoeffInhibitors, S, O, Expression, Value)
     ;
     throw(error(invalid_list))
    )
  ).
  
% This should have been already catch as a reactant, but it may be necessary
% to avoid expansion of Volume define as a function at numerical simulation
% initialization!
eval_kinetics(_Reactants, _Inhibitors, Name@Volume, Name@Volume) :-
  !.

eval_kinetics(Reactants, Inhibitors, FunctionApplication, Expression) :-
  function_apply(FunctionApplication, NewBody),
  !,
  eval_kinetics(Reactants, Inhibitors, NewBody, Expression).

eval_kinetics(Reactants, Inhibitors, Callable, Expression) :-
  term_morphism(kinetics:eval_kinetics(Reactants, Inhibitors), Callable, Expression).


make_product([], _S, _O, _Expression, 1).

make_product([RS * RO | Tail], S, O, Expression, Product) :-
  substitute([S, O], [RS, RO], Expression, SubstitutedExpression),
  Product = SubstitutedExpression * TailProduct,
  make_product(Tail, S, O, Expression, TailProduct).


make_sum([], _S, _O, _Expression, 0).

make_sum([RS * RO | Tail], S, O, Expression, Sum) :-
  substitute([S, O], [RS, RO], Expression, SubstitutedExpression),
  Sum = SubstitutedExpression + TailSum,
  make_sum(Tail, S, O, Expression, TailSum).


add_coefficient(C, 1 * C).


:-doc('Useful abbreviations for mass action law kinetics (with inhibitors), Michaelis-Menten kinetics, Hill kinetics (with inhibitors).').

% :- initial('function MA(k) = default_volume * k * product(S * M in [reactants], M ^ S)').
:- initial('function MA(k) = MA(default_volume, k)').

:- initial('function MA(Vm, k) = Vm * k * product(S * M in [reactants], M ^ S)').

:- initial('function MAI(k) = k * default_volume * product(S * M in [reactants], M ^ S) / (1 + sum(M in [inhibitors], M))').

:- initial('function MAI(Vm, k) = Vm * k * product(S * M in [reactants], M ^ S) / (1 + sum(M in [inhibitors], M))').


:- initial('
  function MM(Velocity, Km) = default_volume * Velocity * single_reactant / (Km + single_reactant)
	  ').

:- initial('
  function MM(Volume, Velocity, Km) = Volume * Velocity * single_reactant / (Km + single_reactant)
	  ').

%:- initial('
%  function MM(Vm, Km) = Vm * product(S * M in [reactants], M ^ S) / (Km + product(S * M in [reactants], M ^ S))
%').


:- initial('
  function Hill(Vm, Km, n) =
    Vm * single_reactant ^ n / (Km ^ n + single_reactant ^ n)
').

%:- initial('
%  function Hill(Vm, Km, n) =
%    Vm * product(S * M in [reactants], M ^ (S * n)) / (Km ^ n + product(S * M in [reactants], M ^ (S * n))).
%').

:- initial('function HillI(Vm, Km, n) = Vm * single_reactant ^ n / (Km ^ n + single_reactant ^ n + sum(M in [inhibitors], M ^ n))').

%:- initial('function HillI(Vm, Km, n) = Vm * product(S * M in [reactants], M ^ (S * n)) / (Km ^ n + product(S * M in [reactants], M ^ (S * n)) + sum(M in [inhibitors], M ^ n))').



%! is_kinetics_function(F)
%
% Manually check if F is a kinetic function

is_kinetics_function(Head) :-
  Head = 'MA'(k);
  Head = 'MA'('Vm', k);
  Head = 'MAI'(k);
  Head = 'MAI'('Vm', k);
  Head = 'MM'('Velocity', 'Km');
  Head = 'MM'('Volume', 'Velocity', 'Km');
  Head = 'Hill'('Vm', 'Km', n);
  Head = 'HillI'('Vm', 'Km', n).
