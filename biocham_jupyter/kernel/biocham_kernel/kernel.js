define(
    [
        'codemirror/lib/codemirror',
        'codemirror/addon/mode/simple',
        'kernelspecs/biocham/commands',
        'base/js/namespace',
        'base/js/events',
        'base/js/utils'
    ],
    function(CodeMirror, Simple, Commands, Jupyter, events, utils) {

        var onload = function() {
            $.ajaxSetup({ cache: true });

            try {
                const biochamVersion =
                      Jupyter.notebook.kernel.info_reply.banner
                      .split('\n')[0].split(' ')[1];
                const docLink =
                      'http://lifeware.inria.fr/biocham4/doc_v'
                      + biochamVersion + '/index.html';
                
            $('#kernel_logo_widget').wrap(function() {
                var link = $('<a/>');
                link.attr('href', docLink);
                link.attr('target', '_blank');
                link.attr('title', 'Biocham Manual');
                link.text($(this).text());
                return link;
            });

            } catch(e) {
                events.on('kernel_ready.Kernel', function() {
                    const biochamVersion =
                          Jupyter.notebook.kernel.info_reply.banner
                          .split('\n')[0].split(' ')[1];
                    const docLink =
                          'http://lifeware.inria.fr/biocham4/doc_v'
                          + biochamVersion + '/index.html';
                     
                    $('#kernel_logo_widget').wrap(function() {
                        var link = $('<a/>');
                        link.attr('href', docLink);
                        link.attr('target', '_blank');
                        link.attr('title', 'Biocham Manual');
                        link.text($(this).text());
                        return link;
                    });
                });
            }
                         

            if (Jupyter.notebook.get_cells().length === 1) {
                events.on('kernel_ready.Kernel', function() {
                    Jupyter.notebook.insert_cell_above('markdown', 0).set_text(
                        Jupyter.notebook.kernel.info_reply.banner
                    );
                    Jupyter.notebook.get_cell(0).execute();
                });
            }

            console.info('Loading biocham codemirror mode');
            CodeMirror.defineSimpleMode("biocham", {
                // The start state contains the rules that are intially used
                start: [
                    // The regex matches the token, the token property contains
                    // the type
                    {regex: /"(?:[^\\]|\\.)*?(?:"|$)/, token: "string"},
                    // Rules are matched in the order in which they appear
                    {regex: Commands.commands, token: "keyword"},
                    {regex: /integral/, token: "keyword"},
                    {regex: /@|::|[-+\/*=<>!]+/, token: "operator"},
                    {regex: /'(?:[^\\]|\\.)*?'/, token: "atom"},
                    {regex: /[a-zA-Z][\w]*:\s/, token: "attribute"},
                    {regex: /[a-zA-Z][\w]*/, token: "variable"},
                    {regex: /0x[a-f\d]+|[-+]?\b(?:\.\d+|\d+\.?\d*)(?:e[-+]?\d+)?/i,
                     token: "string-2"},
                    {regex: /%.*/, token: "comment"},
                    // indent and dedent properties guide autoindentation
                    {regex: /[\{\[\(]/, indent: true},
                    {regex: /[\}\]\)]/, dedent: true},
                ],
                // The meta property contains global information about the mode. It
                // can contain properties like lineComment, which are supported by
                // all modes, and also directives like dontIndentStates, which are
                // specific to simple modes.
                meta: {
                    dontIndentStates: ["comment"],
                    lineComment: "%"
                }
            });

            if (Jupyter.notebook.codemirror_mode !== "biocham") {
                // empty callback method to avoid error
                utils.requireCodeMirrorMode("biocham", () => {});
            }
        };
        
        return { onload: onload };
    });
