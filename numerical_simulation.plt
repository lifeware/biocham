:- use_module(library(plunit)).

:- begin_tests(numerical_simulation, [setup((clear_model, reset_options))]).


test('mapk', [condition(flag(slow_test, true, true))]) :-
  clear_model,
  command(load(library:examples/mapk/mapk)),
  command(numerical_simulation(time:100)).

test('mapk Rosenbrock', [condition(flag(slow_test, true, true))]) :-
  clear_model,
  command(load(library:examples/mapk/mapk)),
  command(numerical_simulation(time:100, method: rsbk)).

test('Time in kinetics') :-
  command('Time' for '_' => 'A'),
  command(numerical_simulation(time: 2)).

test('events') :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 1)),
  command(add_event(b > 0.5, k = 0)),
  command(add_event('Time' > 1, k = 0)),
  command(present(a)),
  command(numerical_simulation(time: 2)).

test('events species') :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 1)),
  command(add_event(b > 0.5, a = 1)),
  command(present(a)),
  command(numerical_simulation(time: 5)).

test('events Rosenbrock') :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 1)),
  command(add_event(b > 0.5, k = 0)),
  command(present(a)),
  command(numerical_simulation(method: rsbk)).

test('events species Rosenbrock') :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 1)),
  command(add_event(b > 0.5, a = 1)),
  command(present(a)),
  command(numerical_simulation(method: rsbk)).

test('events ssa') :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 1)),
  command(add_event(b > 0.5, k = 0)),
  command(present(a)),
  command(numerical_simulation(method: ssa)).

test('events species ssa') :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 1)),
  command(add_event(b > 0.5, a = 1)),
  command(present(a)),
  command(numerical_simulation(method: ssa)).

test('conditional') :-
  clear_model,
  command(if a < 0.5 then 'MA'(2) else 'MA'(1) for a => b),
  command(present(a)),
  command(numerical_simulation(initial_step_size:0.001)).

test('conditional Rosenbrock') :-
  clear_model,
  command(if a < 0.5 then 'MA'(2) else 'MA'(1) for a => b),
  command(present(a)),
  command(numerical_simulation(initial_step_size:0.001 ,method:rsbk)).

test('test_diffusion') :-
  clear_model,
  command('MA'(v1) for a@v1 <=> a@v2),
  command(parameter(v1 = 1, v2 = 10)),
  command(initial_state(a@v1 = 1)),
  command(numerical_simulation(method:bsimp)),
  get_last_state(_, [Cbs1, Cbs2]),
  2*abs(Cbs1 - Cbs2)/(Cbs1 + Cbs2) =< 1e-6, % allows small errors
  abs(Cbs1 - 1/11) =< 1e-6, % allows small errors
  command(numerical_simulation(method:rsbk)),
  get_last_state(_, [Crs1, Crs2]),
  abs(Crs1 - 1/11) =< 1e-6, % allows small errors
  2*abs(Crs1 - Crs2)/(Crs1 + Crs2) =< 1e-6. % allows small errors

test('Diffusion with time varying volumes') :-
  clear_model,
  command('MA'(v1) for a@v1 <=> a@v2),
  command(parameter(v1 = 1)),
  command(function(v2 = 1+'Time')),
  command(initial_state(a@v1 = 1)),
  command(numerical_simulation(method:bsimp)),
  command(numerical_simulation(method:rsbk)).
  
test('test_dilution') :-
  clear_model,
  command('MA'(v1) for a@v1 => a@v2),
  command(parameter(v1 = 10, v2 = 1)),
  command(initial_state(a@v1 = 1)),
  command(numerical_simulation(time:1000, method:bsimp)),
  get_last_state(_, [_Cbs1, Cbs2]),
  abs(Cbs2-10) =< 1e-6. % allows small errors



:- end_tests(numerical_simulation).
