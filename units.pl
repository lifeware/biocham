:- module(units,
  [
    unit_type/1,
    set_units/2,
    list_units/0,
    get_axes_names/3,
    list_dimensions/0,
    set_dimension/3,
    clear_dimensions/0,
    clear_dimension/1
  ]
).

:- use_module(doc).
:- use_module(library(clpfd)).

:- grammar(unit_type).

unit_type(time).

unit_type(volume).

unit_type(substance).


%! set_units(+Type, +Unit) is det.
%
% changes the default Type unit to Unit.
set_units(Type, Unit) :-
  biocham_command,
  type(Type, unit_type),
  type(Unit, name),
  doc('Set the default units for the given type (time, volume or substance) in the current model.'),
  change_item([], unit, Type, Unit).


%! default_unit(+Type, +Unit) is det.
%
% defines the default units for each possible type
default_unit(time, 's').

default_unit(volume, 'l').

default_unit(substance, 'mol').


%! get_units(+Type, -Unit) is det.
%
% unifies Unit with the current default unit of type Type.
get_units(Type, Unit) :-
  (
    item([kind: unit, key: Type, item: Unit])
  ->
    true
  ;
    default_unit(Type, Unit)
  ).


%! list_units is det.
%
% Lists the values of all current default units for all types.
list_units :-
  biocham_command,
  doc('Lists the default units of the current model.'),
  forall(
    unit_type(Type),
    (
      get_units(Type, Unit),
      format("~w: ~w~n", [Type, Unit])
    )
  ).


%! get_axes(+Against, -XAxis, -YAxis) is det.
%
% unifies XAxis and YAxis with atoms corresponding to the correct axes names

get_axes_names(Against, XAxis, YAxis) :-
  get_option(display_concentration_amount, DCA),
  units:get_units(substance, SubstUnit),
  (
    (
      numerical_simulation:last_method(Method),
      memberchk(Method, [ssa, spn, pn, sbn, bn])
    ;
      DCA = amount
    )
  ->
    YAxis = SubstUnit
  ;
    units:get_units(volume, VolUnit),
    format(atom(YAxis), "~w/~w", [SubstUnit, VolUnit])
  ),
  (
    Against == 'Time'
  ->
    units:get_units(time, TimeUnit),
    format(atom(XAxis), "Time (~w)", [TimeUnit])
  ;
    format(atom(XAxis), "~w (~w)", [Against, YAxis])
  ).


% store declared dimensions
:- dynamic(k_dimension/3).

:- biocham_silent(clear_model).


%! list_dimensions.
%
% biocham command
%
% Infers dimensions (time and volume, dimensionless) of all parameters of the model.
%
% Molecular concentration have dimension volume^-1
% Reaction rates (and influence forces) have dimension time^-1
%
% A dimension is encoded by a couple (time exponent, volume exponent)
% No dimension is encoded by the couple (0, 0)
% Any dimension is encoded by a variable
% Numbers have no dimension

:- doc('
   Dimensional analysis infers dimensions for model parameters and checks their consistency.
   In Biocham, only time and volume dimensions are considered.
   The dimension of a molecular concentration is volume$^{-1}$.
   Warning: numbers have no dimension so kinetic parameters should be used in kinetic expressions instead of directly numbers.
').


list_dimensions :-
  biocham_command,
  doc('
       Infers the time and volume dimensions of all parameters according to ODEs.
     An error is raised if some parameters appear in expressions with inconsistent dimensions.
     This can happen in a model if some  multiplicative parameter with value equal to 1 is omitted for example.
    \\begin{example}
    \\trace{
      biocham: MM(v, k) for A => B.
      biocham: parameter(k = 2, v = 3).
      biocham: list_dimensions.
    }
    \\end{example}
  '),
  findall((_ - P), item([kind: parameter, item: parameter(P=_)]), L),
  check_set_dim(L),
  with_current_ode_system(findall(E, ode(_, E), ExprList)),
  find_parameter_dim_rec(ExprList, L),
  !,
  keysort(L, LL),
  write_dimensions(LL).

list_dimensions :-
  print_message(error, no_satisfy),
  nl,
  nb_getval(current_dim_term, T),
  nb_getval(current_dim_expr, E),
  print_message(error, failed_dimension(T, E)).


set_dimension(P, U, V) :-
  biocham_command,
  doc('Declare dimension time^\\argument{U} . volume^\\argument{V}
    for parameter \\argument{P}'),
  type(P, parameter_name),
  type(U, number),
  type(V, number),
  retractall(k_dimension(P, _, _)),
  assertz(k_dimension(P, U, V)).


clear_dimensions :-
  biocham_command,
  doc('Clear all declared dimensions'),
  retractall(k_dimension(_ , _, _)).


clear_dimension(P) :-
  biocham_command,
  doc('Clear declared dimension for parameter \\argument{P}'),
  type(P, parameter_name),
  retractall(k_dimension(P, _, _)).


check_set_dim([]).

check_set_dim([((U, V) - P) | L]) :-
  (
    k_dimension(P, U, V),
    !
  ;
    true
  ),
  check_set_dim(L).


find_parameter_dim_rec([], _).

find_parameter_dim_rec([0 | EL], L) :-
  % no kinetics, i.e. constant species
  print_message(warning, constant_species),
  find_parameter_dim_rec(EL, L).

find_parameter_dim_rec([E | EL], L) :-
  debug(dims, "Inferring for ~w with ~w", [E, L]),
  % all kinetics are Time^(-1) Volume^(-1)
  nb_setval(current_dim_expr, E),
  nb_setval(current_dim_term, E),
  find_parameter_dim(E, (-1, -1), L),
  find_parameter_dim_rec(EL, L).


find_parameter_dim(A, Dim, L) :-
  debug(dims, "checking ~w for ~w with ~w", [A, Dim, L]),
  fail.

find_parameter_dim(X + Y, Dim, L) :-
  !,
  find_parameter_dim(X, Dim, L),
  find_parameter_dim(Y, Dim, L).

find_parameter_dim(X - Y, Dim, L) :-
  !,
  find_parameter_dim(X, Dim, L),
  find_parameter_dim(Y, Dim, L).

find_parameter_dim(min(X,Y), Dim, L) :-
  !,
  find_parameter_dim(X, Dim, L),
  find_parameter_dim(Y, Dim, L).

find_parameter_dim(max(X,Y), Dim, L) :-
  !,
  find_parameter_dim(X, Dim, L),
  find_parameter_dim(Y, Dim, L).

find_parameter_dim(-Y, Dim, L)  :-
  !,
  find_parameter_dim(Y, Dim, L).

% forces exponents to be dimensionless
find_parameter_dim(X ^ Y, (DimT, DimV), L) :-
  !,
  find_parameter_dim(X, (DT, DV), L),
  find_parameter_dim(Y, (DYT, DYV), L),
  DYT #= 0,
  DYV #= 0,
  (
    (
      number(Y),
      V = Y,
      DT1 = DimT,
      DV1 = DimV,
      DT2 = DT,
      DV2 = DV
    ;
      parameter_value(Y, V),
      number(V),
      DT1 = DimT,
      DV1 = DimV,
      DT2 = DT,
      DV2 = DV
    ;
      Y = 1/Z, 
      DT1 = DT,
      DV1 = DV,
      DT2 = DimT,
      DV2 = DimV,
      (
        number(Z),
        V = Z
      ;
        parameter_value(Z, V),
        number(V)
      )
    )
  ->
    (
      integer(V)
    ->
      DT1 #= DT2 * V,
      DV1 #= DV2 * V
    ;
      DT #= 0,
      DV #= 0,
      DimT #= 0,
      DimV #= 0
    )
  ).

% forces Dim=0 for other functions than addition and multiplication
find_parameter_dim(log(X), (DimT, DimV), L) :-
  !,
  nb_setval(current_dim_term, log(X)),
  DimT #= 0,
  DimV #= 0,
  find_parameter_dim(X, (DimT, DimV), L).

find_parameter_dim(exp(X), (DimT, DimV), L) :-
  !,
  nb_setval(current_dim_term, exp(X)),
  DimT #= 0,
  DimV #= 0,
  find_parameter_dim(X, (DimT, DimV), L).

find_parameter_dim(sin(X), (DimT, DimV), L) :-
  !,
  nb_setval(current_dim_term, sin(X)),
  DimT #= 0,
  DimV #= 0,
  find_parameter_dim(X, (DimT, DimV), L).

find_parameter_dim(cos(X), (DimT, DimV), L) :-
  !,
  nb_setval(current_dim_term, cos(X)),
  DimT #= 0,
  DimV #= 0,
  find_parameter_dim(X, (DimT, DimV), L).

find_parameter_dim(N, (DimT, DimV), _) :-
  number(N),
  !,
  DimT #= 0,
  DimV #= 0.

find_parameter_dim('Time', (DimT, DimV), _) :-
  !,
  nb_setval(current_dim_term, 'Time'),
  DimT #= 1,
  DimV #= 0.

find_parameter_dim(X * Y, (DimT, DimV), L) :-
    !,
  find_parameter_dim(X, (DT1, DV1), L),
  find_parameter_dim(Y, (DT2, DV2), L),
  nb_setval(current_dim_term, X * Y),
  DimT #= DT1 + DT2,
  DimV #= DV1 + DV2.

find_parameter_dim(X / Y, (DimT, DimV), L) :-
  !,
  find_parameter_dim(X, (DT1, DV1), L),
  find_parameter_dim(Y, (DT2, DV2), L),
  nb_setval(current_dim_term, X / Y),
  DimT #= DT1 - DT2,
  DimV #= DV1 - DV2.

find_parameter_dim(if(then(_C, else(A, B))), Dim, L) :-
  !,
  find_parameter_dim(A, Dim, L),
  find_parameter_dim(B, Dim, L).

find_parameter_dim([M], (DimT, DimV), _) :-
  !,
  nb_setval(current_dim_term, [M]),
  DimT #= 0,
  DimV #= -1.

find_parameter_dim(M, (DimT, DimV), _) :-
  enumerate_molecules(L),
  member(M, L),
  !,
  DimT #= 0,
  DimV #= -1.

find_parameter_dim(P, (DimT, DimV), L) :-
  parameter_value(P, _), %FF should check the expression
  !,
  (
    member(((DT, DV) - P), L)
  ->
    DimT #= DT,
    DimV #= DV
  ;
    print_message(warning, uninitialized_parameter(P)) %FF the dimensions for P should be added to L (using a difference list)
  ).




write_dimensions([]).

write_dimensions([((DT, DV) - P) | L]) :-
  (
    number(DV),
    number(DV)
  ->
    format("~w has dimension time^(~w).volume^(~w)~n", [P, DT, DV])
  ;
    debug(dims, "~w has unknown dimension: ~w", [P, (DT, DV)]),
    format("~w has unknown dimension~n", [P])
  ),
  write_dimensions(L).


prolog:message(constant_species) -->
  ['There are molecular species without kinetics in this model'-[]].

prolog:message(no_satisfy) -->
  ['Could not satisfy all constraints on parameter dimensions.'-[]].

prolog:message(failed_dimension(T, E)) -->
  ['Failed on ~w in ~w~na kinetic expression which should be time^-1'-[T, E]].
