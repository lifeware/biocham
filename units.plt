:- use_module(library(plunit)).
:- use_module(reaction_rules).

:- begin_tests(units, [setup((clear_model, reset_options))]).


test(
  'infers MA',
  [
    setup(clear_model),
    cleanup(clear_model),
    true(Dimension == (-1, 0))
  ]
) :-
  command(add_reaction('MA'(k) for a => b)),
  command(parameter(k=1)),
  units:find_parameter_dim_rec([k*a], [Dimension-k]).


test(
  'infers MM',
  [
    setup(clear_model),
    cleanup(clear_model),
    true([DV, DK] == [(-1, -1), (0, -1)])
  ]
) :-
  command(add_reaction('MM'(v, k) for a => b)),
  command(parameter(v=1, k=1)),
  units:find_parameter_dim_rec([v*a/(k + a)], [DK-k, DV-v]),
  !.


test(
  'fails MA MM',
  [
    setup(clear_model),
    cleanup(clear_model),
    fail
  ]
) :-
  command(add_reaction('MM'(v, k) for a => b)),
  command(parameter(v=1, k=1)),
  units:find_parameter_dim_rec([v*a/(k + a), k*a], [_K-k, _V-v]).
