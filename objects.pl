:- module(
  objects,
  [
    concentration/1,
    name/1,
    identifier/1,
    parameter_name/1,
    function_prototype/1,
    object/1,
    op(550, yfx, @),
    op(720, fy, not),
    op(740, xfy, and),
    op(760, xfy, or),
    op(780, fy, if),
    op(780, xfy, then),
    op(780, xfy, else),
    op(700, xfy, =),
    op(700, xfy, <=),
    op(600, xfx, in),
    arithmetic_expression/1,
    patch_arithmetic_expression/2,
    condition/1,
    op(100, xfy, '~'),
    yesno/1
  ]
).


% Only for separate compilation/linting
:- use_module(doc).

:- devdoc('\\section{Grammars}').

:- doc('The syntax of elementary types in Biocham is the following:').

:- grammar(concentration).


concentration(Number) :-
  arithmetic_expression(Number).


:- grammar(time).


time(Number) :-
  number(Number).


:- grammar(name).


name(Name) :-
  atom(Name).


:- grammar(identifier).


identifier(Name@Location) :-
  name(Name),
  name(Location).

identifier(Name) :-
  name(Name).


:- grammar(parameter_name).


parameter_name(Name) :-
  identifier(Name).


:- grammar(function_prototype).


function_prototype(FunctionPrototype) :-
  term(FunctionPrototype).


:- grammar(object).


object(Name) :-
  identifier(Name).


:- grammar(arithmetic_expression).


arithmetic_expression([A]) :-
  object(A).

arithmetic_expression(+ ArithmeticExpression0) :-
  arithmetic_expression(ArithmeticExpression0).

arithmetic_expression(- ArithmeticExpression0) :-
  arithmetic_expression(ArithmeticExpression0).

arithmetic_expression(ArithmeticExpression0 + ArithmeticExpression1) :-
  arithmetic_expression(ArithmeticExpression0),
  arithmetic_expression(ArithmeticExpression1).

arithmetic_expression(ArithmeticExpression0 - ArithmeticExpression1) :-
  arithmetic_expression(ArithmeticExpression0),
  arithmetic_expression(ArithmeticExpression1).

arithmetic_expression(ArithmeticExpression0 * ArithmeticExpression1) :-
  arithmetic_expression(ArithmeticExpression0),
  arithmetic_expression(ArithmeticExpression1).

arithmetic_expression(ArithmeticExpression0 / ArithmeticExpression1) :-
  arithmetic_expression(ArithmeticExpression0),
  arithmetic_expression(ArithmeticExpression1).

arithmetic_expression(ArithmeticExpression0 ^ ArithmeticExpression1) :-
  arithmetic_expression(ArithmeticExpression0),
  arithmetic_expression(ArithmeticExpression1).

arithmetic_expression(min(ArithmeticExpression0, ArithmeticExpression1)) :-
  arithmetic_expression(ArithmeticExpression0),
  arithmetic_expression(ArithmeticExpression1).

arithmetic_expression(max(ArithmeticExpression0, ArithmeticExpression1)) :-
  arithmetic_expression(ArithmeticExpression0),
  arithmetic_expression(ArithmeticExpression1).

arithmetic_expression(log(ArithmeticExpression)) :-
  arithmetic_expression(ArithmeticExpression).

arithmetic_expression(exp(ArithmeticExpression)) :-
  arithmetic_expression(ArithmeticExpression).

arithmetic_expression(cos(ArithmeticExpression)) :-
  arithmetic_expression(ArithmeticExpression).

arithmetic_expression(sin(ArithmeticExpression)) :-
  arithmetic_expression(ArithmeticExpression).

arithmetic_expression(sqrt(ArithmeticExpression)) :-
  arithmetic_expression(ArithmeticExpression).

arithmetic_expression(floor(ArithmeticExpression)) :-
  arithmetic_expression(ArithmeticExpression).

arithmetic_expression(ceiling(ArithmeticExpression)) :-
  arithmetic_expression(ArithmeticExpression).

arithmetic_expression(product(Pattern in List, ArithmeticExpression)) :-
  term(Pattern),
  term(List),
  arithmetic_expression(ArithmeticExpression).

arithmetic_expression(if Condition then IfTrue else IfFalse) :-
  condition(Condition),
  arithmetic_expression(IfTrue),
  arithmetic_expression(IfFalse).

arithmetic_expression(piecewise(IfTrue, Condition, IfFalse)) :-
  condition(Condition),
  arithmetic_expression(IfTrue),
  arithmetic_expression(IfFalse).

arithmetic_expression(Number) :-
  number(Number).

arithmetic_expression(FunctionApplication) :-
  function_application(arithmetic_expression, FunctionApplication).

arithmetic_expression(Identifier) :-
  identifier(Identifier).

:- doc('Note the syntax if/then/else to encode piecewise expressions.').


patch_arithmetic_expression(
  if Condition then IfBody,
  if Condition then IfTrue else IfFalse
) :-
  !,
  (
    extract_if_body(IfBody, IfTrue, IfFalse)
  ->
    true
  ;
    throw(
      error(
        illformed_conditional(if Condition then IfBody),
        patch_arithmetic_expression
      )
    )
  ).

patch_arithmetic_expression(Expression, Expression).



:- grammar(condition).


condition(true).

condition(false).

condition(ArithmeticExpression0 = ArithmeticExpression1) :-
  arithmetic_expression(ArithmeticExpression0),
  arithmetic_expression(ArithmeticExpression1).

condition(ArithmeticExpression0 < ArithmeticExpression1) :-
  arithmetic_expression(ArithmeticExpression0),
  arithmetic_expression(ArithmeticExpression1).

condition(ArithmeticExpression0 <= ArithmeticExpression1) :-
  arithmetic_expression(ArithmeticExpression0),
  arithmetic_expression(ArithmeticExpression1).

condition(ArithmeticExpression0 > ArithmeticExpression1) :-
  arithmetic_expression(ArithmeticExpression0),
  arithmetic_expression(ArithmeticExpression1).

condition(ArithmeticExpression0 >= ArithmeticExpression1) :-
  arithmetic_expression(ArithmeticExpression0),
  arithmetic_expression(ArithmeticExpression1).

condition(Condition0 or Condition1) :-
  condition(Condition0),
  condition(Condition1).

condition(Condition0 and Condition1) :-
  condition(Condition0),
  condition(Condition1).

condition(not Condition) :-
  condition(Condition).


:- grammar(yesno).


yesno(yes).

yesno(no).


:- devdoc('\\section{Internal predicates}').

% redefine the way things are printed by prolog
% necessary since we go through portray to avoid quoting capital letters 'A'

user:portray(not(Thing)) :-
  format("not ~p",[Thing]).

user:portray(if(Thing)) :-
  format("if ~p",[Thing]).


extract_if_body(IfTrue else IfFalse, IfTrue, IfFalse).

extract_if_body(
  if Condition then Cont,
  if Condition then SubTrue else SubFalse,
  Tail
) :-
  extract_if_body(Cont, SubTrue, SubTail),
  extract_if_body(SubTail, SubFalse, Tail).
