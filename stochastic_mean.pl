:- module(
  stochastic_mean,
  [
    % Commands
    test_stochastic_mean_ode/0
  ]
).

:- use_module(library(clpfd)).



:- doc("Biocham can check sufficient conditions to ensure that, for some molecular species of the current reaction model, the mean stochastic trace is given by the ODE simulation trace, at all time points, and for any conversion factor (i.e. with small molecule numbers, unlike Kurtz's limit theorem). The command returns the list of molecular species for which that property is guaranteed to hold. The condition basically checks that those species are produced by reactions with mass action law kinetics and that polymolecular reactions are restricted to catalytic synthesis reactions with disjoint sets of ancestor species.").

:- devdoc("Could labeling enumerate alternative solutions? We assume not. Exercise: prove that those Boolean variables in Satisfied that have not been instanciated by constraint propagation can always be instanciated to 1.").


test_stochastic_mean_ode :-
  biocham_command,
  doc('Lists the molecular species of the current reaction model for which the ODE simulation trace is guaranteed to be equal to the mean stochastic trace at all time points, for any conversion factor. This is always the case of the strict input species (see \\command{list_strict_input_species/0}) which are listed apart.'),
  enumerate_molecules(List_Molecule),
  (
   List_Molecule = []
  ->
   format("There is currently no model to work on!~n", [])
  ;
   reactions_with_species(List_Molecule, List_Reaction),
   (
    test_stochastic_mean(List_Molecule, List_Reaction, Satisfied)
   ->
    stat(Satisfied, List_Molecule, Ratio, Species, I, Inputs, ProperSet),
    (
     Ratio=(0/_)
    ->
     format("Undecided: sufficient condition is not satisfied on any molecular species.~n", [])
    ;
     Ratio=(N/N)
    ->
     format("Condition satisfied on all ~w species.~n", [N])
    ;
     Ratio=(I/_)
    ->
     format("Condition satisfied on ~w species, all strict input species only: ~w~n", [Ratio, Species])
    ;
     I==0
    ->
     format("Condition satisfied on ~w species: ~w, there is no strict input species~n", [Ratio, Species])
    ;
     format("Condition satisfied on ~w species: ~w plus all (~w) strict input species ~w~n", [Ratio, ProperSet, I, Inputs])
    )
   ;
    format("Error: failure to check the condition.~n", [])
   )
  ).

stat(Satisfied, List_Molecule, R/N, Species, I, Inputs, ProperSet):-
    length(List_Molecule, N),
    sum_list(Satisfied, R),
    findall(Name, (nth1(I, Satisfied, 1), nth1(I, List_Molecule, Name)), Species),
    
    %my_strict_input_species_1(Inputs),
    strict_input_species(Inputs),
    length(Inputs,I),
    subtract(Species, Inputs, ProperSet).


%! test_stochastic_mean(+List_Molecule, +List_Reaction)
%
% Tests a graphical condition of separation of the reactants and reactant Ancestors of multimolecular reactions
% Mass action law kinetics is checked.
% Associate one boolean vector to each molecular species representing the set of its ancestors
% Express ancestor set inclusion and empty intersection by simple boolean constraints using FD solver


test_stochastic_mean(List_Molecule, List_Reaction, Satisfied):-
    length(List_Molecule, N),
    
    length(Satisfied, N),
    Satisfied ins 0..1,	% satisfaction or not of the condition for each variable
    
    %my_strict_input_species_1(Inputs), % strict input species with stochiometry equal to 1 satisfy the condition
    strict_input_species(Inputs), % strict input species satisfy the condition (whatever stoichiometry) 
    for_all(B, Name^I^(member(Name, Inputs), nth1(I, List_Molecule, Name), nth1(I, Satisfied, B)), '='(1)),

    length(Ancestors, N), % boolean matrix of ancestors
    ancestors(Ancestors, 1, N),

    reaction_constraints(List_Reaction, List_Molecule, Ancestors, Satisfied),

    
    labeling([down], Satisfied).


my_strict_input_species_1(L):-
    enumerate_molecules(Molecules),
    realsetof(M, stochastic_mean:is_strict_input_1(M, Molecules), L).

is_strict_input_1(M, Molecules):-
  member(M, Molecules),
  \+ (
      is_strict_reactant(M,_)
     ),
  \+ (
      is_strict_product(M,_)
     ),
  \+ (
      is_poly_reactant(M,_)
     ),
  \+ (
     is_target(M,_)
    ).


ancestors([], _, _).
ancestors([A | Ancestors], I, N):-
    length(A, N),
    A ins 0..1,
    nth1(I, A, 1), % self ancestor diagonal
    I1 is I+1,
    ancestors(Ancestors, I1, N).

:- devdoc('To generalize to influence rules as well.').

reaction_constraints([], _List_Molecule, _Ancestors, _Satisfied).
reaction_constraints([(F for R => P) | List_Reaction], List_Molecule, Ancestors, Satisfied):-
    (
     F='MA'(_)
    ->
     (
      R='_'
     ->
      true
     ;
      solution_to_list(R, Rlist),
      solution_to_list(P, Plist),
      Bool in 0..1, % satisfaction of the condition on the reactants
      (
       Rlist=[1*_]
      ->
       ancestor_constraints(Rlist, Plist, List_Molecule, Ancestors, Bool),
       % dependency of the satisfaction of the condition on the products to the reactants through variable Bool
       for_all(B, Name^I^(member((_*Name), Plist), nth1(I, List_Molecule, Name), nth1(I, Satisfied, B)), '#>='(Bool))
      ;
       % tests that reactants are catalysts with stoichiometric coefficients = 1 and returns products
       subtract_1(Rlist, Plist, Products) 
      ->
       ancestor_constraints(Rlist, Products, List_Molecule, Ancestors, Bool),
       for_all(B, Name^I^(member((_*Name), Products), nth1(I, List_Molecule, Name), nth1(I, Satisfied, B)), '#>='(Bool))
      ;
       % only strict catalysts might satisfy the condition
       subtract(Rlist, Plist, Reactants),
       for_all(B, Name^I^(member((_*Name), Reactants), nth1(I, List_Molecule, Name), nth1(I, Satisfied, B)), '='(0)),
       subtract(Plist, Rlist, Products),
       for_all(B, Name^I^(member((_*Name), Products), nth1(I, List_Molecule, Name), nth1(I, Satisfied, B)), '='(0))
      )
     )
    ;
     % condition unsatisfied on all reactants and products except strict catalysts
     unsatisfied_except_strict_catalysts(R, P, List_Molecule, Satisfied)
    ),
    reaction_constraints(List_Reaction, List_Molecule, Ancestors, Satisfied).


unsatisfied_except_strict_catalysts(R, P, List_Molecule, Satisfied):-
    solution_to_list(R, Rlist),
    solution_to_list(P, Plist),
    intersection(Rlist, Plist, Clist),
    union(Rlist, Plist, Alist),
    subtract(Alist, Clist, List),
    for_all(B, Name^I^(member((_*Name), List), nth1(I, List_Molecule, Name), nth1(I, Satisfied, B)), '='(0)).


subtract_1([], Plist, Plist).
subtract_1([E | L], Plist, Products):-
    E=(1*_),
    member(E, Plist),
    delete(Plist, E, Nlist),
    subtract_1(L, Nlist, Products).


% constraints on the ancestors of the reactants to satisfy the condition on the products (with variable Bool)
ancestor_constraints([], _Products, _List_Molecule, _Ancestors, _Bool).
ancestor_constraints([E | Rlist], Products, List_Molecule, Ancestors, Bool):-
    E=(_*R),
    nth1(I, List_Molecule, R),
    nth1(I, Ancestors, Rancestors),
    disjoint(Rlist, Rancestors, List_Molecule, Ancestors, Bool),
    includes(Products, Rancestors, List_Molecule, Ancestors, Bool),
    ancestor_constraints(Rlist, Products, List_Molecule, Ancestors, Bool).

% constraint Bool ==> disjoint(reactant_ancestors)
disjoint([], _Rancestors, _List_Molecule, _Ancestors, _Bool).
disjoint([(_*R2) | Rlist], Rancestors, List_Molecule, Ancestors, Bool):-
    nth1(I, List_Molecule, R2),
    nth1(I, Ancestors, R2ancestors),
    disjoint(Rancestors, R2ancestors, Bool),
    disjoint(Rlist, Rancestors, List_Molecule, Ancestors, Bool).

disjoint([],_,_).
disjoint([R | Rancestors], [R2 | R2ancestors], Bool):-
    Bool*R*R2#=0,
    disjoint(Rancestors,R2ancestors, Bool).


% constraint Bool ==> includes(product_ancestors, reactant_ancestors)
includes([], _Rancestors, _List_Molecule, _Ancestors, _Bool).
includes([(_*P) | Plist], Rancestors, List_Molecule, Ancestors, Bool):-
    nth1(I, List_Molecule, P),
    nth1(I, Ancestors, Pancestors),
    includes(Pancestors, Rancestors, Bool),
    includes(Plist, Rancestors, List_Molecule, Ancestors, Bool).

includes([],_,_Bool).
includes([P | Pancestors], [R | Rancestors], Bool):-
    P#>=R*Bool,
    includes(Pancestors, Rancestors, Bool).





