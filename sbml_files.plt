:- use_module(library(plunit)).
:- use_module(library(http/http_open)).

% Only for separate compilation/linting
:- use_module(reaction_rules).


:- begin_tests(
  sbml_files,
  [condition(flag(slow_test, true, true)),
    setup((clear_model, reset_options))]
).


test(
  'load_sbml_reactions',
  [
    true(Reactions = [
    comp1*(kf_0*'Basal') for 'Basal'=>'BasalACh',
    comp1*(kr_0*'BasalACh') for 'BasalACh'=>'Basal',
    comp1*(kf_1*'BasalACh') for 'BasalACh'=>'BasalACh2',
    comp1*(kr_1*'BasalACh2') for 'BasalACh2'=>'BasalACh' | _ ])
  ]
) :-
  load('library:biomodels/BIOMD0000000001.xml'),
  all_items([kind: reaction], Reactions).


test(
  'load_sbml_initial_state',
  [true((
    member(initial_state('BasalACh2' = 0.0), Initial),
    member(initial_state('Basal' = 1.66057788110262e-5), Initial)
  ))]
) :-
  load('library:biomodels/BIOMD0000000001.xml'),
  all_items([kind: initial_state], Initial).


test(
  'load_sbml_local_parameters_assignment_rules'
) :-
  load('library:biomodels/BIOMD0000000003.xml'),
  command(numerical_simulation(time:1)).


test(
  'load assignmentRules and rateRules',
  [nondet]
) :-
  load('library:biomodels/BIOMD0000000006.xml'),
  % this reaction is inferred through rate rule and exists only if it works
  item([kind:reaction, item:Reaction]),
  reaction(Reaction, kappa, _,_,[1*v]),
  item([kind:function, item:function(z = v-u)]).

test(
  'load boundary species defined through assignmentRule',
  [nondet]
  ) :-
  load('library:biomodels/BIOMD0000000006.xml'),
  with_option(
    [method:rsbk, time:1],
    numerical_simulation
  ).

test(
  'download_curated_biomodel',
  [
    fixme('EBI-EMBL API seems broken right now'),
    true(ItemsLocal = ItemsDownloaded),
    % test that we have a working network
    condition(
      catch(
        setup_call_cleanup(
          http_open('http://www.google.com', In, []),
          true,
          close(In)
        ),
        _,
        (
          write('-'),
          fail
        )
      )
    )
  ]
) :-
  load('library:biomodels/BIOMD0000000003.xml'),
  all_items([], ItemsLocal),
  catch(
    (
      command(download_curated_biomodel(3)),
      load('BIOMD0000000003.xml'),
      all_items([], ItemsDownloaded),
      delete_file('BIOMD0000000003.xml')
    ),
    % 500 is server error, not our fault?
    error(existence_error(_Url, _Id), context(_Msg, status(500, _Text))),
    ItemsDownloaded = ItemsLocal
  ).

test(
  'download_recent_curated_biomodel',
  [
    fixme('EBI-EMBL API seems broken right now'),
    % test that we have a working network
    condition(
      catch(
        setup_call_cleanup(
          http_open('http://www.google.com', In, []),
          true,
          close(In)
        ),
        _,
        (
          write('-'),
          fail
        )
      )
    )
  ]
) :-
  catch(
    (
      command(download_curated_biomodel(800)),
      load('BIOMD0000000800.xml'),
      delete_file('BIOMD0000000800.xml')
    ),
    % 500 is server error, not our fault?
    error(existence_error(_Url, _Id), context(_Msg, status(500, _Text))),
    true
  ).

test(
  'export_sbml',
  []
) :-
  clear_model,
  command(add_reaction(f*[a] for a => b)),
  command(add_reaction('MA'(k) for a + 3*b => 2*a)),
  command(present(b, 3.5)),
  command(parameter(k=1.0e-3)),
  command(parameter(l=3)),
  command(function(f=l*'Time')),
  command(add_event([a] > 10, l=10*l)),
  export_sbml('test.xml'),
  load_sbml('test.xml'),
  delete_file('test.xml').

test(
  'export/import_volumes',
  []
) :-
  clear_model,
  command('a@v1 => a@v2'),
  export_sbml('test_iov.xml'),
  load_sbml('test_iov.xml'),
  list_locations(Comps),
  member(v1-_V, Comps),
  delete_file('test_iov.xml').

:- end_tests(sbml_files).
