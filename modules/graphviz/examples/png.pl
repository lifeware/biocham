:- module(
  png,
  [
    png/2
  ]
).

:- use_module('../graphviz').

png(SourceFilename, TargetFilename) :-
  agread(SourceFilename, Graph),
  gvLayout(Graph, dot),
  gvRenderFilename(Graph, png, TargetFilename),
  gvFreeLayout(Graph),
  agclose(Graph).
