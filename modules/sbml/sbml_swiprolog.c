#include <stdio.h>
#include <SWI-Prolog.h>
#include <sbml/SBMLTypes.h>
#include <sbml/xml/XMLError.h>
#include "sbml_swiprolog.h"

#define PL_check(result) \
    if (!(result)) { \
        PL_fail; \
    }

void
replace_time(ASTNode_t* math) {
    if (ASTNode_getType(math) == AST_NAME_TIME) {
        ASTNode_setName(math, "Time");
    } else if (ASTNode_getNumChildren(math) > 0) {
        for (int n = 0; n < ASTNode_getNumChildren(math); ++n) {
            replace_time(ASTNode_getChild(math, n));
        }
    }
}

static foreign_t
get_formula_from_math(const ASTNode_t* math, term_t formula_term) {
    const char* formula;
    ASTNode_t* newmath = ASTNode_deepCopy(math);
    replace_time(newmath);
    formula = SBML_formulaToString(newmath);
    PL_check(PL_unify_atom_chars(formula_term, formula));
    PL_succeed;
}

static foreign_t
get_string_from_XMLNode(const XMLNode_t* node, term_t string_term) {
    const char* string;
    string = XMLNode_convertXMLNodeToString(node);
    PL_succeed;
}


int
PL_get_SBase(term_t object_term, SBase_t **object) {
    return PL_get_pointer(object_term, (void **) object);
}

int
PL_get_sbmlDocument(term_t document_term, SBMLDocument_t **document) {
    return PL_get_pointer(document_term, (void **) document);
}

int
PL_get_model(term_t model_term, Model_t **model) {
    return PL_get_pointer(model_term, (void **) model);
}

int
PL_get_listOf(term_t list_term, ListOf_t **list) {
    return PL_get_pointer(list_term, (void **) list);
}

int
PL_get_reaction(term_t reaction_term, Reaction_t **reaction) {
    return PL_get_pointer(reaction_term, (void **) reaction);
}

int
PL_get_kineticLaw(term_t kineticLaw_term, KineticLaw_t **kineticLaw) {
    return PL_get_pointer(kineticLaw_term, (void **) kineticLaw);
}

int
PL_get_speciesReference(
    term_t speciesReference_term, SpeciesReference_t **speciesReference
) {
    return PL_get_pointer(speciesReference_term, (void **) speciesReference);
}

int
PL_get_modifierSpeciesReference(
    term_t modifierSpeciesReference_term,
    ModifierSpeciesReference_t **modifierSpeciesReference
) {
    return PL_get_pointer(
        modifierSpeciesReference_term,
        (void **) modifierSpeciesReference
    );
}

int
PL_get_species(term_t species_term, Species_t **species) {
    return PL_get_pointer(species_term, (void **) species);
}

int
PL_get_parameter(term_t parameter_term, Parameter_t **parameter) {
    return PL_get_pointer(parameter_term, (void **) parameter);
}

int
PL_get_rule(term_t rule_term, Rule_t **rule) {
    return PL_get_pointer(rule_term, (void **) rule);
}

int
PL_get_function(term_t function_term, FunctionDefinition_t **function) {
    return PL_get_pointer(function_term, (void **) function);
}

int
PL_get_event(term_t event_term, Event_t **event) {
    return PL_get_pointer(event_term, (void **) event);
}

int
PL_get_assignment(term_t assignment_term, EventAssignment_t **assignment) {
    return PL_get_pointer(assignment_term, (void **) assignment);
}

int
PL_unify_sbmlDocument_or_free(term_t document_term, SBMLDocument_t *document) {
    int result = PL_unify_pointer(document_term, document);
    if (!result) {
        SBMLDocument_free(document);
    }
    return result;
}

static foreign_t
pl_readSBML(term_t filename_term, term_t document_term) {
    char *filename;
    SBMLDocument_t *document;
    PL_check(PL_get_atom_chars(filename_term, &filename));
    PL_check(document = readSBML(filename));
    PL_check(PL_unify_sbmlDocument_or_free(document_term, document));
    PL_succeed;
}

static foreign_t
pl_sbmlDocument_getNumErrors(term_t document_term, term_t numErrors_term) {
    SBMLDocument_t *document;
    char *filename;
    unsigned int numErrors;
    PL_check(PL_get_sbmlDocument(document_term, &document));
    // Do not count info and warning messages
    numErrors = SBMLDocument_getNumErrorsWithSeverity(document, LIBSBML_SEV_ERROR) + SBMLDocument_getNumErrorsWithSeverity(document, LIBSBML_SEV_FATAL);
    PL_check(PL_unify_integer(numErrors_term, numErrors));
    PL_succeed;
}

static foreign_t
pl_sbmlDocument_printErrors(term_t document_term, term_t output_term) {
    SBMLDocument_t *document;
    char *filename;
    PL_check(PL_get_sbmlDocument(document_term, &document));
    // TODO: Handle custom stream
    SBMLDocument_printErrors(document, stderr);
    PL_succeed;
}

static foreign_t
pl_sbmlDocument_getModel(term_t document_term, term_t model_term) {
    SBMLDocument_t *document;
    Model_t *model;
    PL_check(PL_get_sbmlDocument(document_term, &document));
    PL_check(model = SBMLDocument_getModel(document));
    PL_check(PL_unify_pointer(model_term, model));
    PL_succeed;
}

static foreign_t
pl_sbmlDocument_free(term_t document_term) {
    SBMLDocument_t *document;
    PL_check(PL_get_sbmlDocument(document_term, &document));
    SBMLDocument_free(document);
    PL_succeed;
}

static foreign_t
pl_model_getListOfReactions(term_t model_term, term_t listOfReactions_term) {
    Model_t *model;
    ListOf_t *listOfReactions;
    PL_check(PL_get_model(model_term, &model));
    PL_check(listOfReactions = Model_getListOfReactions(model));
    PL_check(PL_unify_pointer(listOfReactions_term, listOfReactions));
    PL_succeed;
}

static foreign_t
pl_model_getListOfSpecies(term_t model_term, term_t listOfSpecies_term) {
    Model_t *model;
    ListOf_t *listOfSpecies;
    PL_check(PL_get_model(model_term, &model));
    PL_check(listOfSpecies = Model_getListOfSpecies(model));
    PL_check(PL_unify_pointer(listOfSpecies_term, listOfSpecies));
    PL_succeed;
}

static foreign_t
pl_model_getListOfCompartments(term_t model_term, term_t listOfCompartments_term) {
    Model_t *model;
    ListOf_t *listOfCompartments;
    PL_check(PL_get_model(model_term, &model));
    PL_check(listOfCompartments = Model_getListOfCompartments(model));
    PL_check(PL_unify_pointer(listOfCompartments_term, listOfCompartments));
    PL_succeed;
}

static foreign_t
pl_model_getListOfParameters(term_t model_term, term_t listOfParameters_term) {
    Model_t *model;
    ListOf_t *listOfParameters;
    PL_check(PL_get_model(model_term, &model));
    PL_check(listOfParameters = Model_getListOfParameters(model));
    PL_check(PL_unify_pointer(listOfParameters_term, listOfParameters));
    PL_succeed;
}

static foreign_t
pl_model_getListOfRules(term_t model_term, term_t listOfRules_term) {
    Model_t *model;
    ListOf_t *listOfRules;
    PL_check(PL_get_model(model_term, &model));
    PL_check(listOfRules = Model_getListOfRules(model));
    PL_check(PL_unify_pointer(listOfRules_term, listOfRules));
    PL_succeed;
}

static foreign_t
pl_model_getListOfFunctions(term_t model_term, term_t listOfFunctions_term) {
    Model_t *model;
    ListOf_t *listOfFunctions;
    PL_check(PL_get_model(model_term, &model));
    PL_check(listOfFunctions = Model_getListOfFunctionDefinitions(model));
    PL_check(PL_unify_pointer(listOfFunctions_term, listOfFunctions));
    PL_succeed;
}

static foreign_t
pl_model_getListOfEvents(term_t model_term, term_t listOfEvents_term) {
    Model_t *model;
    ListOf_t *listOfEvents;
    PL_check(PL_get_model(model_term, &model));
    PL_check(listOfEvents = Model_getListOfEvents(model));
    PL_check(PL_unify_pointer(listOfEvents_term, listOfEvents));
    PL_succeed;
}

static foreign_t
pl_model_getListOfInitialAssignments(term_t model_term, term_t listOfInitialAssignments_term) {
    Model_t *model;
    ListOf_t *listOfInitialAssignments;
    PL_check(PL_get_model(model_term, &model));
    PL_check(listOfInitialAssignments = Model_getListOfInitialAssignments(model));
    PL_check(PL_unify_pointer(listOfInitialAssignments_term, listOfInitialAssignments));
    PL_succeed;
}

static foreign_t
pl_model_getSpeciesById(
    term_t model_term, term_t id_term, term_t species_term
) {
    Model_t *model;
    char *id;
    Species_t *species;
    PL_check(PL_get_model(model_term, &model));
    PL_check(PL_get_atom_chars(id_term, &id));
    PL_check(species = Model_getSpeciesById(model, id));
    PL_check(PL_unify_pointer(species_term, species));
    PL_succeed;
}

static foreign_t
pl_listOf_get(term_t list_term, term_t index_term, term_t base_term) {
    ListOf_t *list;
    int index;
    SBase_t *base;
    PL_check(PL_get_listOf(list_term, &list));
    PL_check(PL_get_integer(index_term, &index));
    PL_check(base = ListOf_get(list, index));
    PL_check(PL_unify_pointer(base_term, base));
    PL_succeed;
}

static foreign_t
pl_listOf_size(term_t list_term, term_t size_term) {
    ListOf_t *list;
    unsigned int size;
    PL_check(PL_get_listOf(list_term, &list));
    PL_check(size = ListOf_size(list));
    PL_check(PL_unify_integer(size_term, size));
    PL_succeed;
}

static foreign_t
pl_reaction_getKineticLaw(term_t reaction_term, term_t kineticLaw_term) {
    Reaction_t *reaction;
    KineticLaw_t *kineticLaw;
    PL_check(PL_get_reaction(reaction_term, &reaction));
    PL_check(kineticLaw = Reaction_getKineticLaw(reaction));
    PL_check(PL_unify_pointer(kineticLaw_term, kineticLaw));
    PL_succeed;
}

static foreign_t
pl_reaction_getListOfReactants(term_t reaction_term, term_t list_term) {
    Reaction_t *reaction;
    ListOf_t *list;
    PL_check(PL_get_reaction(reaction_term, &reaction));
    PL_check(list = Reaction_getListOfReactants(reaction));
    PL_check(PL_unify_pointer(list_term, list));
    PL_succeed;
}

static foreign_t
pl_reaction_getListOfModifiers(term_t reaction_term, term_t list_term) {
    Reaction_t *reaction;
    ListOf_t *list;
    PL_check(PL_get_reaction(reaction_term, &reaction));
    PL_check(list = Reaction_getListOfModifiers(reaction));
    PL_check(PL_unify_pointer(list_term, list));
    PL_succeed;
}

static foreign_t
pl_reaction_getListOfProducts(term_t reaction_term, term_t list_term) {
    Reaction_t *reaction;
    ListOf_t *list;
    PL_check(PL_get_reaction(reaction_term, &reaction));
    PL_check(list = Reaction_getListOfProducts(reaction));
    PL_check(PL_unify_pointer(list_term, list));
    PL_succeed;
}

static foreign_t
pl_reaction_getReversible(term_t reaction_term, term_t reversible_term) {
    Reaction_t *reaction;
    int reversible;
    PL_check(PL_get_reaction(reaction_term, &reaction));
    PL_check(PL_unify_bool(reversible_term, Reaction_getReversible(reaction)));
    PL_succeed;
}

static foreign_t
pl_reaction_getId(term_t reaction_term, term_t id_term) {
    Reaction_t *reaction;
    const char *id;
    PL_check(PL_get_reaction(reaction_term, &reaction));
    PL_check(id = Reaction_getId(reaction));
    PL_check(PL_unify_atom_chars(id_term, id));
    PL_succeed;
}

static foreign_t
pl_kineticLaw_getMath(term_t kineticLaw_term, term_t formula_term) {
    KineticLaw_t *kineticLaw;
    const ASTNode_t *math;
    PL_check(PL_get_kineticLaw(kineticLaw_term, &kineticLaw));
    PL_check(math = KineticLaw_getMath(kineticLaw));
    get_formula_from_math(math, formula_term);
    PL_succeed;
}

static foreign_t
pl_kineticLaw_getListOfParameters(term_t kineticLaw_term, term_t list_term) {
    KineticLaw_t *kineticLaw;
    ListOf_t *list;
    PL_check(PL_get_kineticLaw(kineticLaw_term, &kineticLaw));
    PL_check(list = KineticLaw_getListOfParameters(kineticLaw));
    PL_check(PL_unify_pointer(list_term, list));
    PL_succeed;
}

static foreign_t
pl_speciesReference_getSpecies(
    term_t speciesReference_term, term_t species_term
) {
    SpeciesReference_t *speciesReference;
    const char *species;
    PL_check(PL_get_speciesReference(speciesReference_term, &speciesReference));
    PL_check(species = SpeciesReference_getSpecies(speciesReference));
    PL_check(PL_unify_atom_chars(species_term, species));
    PL_succeed;
}

static foreign_t
pl_speciesReference_getStoichiometry(
    term_t speciesReference_term, term_t stoichiometry_term
) {
    SpeciesReference_t *speciesReference;
    double stoichiometry;
    PL_check(PL_get_speciesReference(speciesReference_term, &speciesReference));
    PL_check(
        stoichiometry = SpeciesReference_getStoichiometry(speciesReference)
    );
    PL_check(PL_unify_float(stoichiometry_term, stoichiometry));
    PL_succeed;
}

static foreign_t
pl_modifierSpeciesReference_getSpecies(
    term_t modifierSpeciesReference_term, term_t species_term
) {
    ModifierSpeciesReference_t *modifierSpeciesReference;
    const char *species;
    PL_check(
        PL_get_modifierSpeciesReference(
            modifierSpeciesReference_term, &modifierSpeciesReference
        )
    );
    PL_check(
        species =
            ModifierSpeciesReference_getSpecies(modifierSpeciesReference)
    );
    PL_check(PL_unify_atom_chars(species_term, species));
    PL_succeed;
}

static foreign_t
pl_species_getName(term_t species_term, term_t name_term) {
    Species_t *species;
    const char *name;
    PL_check(PL_get_species(species_term, &species));
    PL_check(name = Species_getName(species));
    PL_check(PL_unify_atom_chars(name_term, name));
    PL_succeed;
}

static foreign_t
pl_species_getId(term_t species_term, term_t id_term) {
    Species_t *species;
    const char *id;
    PL_check(PL_get_species(species_term, &species));
    PL_check(id = Species_getId(species));
    PL_check(PL_unify_atom_chars(id_term, id));
    PL_succeed;
}

static foreign_t
pl_species_getCompartment(term_t species_term, term_t compartment_term) {
    Species_t *species;
    const char *compartment;
    PL_check(PL_get_species(species_term, &species));
    PL_check(compartment = Species_getCompartment(species));
    PL_check(PL_unify_atom_chars(compartment_term, compartment));
    PL_succeed;
}

static foreign_t
pl_species_getInitialAmount(term_t species_term, term_t initialAmount_term) {
    Species_t *species;
    PL_check(PL_get_species(species_term, &species));
    PL_check(Species_isSetInitialAmount(species));
    PL_check(
        PL_unify_float(
            initialAmount_term,
            Species_getInitialAmount(species)
        )
    );
    PL_succeed;
}

static foreign_t
pl_species_getInitialConcentration(
    term_t species_term, term_t initialConcentration_term
) {
    Species_t *species;
    PL_check(PL_get_species(species_term, &species));
    PL_check(Species_isSetInitialConcentration(species));
    PL_check(
        PL_unify_float(
            initialConcentration_term,
            Species_getInitialConcentration(species)
        )
    );
    PL_succeed;
}

static foreign_t
pl_species_getConstant(
    term_t species_term, term_t constant_term
) {
    Species_t *species;
    PL_check(PL_get_species(species_term, &species));
    PL_check(
        PL_unify_bool(
            constant_term,
            Species_getConstant(species)
        )
    );
    PL_succeed;
}

static foreign_t
pl_species_getBoundaryCondition(
    term_t species_term, term_t boundarycondition_term
) {
    Species_t *species;
    PL_check(PL_get_species(species_term, &species));
    PL_check(
        PL_unify_bool(
            boundarycondition_term,
            Species_getBoundaryCondition(species)
        )
    );
    PL_succeed;
}

static foreign_t
pl_species_hasOnlySubstanceUnits(
    term_t species_term
) {
    Species_t *species;
    PL_check(PL_get_species(species_term, &species));
    PL_check(Species_getHasOnlySubstanceUnits(species));
    PL_succeed;
}

static foreign_t
pl_parameter_getId(term_t parameter_term, term_t id_term) {
    Parameter_t *parameter;
    const char *id;
    PL_check(PL_get_parameter(parameter_term, &parameter));
    PL_check(id = Parameter_getId(parameter));
    PL_check(PL_unify_atom_chars(id_term, id));
    PL_succeed;
}

static foreign_t
pl_parameter_getValue(
    term_t parameter_term, term_t value_term
) {
    Parameter_t *parameter;
    PL_check(PL_get_parameter(parameter_term, &parameter));
    PL_check(Parameter_isSetValue(parameter));
    PL_check(
        PL_unify_float(
            value_term,
            Parameter_getValue(parameter)
        )
    );
    PL_succeed;
}

static foreign_t
pl_rule_getVariable(term_t rule_term, term_t variable_term) {
    Rule_t *rule;
    const char *variable;
    PL_check(PL_get_rule(rule_term, &rule));
    PL_check(variable = Rule_getVariable(rule));
    PL_check(PL_unify_atom_chars(variable_term, variable));
    PL_succeed;
}

static foreign_t
pl_rule_getMath(term_t rule_term, term_t formula_term) {
    Rule_t *rule;
    const ASTNode_t *math;
    PL_check(PL_get_rule(rule_term, &rule));
    PL_check(math = Rule_getMath(rule));
    get_formula_from_math(math, formula_term);
    PL_succeed;
}

static foreign_t
pl_function_getName(term_t function_term, term_t formula_term) {
    FunctionDefinition_t *function;
    const char *name;
    PL_check(PL_get_function(function_term, &function));
    PL_check(name = FunctionDefinition_getId(function));
    PL_check(PL_unify_atom_chars(formula_term, name));
    PL_succeed;
}

static foreign_t
pl_function_getBody(term_t function_term, term_t formula_term) {
    FunctionDefinition_t *function;
    const char *formula;
    const ASTNode_t *math;
    PL_check(PL_get_function(function_term, &function));
    PL_check(math = FunctionDefinition_getBody(function));
    get_formula_from_math(math, formula_term);
    PL_succeed;
}

static foreign_t
pl_function_getNumArguments(term_t function_term, term_t num_term) {
    FunctionDefinition_t *function;
    unsigned int n;
    PL_check(PL_get_function(function_term, &function));
    PL_check(n = FunctionDefinition_getNumArguments(function));
    PL_check(PL_unify_integer(num_term, n));
    PL_succeed;
}

static foreign_t
pl_function_getArgument(term_t function_term, term_t num_term, term_t arg_term) {
    FunctionDefinition_t *function;
    const char *argument;
    int num;
    PL_check(PL_get_function(function_term, &function));
    PL_check(PL_get_integer(num_term, &num));
    PL_check(argument = SBML_formulaToString(FunctionDefinition_getArgument(function, num)));
    PL_check(PL_unify_atom_chars(arg_term, argument));
    PL_succeed;
}

static foreign_t
pl_event_getTrigger(term_t event_term, term_t formula_term) {
    Event_t *event;
    const ASTNode_t *math;
    PL_check(PL_get_event(event_term, &event));
    PL_check(math = Trigger_getMath(Event_getTrigger(event)));
    get_formula_from_math(math, formula_term);
    PL_succeed;
}

static foreign_t
pl_event_getAssignments(term_t event_term, term_t list_term) {
    Event_t *event;
    ListOf_t *list;
    PL_check(PL_get_event(event_term, &event));
    PL_check(list = Event_getListOfEventAssignments(event));
    PL_check(PL_unify_pointer(list_term, list));
    PL_succeed;
}

static foreign_t
pl_event_hasDelay(term_t event_term, term_t hasDelay) {
    Event_t *event;
    PL_check(PL_get_event(event_term, &event));
    PL_check(
        PL_unify_bool(
            hasDelay,
            Event_isSetDelay(event)
        )
    );
    PL_succeed;
}

static foreign_t
pl_assignment_getVar(term_t assignment_term, term_t formula_term) {
    EventAssignment_t *assignment;
    const char *name;
    PL_check(PL_get_assignment(assignment_term, &assignment));
    PL_check(name = EventAssignment_getVariable(assignment));
    PL_check(PL_unify_atom_chars(formula_term, name));
    PL_succeed;
}

static foreign_t
pl_assignment_getBody(term_t assignment_term, term_t formula_term) {
    EventAssignment_t *assignment;
    const ASTNode_t *math;
    const char *formula;
    PL_check(PL_get_assignment(assignment_term, &assignment));
    PL_check(math = EventAssignment_getMath(assignment));
    get_formula_from_math(math, formula_term);
    PL_succeed;
}

static foreign_t
pl_is_assignment(term_t rule_term) {
    Rule_t *rule;
    PL_check(PL_get_rule(rule_term, &rule));
    PL_check(Rule_isAssignment(rule));
    PL_succeed;
}

static foreign_t
pl_is_rateRule(term_t rule_term) {
    Rule_t *rule;
    PL_check(PL_get_rule(rule_term, &rule));
    PL_check(Rule_isRate(rule));
    PL_succeed;
}

static foreign_t
pl_is_algebraic_rule(term_t rule_term) {
    Rule_t *rule;
    PL_check(PL_get_rule(rule_term, &rule));
    PL_check(Rule_isAlgebraic(rule));
    PL_succeed;
}

static foreign_t
pl_compartment_getId(term_t compartment_term, term_t id_term) {
    Compartment_t *compartment;
    const char *id;
    PL_check(PL_get_pointer(compartment_term, (void **) &compartment));
    PL_check(id = Compartment_getId(compartment));
    PL_check(PL_unify_atom_chars(id_term, id));
    PL_succeed;
}


static foreign_t
pl_compartment_getVolume(term_t compartment_term, term_t volume_term) {
    Compartment_t *compartment;
    double volume;
    PL_check(PL_get_pointer(compartment_term, (void **) &compartment));
    PL_check(volume = Compartment_getVolume(compartment));
    PL_check(PL_unify_float(volume_term, volume));
    PL_succeed;
}

static foreign_t
pl_compartment_getDimension(term_t compartment_term, term_t dimension_term) {
    Compartment_t *compartment;
    double dimension;
    PL_check(PL_get_pointer(compartment_term, (void **) &compartment));
    // As dimension may be 0, using PL_check could imply undesired fail
    dimension = Compartment_getSpatialDimensionsAsDouble(compartment);
    PL_check(PL_unify_float(dimension_term, dimension));
    PL_succeed;
}

static foreign_t
pl_initialAssignment_getSymbol(term_t initialAssignment_term, term_t symbol_term) {
    InitialAssignment_t *initialAssignment;
    const char *symbol;
    PL_check(PL_get_pointer(initialAssignment_term, (void **) &initialAssignment));
    PL_check(symbol = InitialAssignment_getSymbol(initialAssignment));
    PL_check(PL_unify_atom_chars(symbol_term, symbol));
    PL_succeed;
}

static foreign_t
pl_initialAssignment_getMath(term_t initialAssignment_term, term_t formula_term) {
    InitialAssignment_t *initialAssignment;
    const char *formula;
    const ASTNode_t *math;
    PL_check(PL_get_pointer(initialAssignment_term, (void **) &initialAssignment));
    PL_check(math = InitialAssignment_getMath(initialAssignment));
    get_formula_from_math(math, formula_term);
    PL_succeed;
}

static foreign_t
pl_model_getTimeUnits(
    term_t model_term, term_t units_term
) {
    const char *units;
    Model_t *model;
    UnitDefinition_t *unitdef;
    PL_check(PL_get_model(model_term, &model));
    if (Model_isSetTimeUnits(model)) {
        PL_check(units = Model_getTimeUnits(model));
    } else {
        PL_check(unitdef = Model_getUnitDefinitionById(model, "time"));
        PL_check(units = UnitDefinition_getName(unitdef))
    }
    PL_check(PL_unify_atom_chars(units_term, units));
    PL_succeed;
}


static foreign_t
pl_model_getSubstanceUnits(
    term_t model_term, term_t units_term
) {
    const char *units;
    Model_t *model;
    UnitDefinition_t *unitdef;
    PL_check(PL_get_model(model_term, &model));
    if (Model_isSetSubstanceUnits(model)) {
        PL_check(units = Model_getSubstanceUnits(model));
    } else {
        PL_check(unitdef = Model_getUnitDefinitionById(model, "substance"));
        PL_check(units = UnitDefinition_getName(unitdef))
    }
    PL_check(PL_unify_atom_chars(units_term, units));
    PL_succeed;
}


static foreign_t
pl_model_getVolumeUnits(
    term_t model_term, term_t units_term
) {
    const char *units;
    Model_t *model;
    UnitDefinition_t *unitdef;
    PL_check(PL_get_model(model_term, &model));
    if (Model_isSetVolumeUnits(model)) {
        PL_check(units = Model_getVolumeUnits(model));
    } else {
        PL_check(unitdef = Model_getUnitDefinitionById(model, "volume"));
        PL_check(units = UnitDefinition_getName(unitdef))
    }
    PL_check(PL_unify_atom_chars(units_term, units));
    PL_succeed;
}


static foreign_t
pl_object_getAnnotation(term_t object_term, term_t string_term) {
    const char *string;
    SBase_t *object;
    PL_check(PL_get_SBase(object_term, &object));
    PL_check(string = SBase_getAnnotationString(object));
    PL_check(PL_unify_atom_chars(string_term, string));
    PL_succeed;
}

static foreign_t
pl_object_setAnnotation(term_t object_term, term_t string_term) {
    const char *string;
    SBase_t *object;
    PL_check(PL_unify_atom_chars(string_term, string));
    PL_check(PL_get_SBase(object_term, &object));
    PL_check(SBase_appendAnnotationString(object, string));
    PL_succeed;
}

static foreign_t
pl_object_getNotes(term_t object_term, term_t string_term) {
    const char *string;
    SBase_t *object;
    PL_check(PL_get_SBase(object_term, &object));
    PL_check(string = SBase_getNotesString(object));
    PL_check(PL_unify_atom_chars(string_term, string));
    PL_succeed;
}

static foreign_t
pl_object_setNotes(term_t object_term, term_t string_term) {
    const char *string;
    SBase_t *object;
    PL_check(PL_unify_atom_chars(string_term, string));
    PL_check(PL_get_SBase(object_term, &object));
    PL_check(SBase_appendNotesString(object, string));
    PL_succeed;
}


PL_extension sbml_predicates[] = {
    { "readSBML", 2, (pl_function_t) pl_readSBML, 0 },
    { "sbmlDocument_getNumErrors", 2, (pl_function_t) pl_sbmlDocument_getNumErrors, 0 },
    { "sbmlDocument_printErrors", 2, (pl_function_t) pl_sbmlDocument_printErrors, 0 },
    { "sbmlDocument_getModel", 2, (pl_function_t) pl_sbmlDocument_getModel, 0 },
    { "sbmlDocument_free", 1, (pl_function_t) pl_sbmlDocument_free, 0 },
    { "model_getListOfReactions", 2, (pl_function_t) pl_model_getListOfReactions, 0 },
    { "model_getListOfSpecies", 2, (pl_function_t) pl_model_getListOfSpecies, 0 },
    { "model_getListOfCompartments", 2, (pl_function_t) pl_model_getListOfCompartments, 0 },
    { "model_getListOfParameters", 2, (pl_function_t) pl_model_getListOfParameters, 0 },
    { "model_getListOfRules", 2, (pl_function_t) pl_model_getListOfRules, 0 },
    { "model_getListOfFunctions", 2, (pl_function_t) pl_model_getListOfFunctions, 0 },
    { "model_getListOfEvents", 2, (pl_function_t) pl_model_getListOfEvents, 0 },
    { "model_getListOfInitialAssignments", 2, (pl_function_t) pl_model_getListOfInitialAssignments, 0 },
    { "model_getSpeciesById", 3, (pl_function_t) pl_model_getSpeciesById, 0 },
    { "model_getTimeUnits", 2, (pl_function_t) pl_model_getTimeUnits, 0 },
    { "model_getSubstanceUnits", 2, (pl_function_t) pl_model_getSubstanceUnits, 0 },
    { "model_getVolumeUnits", 2, (pl_function_t) pl_model_getVolumeUnits, 0 },
    { "listOf_get", 3, (pl_function_t) pl_listOf_get, 0 },
    { "listOf_size", 2, (pl_function_t) pl_listOf_size, 0 },
    { "reaction_getKineticLaw", 2, (pl_function_t) pl_reaction_getKineticLaw, 0 },
    { "reaction_getListOfReactants", 2, (pl_function_t) pl_reaction_getListOfReactants, 0 },
    { "reaction_getListOfModifiers", 2, (pl_function_t) pl_reaction_getListOfModifiers, 0 },
    { "reaction_getListOfProducts", 2, (pl_function_t) pl_reaction_getListOfProducts, 0 },
    { "reaction_getReversible", 2, (pl_function_t) pl_reaction_getReversible, 0 },
    { "reaction_getId", 2, (pl_function_t) pl_reaction_getId, 0 },
    { "kineticLaw_getMath", 2, (pl_function_t) pl_kineticLaw_getMath, 0 },
    { "kineticLaw_getListOfParameters", 2, (pl_function_t) pl_kineticLaw_getListOfParameters, 0 },
    { "speciesReference_getSpecies", 2, (pl_function_t) pl_speciesReference_getSpecies, 0 },
    { "speciesReference_getStoichiometry", 2,
      (pl_function_t) pl_speciesReference_getStoichiometry, 0 },
    { "modifierSpeciesReference_getSpecies", 2,
      (pl_function_t) pl_modifierSpeciesReference_getSpecies, 0 },
    { "species_getName", 2, (pl_function_t) pl_species_getName, 0 },
    { "species_getId", 2, (pl_function_t) pl_species_getId, 0 },
    { "species_getCompartment", 2, (pl_function_t) pl_species_getCompartment, 0 },
    { "species_getInitialAmount", 2, (pl_function_t) pl_species_getInitialAmount, 0 },
    { "species_getInitialConcentration", 2,
      (pl_function_t) pl_species_getInitialConcentration, 0 },
    { "species_hasOnlySubstanceUnits", 1, (pl_function_t) pl_species_hasOnlySubstanceUnits, 0 },
    { "species_getConstant", 2, (pl_function_t) pl_species_getConstant, 0 },
    { "species_getBoundaryCondition", 2, (pl_function_t) pl_species_getBoundaryCondition, 0 },
    { "parameter_getId", 2, (pl_function_t) pl_parameter_getId, 0 },
    { "parameter_getValue", 2, (pl_function_t) pl_parameter_getValue, 0 },
    { "rule_getVariable", 2, (pl_function_t) pl_rule_getVariable, 0 },
    { "rule_getMath", 2, (pl_function_t) pl_rule_getMath, 0 },
    { "function_getName", 2, (pl_function_t) pl_function_getName, 0 },
    { "function_getBody", 2, (pl_function_t) pl_function_getBody, 0 },
    { "function_getNumArguments", 2, (pl_function_t) pl_function_getNumArguments, 0 },
    { "function_getArgument", 3, (pl_function_t) pl_function_getArgument, 0 },
    { "event_getTrigger", 2, (pl_function_t) pl_event_getTrigger, 0 },
    { "event_getAssignments", 2, (pl_function_t) pl_event_getAssignments, 0 },
    { "event_hasDelay", 2, (pl_function_t) pl_event_hasDelay, 0 },
    { "assignment_getVar", 2, (pl_function_t) pl_assignment_getVar, 0 },
    { "assignment_getBody", 2, (pl_function_t) pl_assignment_getBody, 0 },
    { "compartment_getId", 2, (pl_function_t) pl_compartment_getId, 0 },
    { "compartment_getVolume", 2, (pl_function_t) pl_compartment_getVolume, 0 },
    { "compartment_getDimension", 2, (pl_function_t) pl_compartment_getDimension, 0 },
    { "object_getAnnotation", 2, (pl_function_t) pl_object_getAnnotation, 0 },
    { "object_setAnnotation", 2, (pl_function_t) pl_object_setAnnotation, 0 },
    { "object_getNotes", 2, (pl_function_t) pl_object_getNotes, 0 },
    { "object_setNotes", 2, (pl_function_t) pl_object_setNotes, 0 },
    { "is_assignment", 1, (pl_function_t) pl_is_assignment, 0 },
    { "is_rateRule", 1, (pl_function_t) pl_is_rateRule, 0 },
    { "is_algebraic_rule", 1, (pl_function_t) pl_is_algebraic_rule, 0 },
    { "initialAssignment_getSymbol", 2, (pl_function_t) pl_initialAssignment_getSymbol, 0 },
    { "initialAssignment_getMath", 2, (pl_function_t) pl_initialAssignment_getMath, 0 },
    { NULL, 0, NULL, 0 }
};

install_t
install_sbml_swiprolog(void) {
    PL_register_extensions(sbml_predicates);
}
