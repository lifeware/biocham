:- module(
  sbml_test_suite,
  [
    execute_syntactic_sbml_test/2,
    execute_all_semantic_sbml_test/1,
    execute_one_semantic_sbml_test/1,
    execute_one_semantic_sbml_test/3
  ]
).

% Only for separate compilation/linting
:- use_module(sbml_files).


%! execute_syntactic_sbml_test(+Directory, +Logpath)
%
%

increase(Counter) :-
  nb_getval(Counter, X),
  Xp is X+1,
  nb_setval(Counter, Xp).

:- dynamic(should_look/1).


execute_syntactic_sbml_test(Directory, Logpath) :-
  open(Logpath, write, Stream),
  nb_setval(total_cases, 0),
  nb_setval(true_positive, 0),
  nb_setval(false_positive, 0),
  nb_setval(true_negative, 0),
  nb_setval(false_negative, 0),
  nb_setval(error, 0),
  retractall(should_look(_)),
  forall(
    (
      directory_member(Directory, Folder, []),
      directory_file_path(Directory, File, Folder),
      re_match("^\\d+", File)
    ),
    (
      format("Test: ~w~n", [File]),
      execute_one_syntactic_sbml_test(Folder, Stream)
    )
  ),
  close(Stream),
  nb_getval(total_cases, TC),
  format("Total cases: ~w~n", [TC]),
  nb_getval(true_positive, TP),
  format("True positive: ~w~n", [TP]),
  nb_getval(true_negative, TN),
  format("True negative: ~w~n", [TN]),
  nb_getval(false_positive, FP),
  format("False positive: ~w~n", [FP]),
  nb_getval(false_negative, FN),
  format("False negative: ~w~n", [FN]),
  nb_getval(error, Err),
  format("Error: ~w~n", [Err]),
  writeln("Should look at:"),
  forall(
    should_look(M),
    writeln(M)
  ).

%! execute_one_syntactic_sbml_test(+Directory, +Logpath)
%
% Execute all the cases provided in one of the folder of the sbml syntactic test suite
% Simply print the result of the test
% TODO: remove the error stream from sbml-lib C api call

execute_one_syntactic_sbml_test(Directory, Stream) :-
  forall(
    (
      directory_member(Directory, Path_xml, [extensions([xml])]),
      atom_concat(Path, '.xml', Path_xml),
      re_match("l3v2", Path_xml),
      atom_concat(Path, '.txt', Path_txt)
    ),
      catch(
        (
          increase(total_cases),
          format(Stream, "Testing: ~w~n", [Path]),
          (
            re_match('fail', Path), % should fail
            re_match('sev2', Path),
            catch(
              (
                with_output_to(atom(_), load(Path_xml)),
                increase(false_negative),
                fail
              ),
              error(sbml_errors, _),
              increase(true_negative)
            )
          ->
            format(Stream, " ++ Test succeed ++ ~n", [])
          ;
            (
              re_match('pass', Path)  % should succeed
            ;
              re_match('fail', Path),
              re_match('sev1', Path)  % should warning
            ),
            catch(
              with_output_to(atom(_), load(Path_xml)),
              error(sbml_errors, _),
              (
                increase(false_positive),
                format(Stream, "Test ~w is a dangerous false_positive", [Path_xml]),
                fail
              )
            )
          ->
            increase(true_positive),
            format(Stream, " ++ Test succeed ++ ~n", [])
          ;
            fetch_short_message(Path_txt, Message),
            format(Stream, "\e[1;31m -- Test Fail -- \e[0m~n~w~n", [Message]),
            forall(
              member(M, Message),
              (
                should_look(M)
              ->
                true
              ;
                assertz(should_look(M))
              )
            )
          )
        ),
        _Error,
        (
          increase(error),
          format(Stream, "\e[1;31m -- Test ERROR -- \e[0m~n~w~n", [Message])
        )
      )
  ).


%! fetch_short_message(+Path_txt, -Message_list)
%
% Look on the textual description of the test for the expected errors

fetch_short_message(Path_txt, Message_list) :-
  read_file_to_string(Path_txt, Str_txt, []),
  split_string(Str_txt, "\n", "", Split_txt),
  findall(
    Message,
    (
      member(Line, Split_txt),
      re_matchsub("Short message[ :\t]+(?<message>.*)", Line, D, []),
      Message = D.message
    ),
    Message_list
  ).




execute_all_semantic_sbml_test(Directory) :-
  findall(
    Path,
    directory_member(Directory, Path, []),
    List_path
  ),
  sort(List_path, Sorted_path),
  forall(
    member(Path, Sorted_path),
    (
      execute_one_semantic_sbml_test(Path, false, Status),
      format("Tested: ~w, Status: ~w~n", [Path, Status])
    )
  ).
 

%! execute_one_semantic_sbml_test(+Directory, +with_plot, Status)
%
% Status may be: fine, warning-fine, wrong, warning-wrong, error, failure

execute_one_semantic_sbml_test(Directory) :-
  execute_one_semantic_sbml_test(Directory, true, Status),
  format("~nTesting: ~w, exit with status ~w~n", [Directory, Status]).


execute_one_semantic_sbml_test(Directory, With_plot, WStatus) :-
  set_counter(warnings, 0),
  catch(
    (
      (
        directory_member(Directory, Path_xml, [extensions([xml])]),
        atom_concat(X, 'sbml-l3v2.xml', Path_xml),
        directory_member(Directory, Path_settings, [extensions([txt])]),
        atom_concat(X, 'settings.txt', Path_settings),
        directory_member(Directory, Path_results, [extensions([csv])]),
        debug(semantic, "xml path; ~w~nsetting path: ~w~n", [Path_xml, Path_settings]),
        read_file_to_string(Path_settings, Str_settings, []),
        parse_all_lines(Str_settings, List_settings_unf),
        maplist([Unf, For]>>split_string(Unf, ":", " ", For), List_settings_unf, List_settings),
        debug(semantic, "Settings: ~w", [List_settings]),
        clear_model,
        load(Path_xml),
        build_show_comparison(List_settings, To_show, Comparisons),
        debug(semantic, "To_compare: ~w", [Comparisons]),
        % fetch option
        member(["duration", Time_str], List_settings), number_string(Time, Time_str),
        member(["steps", Steps_str], List_settings), number_string(Steps, Steps_str),
        with_option([
            time: Time,
            steps:Steps,
            display_concentration_amount: both
            ], numerical_simulation),
        (
            With_plot
        ->
            with_option([show: To_show], plot)
        ; 
            true
        ),
        get_table_headers(Biocham_header),
        get_table_data(Biocham_data),
        clear_model,
        load(Path_results),
        (With_plot -> plot; true),
        get_table_headers(Sbml_header),
        get_table_data(Sbml_data),
        debug(semantic, "SBML_header: ~w", [Sbml_header]),
        filter_on_time(Sbml_data, Biocham_data, Sbml_aligned, Biocham_aligned),
        semantic_comparison(Comparisons, Sbml_header, Sbml_aligned, Biocham_header, Biocham_aligned, Error),
        (
          Error < 1e-2
        ->
          Status = fine
        ;
          Status = wrong
          % format("\e[1;31mSomething seems really strange with the test above. Error: ~w\e[0m ~n", [Error])
        )
      )
    ;
      Status = failure
    ),
    _,
    Status = error
  ),
  count(warnings, Warnings),
  (
    Warnings > 0
  ->
    WStatus = warning-Status
  ;
    WStatus = Status
  ).


build_show_comparison(List_settings, To_show, Comparisons) :-
    member(["amount"|[String_amounts]], List_settings),
    split_string(String_amounts, ",", " ", List_string_amounts),
    maplist(atom_string, List_Atom_Amounts, List_string_amounts),
    include(is_molecule, List_Atom_Amounts, List_Amounts),
    member(["concentration"|[String_concentrations]], List_settings),
    split_string(String_concentrations, ",", " ", List_string_concentrations),
    maplist(atom_string, List_Atom_Concentrations, List_string_concentrations),
    include(is_molecule, List_Atom_Concentrations, List_toBe_Concentrations),
    maplist([X, Y]>>atom_concat(X, '__conc', Y), List_toBe_Concentrations, List_Concentrations),
    append(List_Concentrations, List_Amounts, To_show),
    maplist([X,Y,X-Y]>>true, List_Amounts, List_Amounts, Compare_Amounts),
    maplist([X,Y,X-Y]>>true, List_toBe_Concentrations, List_Concentrations, Compare_Conc),
    append(Compare_Amounts, Compare_Conc, Comparisons).


%! parse_all_lines(+Input, -Output)
%
% split a file to its non empty lines independently of the encoding

parse_all_lines(Input, Output) :-
  split_string(Input, "\r\n", " ", Tempo),
  findall(
    Line,
    (
        member(Line, Tempo),
        Line \= ""
    ),
    Output
  ).


%! filter_on_time(Sbml_data_raw, Biocham_data_raw, Sbml_data, Biocham_data)

filter_on_time([], _, [], []).

filter_on_time(_, [], [], []).

filter_on_time([SHead|STail], [BHead|BTail], [SHead|SCTail], [BHead|BCTail]) :-
    SHead =.. [row, STime | _],
    BHead =.. [row, BTime | _],
    abs(STime - BTime) < 1e-6,
    !,
    filter_on_time(STail, BTail, SCTail, BCTail).

filter_on_time([SHead|STail], [BHead|BTail], SCTail, BCTail) :-
    SHead =.. [row, STime | _],
    BHead =.. [row, BTime | _],
    STime > BTime,
    !,
    filter_on_time([SHead|STail], BTail, SCTail, BCTail).

filter_on_time([SHead|STail], [BHead|BTail], SCTail, BCTail) :-
    SHead =.. [row, STime | _],
    BHead =.. [row, BTime | _],
    STime < BTime,
    !,
    filter_on_time(STail, [BHead|BTail], SCTail, BCTail).


%! semantic_comparison(Comparison, Sbml_header, Sbml_data, Biocham_header, Biocham_data, Error)

semantic_comparison([], _, _, _, _, 0).

semantic_comparison([Sbml-Biocham|Tail], SH, SD, BH, BD, Err) :-
  debug(semantic, "Comparing: ~w", [Sbml-Biocham]),
  nth1(IndexS, SH, Sbml),
  maplist([Data, Value]>>(Data =.. [row|L], nth1(IndexS, L, Value)), SD, SBML_Value), 
  debug(semantic, "SBML_Value: ~w", [SBML_Value]), 
  nth1(IndexB, BH, Biocham),
  debug(semantic, "~w in ~w", [Biocham, BH]),
  maplist([Data, Value]>>(Data =.. [row|L], nth1(IndexB, L, Value)), BD, Biocham_Value),
  debug(semantic, "Biocham_Value: ~w", [Biocham_Value]), 
  maplist(compute_error, SBML_Value, Biocham_Value, Error_list),
  max_list(Error_list, Local_error),
  debug(semantic, "Error: ~w", [Local_error]),
  semantic_comparison(Tail, SH, SD, BH, BD, ErrTail),
  debug(semantic, "Error tail: ~w", [ErrTail]),
  Err is max(ErrTail, Local_error). 

compute_error(Expected, Obtained, Error) :-
  (
    abs(Expected) < 1e-9
  ->
    Error is Obtained
  ;
    abs(Obtained) < 1e-9
  ->
    Error is Expected
  ;
    Error is abs(Obtained - Expected)/Expected
  ).
