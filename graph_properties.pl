:- module(
  graph_properties,
  [
   pathway/2,
   list_source_species/0,
   list_input_species/0,
   list_strict_input_species/0,
   list_sink_species/0,
				% API
   pathway/3,
   input_species/1,
   strict_input_species/1,
   source_species/1,
   sink_species/1,
   is_source/2,
   is_input/2,
   is_strict_input/2,
   is_sink/2
  ]
	 ).

:- doc('
      Analysis of some graph properties of the current model.
      ').

pathway(Object1, Object2):-
  biocham_command,
  type(Object1, object),
  type(Object2, object),
  doc('Gives one reaction pathway from \\argument{Object1} to \\argument{Object2} if one exists in the directed reaction graph of the current model (for more complex queries, see next section on Computation Tree Logic model-checking).'),
  (
   pathway(Object1, Object2, Path)
  ->
   reverse(Path, Reactions),
   forall(member(R, Reactions), writeln(R))
  ;
   writeln('there is no pathway')
  ).
  
pathway(Source, Target, Path):-
  path([(Source, [Source])], [], [(Source, [Source])], Target, Path).

% Dijsktra algorithm with pathway memory
path([], [], _Forward, _Target, _Path):-
  !,
  fail.

path([], New, Forward, Target, Path):-
  path(New, [], Forward, Target, Path).

path([A|Frontier], New, Forward, Target, Path):-
  new_successors(A, Forward, List),
  (
   member((Target, Path), List)
  ->
   true
  ;
   append(List, New, New2),
   append(List, Forward, Forward2),
   path(Frontier, New2, Forward2, Target, Path)
  ).
  
new_successors(A, Forward, List):-
  realsetof(B, graph_properties:new_successor(A, B, Forward), List).
  
new_successor((A, PathA), (B, [Reaction|PathA]), Forward):-
  is_reactant(A, Reaction),
  is_product(B, Reaction),
  \+ member((B,_), Forward).

				% Sources and Sinks


list_input_species:-
  biocham_command,
  doc('Lists the molecular species that are neither a reaction product apart from strict catalysts, nor the target of an influence rule, in the curent model. Reactants that are not catalysts, and strict catalysts, i.e. catalysts with same stoichiometry in left-hand and right side of reactions, are allowed as inputs.'),
  input_species(Molecules),
  writeln(Molecules).

list_strict_input_species:-
  biocham_command,
  doc('Lists the molecular species that are strict catalysts, i.e. neither a strict product, nor strict reactant, nor the target of an influence rule in the curent model.'),
  strict_input_species(Molecules),
  writeln(Molecules).

list_source_species:-
  biocham_command,
  doc('Lists the molecular species that are neither a reaction product nor the target of an influence rule in the curent model.'),
  source_species(Molecules),
  writeln(Molecules).

list_sink_species:-
  biocham_command,
  doc('Lists the molecular species that are neither a reactant nor a positive source of an influence rule in the curent model.'),
  sink_species(Molecules),
  writeln(Molecules).




				%API

:-devdoc('Prolog lists of graph sources and sinks.').

strict_input_species(L):-
  enumerate_molecules(Molecules),
  realsetof(M, is_strict_input(M, Molecules), L).

input_species(L):-
  enumerate_molecules(Molecules),
  realsetof(M, is_input(M, Molecules), L).

source_species(L):-
  enumerate_molecules(Molecules),
  realsetof(M, is_source(M, Molecules), L).

sink_species(L):-
  enumerate_molecules(Molecules),
  realsetof(M, is_sink(M, Molecules), L).


  
is_source(M, Molecules):-
  member(M, Molecules),
  \+ (
      is_product(M,_)
     ),
  \+ (
     is_target(M,_)
    ).

is_input(M, Molecules):-
  member(M, Molecules),
  \+ (
      is_strict_product(M,_)
     ),
  \+ (
     is_target(M,_)
     ).

is_strict_input(M, Molecules):-
  member(M, Molecules),
  \+ (
      is_strict_reactant(M,_)
     ),
  \+ (
      is_strict_product(M,_)
     ),
  \+ (
     is_target(M,_)
    ).

is_sink(M, Molecules):-
  member(M, Molecules),
  \+ (
      is_reactant(M,_)
     ),
  \+ (
     is_influence_source(M,_)
     ).






  