:- module(
  conservation_laws,
  [
    add_conservation/1,
    delete_conservation/1,
    delete_conservations/0,
    list_conservations/0,
    check_conservations/0,
    search_conservations/0
  ]
).

:- doc('Petri Net Place-invariants provide linear conservation laws for the differential semantics of a reaction network.
They can be verified or computed with the commands below. Note that such invariants are useful information but it may be not a good idea to use them to reduce
the dimensionality of the system since it may introduce numerical instabilities.').

add_conservation(Conservation) :-
  biocham_command,
  type(Conservation, solution),
  % Avoid a SWI-Prolog warning about long strings…
  doc(
    'declares a new mass conservation law for all molecules given with the
    corresponding weight in \\argument{Conservation}. During a numerical
    simulation, one of those variables will be
    eliminated thanks to this conservation law. Be careful if you declare
    conservation laws and then plot the result of a previous simulation, the
    caption might not be correct.
When added, the conservation law will be checked against the reactions
    (i.e. purely from stoichiometry), if that fails against the kinetics.
    Since these checks are not complete, even a failure will be accepted with
    a warning.'
  ),
  solution_to_conservation(Conservation, C),
  add_item([kind: conservation, item: Conservation, id: Id]),
  set_annotation(Id, conservation_list, C).


delete_conservation(Conservation) :-
  biocham_command,
  type(Conservation, solution),
  doc('removes the given mass conservation law.'),
  % model: current_model?
  find_item([id: Id, type: conservation, item: Conservation]),
  delete_item(Id).


delete_conservations :-
  biocham_command,
  doc('removes all mass conservation laws.'),
  % model: current_model?
  delete_items([kind: conservation]).


list_conservations :-
  biocham_command,
  doc('prints out all the mass conservation laws.'),
  % model: current_model?
  list_items([kind: conservation]).


check_conservations :-
  biocham_command,
  doc(
    'checks all conservation laws against reactions, and if necessary kinetics
    (see also \\command{add_conservation/1}).'
  ),
  ode_system,
  forall(
    (
      % model: current_model? compute_ode already supposes a single model
      % otherwise for each selected model, get all conservations and check
      item([kind: conservation, item: Conserv, id: Id]),
      get_annotation(Id, conservation_list, C)
    ),
    check_conservation(C, Conserv)
  ).

:- initial(option(conservation_size: 4)).


search_conservations :-
  biocham_command,
  doc(
    'computes and displays the P-invariants of the network up to the maximal
    size given by the option conservation_size and defaulting to 4. Such
    P-invariants are particular mass conservation laws that are independent
    from the kinetics.'
  ),
  option(conservation_size, integer, Size,
    'Set the maximal stoichiometric size for a P-invariant.'),
  invariants:find_pinvar(Size).


search_efms :-
  biocham_command,
  doc(
    'computes and displays the T-invariants of the network up to the maximal
    flux given by the option conservation_size and defaulting to 4. Such
    T-invariants can be seen as a way to compute Extreme Rays of the cone of
    Elementary Flux Modes.'
  ),
  option(conservation_size, integer, Size,
    'Set the maximal stoichiometric size for a P-invariant.'),
  invariants:find_tinvar(Size).


%%% End of public API


solution_to_conservation(Solution, Conservation) :-
  devdoc('transforms a solution to a factorized list of coeff*object'),
  reaction_editor:solution_to_list(Solution, List),
  reaction_editor:simplify_solution(List, Conservation).


check_conservation(C, Conserv) :-
  (
    check_conserv_reactions(C)
  ->
    print_message(informational, conservation_checked(Conserv, reactions, true))
  ;
    print_message(
      informational,
      conservation_checked(C, reactions, false)
    ),
    check_conserv_num(C)
  ->
    print_message(informational, conservation_checked(Conserv, kinetics, true))
  ;
    print_message(informational, conservation_checked(Conserv, kinetics, false)),
    print_message(warning, conservation_trusted(Conserv))
  ).


check_conserv_reactions(C) :-
  forall(
    (
      % model: current_model?
      item([kind: reaction, item: Item]),
      reaction(Item, [
        reactants: Reactants,
        inhibitors: Inhibitors,
        products: Products
      ])
    ),
    (
      scalar_conservation(Reactants, C, Moiety),
      scalar_conservation(Inhibitors, C, Moiety),
      scalar_conservation(Products, C, Moiety)
    )
  ).


check_conserv_num(C) :-
  get_weighted_ode(C, WeightedSum),
  simplify(WeightedSum, Sum),
  Sum == 0.


get_weighted_ode([], 0).

get_weighted_ode([K*M | L], K*O + S) :-
  ode(M, O),
  get_weighted_ode(L, S).


scalar_conservation([], _, 0).

scalar_conservation([K*M | L], C, Total) :-
  (
    member(I*M, C)
  ->
    true
  ;
    I = 0
  ),
  scalar_conservation(L, C, T1),
  Total is I*K + T1.


prolog:message(conservation_trusted(C)) -->
  ['Conservation ~w trusted without proof'-[C]].

prolog:message(conservation_checked(C, From, Truth)) -->
  {
    Truth == true
  ->
    Not = ''
  ;
    Not = 'not '
  },
  ['Conservation ~w ~wchecked from ~w'-[C, Not, From]].

% vi: set ts=2 sts=2 sw=2
