:- use_module(library(plunit)).

:- begin_tests(biocham, [setup((clear_model, reset_options))]).

test('initialize') :-
  initialize.

:- end_tests(biocham).
