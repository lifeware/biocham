:- module(
  influence_properties,
  [
    % Commands
    list_stable_states/0,
    list_tscc_candidates/0,
    % Public API
    mark_subsumed_influences/1,
    subsumed_influence/1
  ]
).

% Only for separate compilation/linting
:- use_module(doc).
:- use_module(influence_rules).
:- use_module(sat).
:- use_module(util).

:- doc('The following commands refer to the Boolean dynamics of a BIOCHAM model, possibly combining reaction rules and influence rules.
      The Boolean semantics can be either positive (i.e. without negation, the inhibitors are ignored)
or negative (the inhibitors of a reaction or an influence are interpreted as negative conditions) \\cite{FMSR16cmsb}.
The notion of Boolean states considered in this first section is the one of complete ground states defined by the presence or absence of each molecular species,
unlike the notion of symbolic representation of partial states represented by Boolean constraints and considered in the following sections.').


list_stable_states :-
  biocham_command,
  doc('
    lists stable steady ground Boolean states of the ground state transition graph corresponding
    to the Boolean semantics of the current model.
  '),
  option(
    boolean_semantics,
    boolean_semantics,
    BoolSem,
    'Use positive or negative boolean semantics for inhibitors.'
  ),
  option(boolean_state_display, boolean_state_display, _,
    'choice of display of the boolean states.'),
  list_stable_states(BoolSem),
  findall(
    NState,
    (
      stable_state(BState),
      maplist(number_state, BState, NState)
    ),
    States
  ),
  display_vectors(States).


:- dynamic(stable_state/1).


%! list_stable_states(+BoolSem)
%
% Compute stable ground states for the given BoolSem semantics by calling a SAT
% solver iteratively and storing solutions in stable_state/1
%
% @arg BoolSem positive/negative semantics for influences
list_stable_states(BoolSem) :-
  retractall(stable_state(_)),
  with_influence_model(influence_properties:(
    format(atom(Comment), "list stable states (~w)", [BoolSem]),
    mark_subsumed_influences(BoolSem),
    write_sat_file(Comment, FileName, Stream),
    add_stable_constraints(BoolSem),
    close(Stream),
    run_sat_iteratively(FileName, Solutions),
    forall(
      member(Solution, Solutions),
      assertz(stable_state(Solution))
    )
  )),
  !.

:- dynamic(candidate/1).


%! list_tscc_candidates
%
% Compute terminal strongly connected components (TSCC) candidates of the ground state transition graph, by calling a SAT solver iteratively and storing
% solutions in candidate/1
list_tscc_candidates :-
  biocham_command,
  doc('
    lists possible representative states of Terminal Strongly Connected
    Components (TSCC) of the ground state transition graph corresponding to the
    positive semantics of the current model.
  '),
  option(boolean_state_display, boolean_state_display, _,
    'choice of display of the boolean states.'),
  retractall(candidate(_)),
  statistics(walltime, _),
  with_influence_model(influence_properties:(
    mark_subsumed_influences(positive),
    write_sat_file('list tscc candidates', FileName, Stream),
    add_tscc_constraints,
    debug(tscc, "tscc constraint added", []),
    add_reversibility_constraints,
    debug(tscc, "reversibility constraint added", []),
    close(Stream),
    run_sat_iteratively(FileName, Solutions),
    debug(tscc, "solutions: ~w", [Solutions]),
    display_vector_header,
    forall(
      member(Solution, Solutions),
      (
        (
          has_enabled_effective('-', Solution)
        ->
          assertz(candidate(Solution))
        ;
          write_bool_state(Solution),
          write(' stable\n')
        )
      )
    )
  )),
  candidates_in_positive_tscc(NCandidates, NTSCCs),
  statistics(walltime, [_, MilliTime]),
  Time is MilliTime / 1000,
  write('Candidates from constraints: '), write(NCandidates),nl,
  write('Complex TSCCs computed: '), write(NTSCCs),nl,
  write('Time: '), write(Time),nl.


%! write_sat_file(+Comment, -FileName, -Stream)
%
% Create a temporary file, write a temporary header to it, setting up the
% sat_vars fact with the number of variables, the sat_clauses counter to -1
% (will be bumped to 0 on writing the header and increased at each
% write_clause call) and the sat_comment fact to Comment. Also create an alias
% infl_property_out_stream for the stream for ease of use in write_clause/2
%
% @arg Comment    the atom to we put on the first 'c ...' line of the file
% @arg FileName   the name of the created temporary file
% @arg Stream     the corresponding stream
write_sat_file(Comment, FileName, Stream) :-
    enumerate_objects(Objects),
    length(Objects, NVar),
    associate_number_to_objects(Objects),
    tmp_file_stream(text, FileName, Stream),
    retractall(sat_comment(_)),
    assertz(sat_comment(Comment)),
    retractall(sat_vars(_)),
    assertz(sat_vars(NVar)),
    set_counter(sat_clauses, -1),
    write_sat_header(Stream),
    set_stream(Stream, alias(infl_property_out_stream)).


% all outgoing influences do not change the state
add_stable_constraints(BoolSem) :-
  findall(
    (PositiveInputs, NegativeInputs, Sign, Output),
    (
      item([kind: influence, item: Item]),
      influence(Item, [
        positive_inputs: PositiveInputs,
        negative_inputs: NegInputs,
        sign: Sign,
        output: Output
      ]),
      \+ subsumed_influence(Item),
      (
        BoolSem == positive
      ->
        NegativeInputs = []
      ;
        NegativeInputs = NegInputs
      )
    ),
    InfluenceQuads
  ),
  add_stable_constraint(InfluenceQuads).


% all outgoing positive influences do not change the state
add_tscc_constraints :-
  findall(
    (PositiveInputs, [], Sign, Output),
    (
      item([kind: influence, item: Item]),
      influence(Item, [
        positive_inputs: PositiveInputs,
        sign: Sign,
        output: Output
      ]),
      Sign = '+',
      \+ subsumed_influence(Item)
    ),
    InfluenceQuads
  ),
  add_stable_constraint(InfluenceQuads).


% all outgoing negative influences can be reversed
add_reversibility_constraints :-
  findall(
    (PositiveInputs, Output),
    (
      % FIXME we decompose Item first to filter, but result is dependent on
      % precise influence operators
      Item = (_ -< _),
      item([kind: influence, item: Item]),
      influence(Item, [
        positive_inputs: PositiveInputs,
        sign: '-',
        output: Output
      ]),
      \+ subsumed_influence(Item)
    ),
    InfluenceDoubles
  ),
  add_reversible_constraint(InfluenceDoubles).


add_stable_constraint([]).


% given influence triples do not change the state
add_stable_constraint([(PositiveInputs, NegativeInputs, Sign, Output) | InfluenceQuads]) :-
  maplist(object_number, PositiveInputs, BoolPositiveInputs),
  maplist(object_number, NegativeInputs, BoolNegativeInputs),
  object_number(Output, BoolOutput),
  (
    Sign = '+'
  ->
    write_clause([BoolOutput | BoolNegativeInputs], BoolPositiveInputs)
  ;
    write_clause(BoolNegativeInputs, [BoolOutput | BoolPositiveInputs])
  ),
  add_stable_constraint(InfluenceQuads).


add_reversible_constraint([]).

add_reversible_constraint([(PositiveInputs, Output) | InfluenceDoubles]) :-
  debug(tscc, "adding reversible for ~w -< ~w", [PositiveInputs, Output]),
  maplist(object_number, PositiveInputs, BoolPositiveInputs),
  object_number(Output, BoolOutput),
  findall(
    BoolPositiveInputs2,
    (
      Item = (_ -> Output),
      item([kind: influence, item: Item]),
      % FIXME we decompose Item first to filter, but result is dependent on
      % precise influence operators
      influence(Item, [positive_inputs: PositiveInputs2]),
      \+ subsumed_influence(Item),
      \+ member(Output, PositiveInputs2),
      maplist(object_number, PositiveInputs2, BoolPositiveInputs2)
    ),
    Reverse
  ),
  debug(tscc, "found reversibility in all cases: ~w", [Reverse]),
  % for any negative influence, either it is not enabled, or it does not
  % change anything, or there is some positive influence on the same output,
  % that does not need it as input and is enabled
  % that last step is an or over an and, we have to distribute it
  forall(
    maplist(member, Distributed, Reverse),
    write_clause(Distributed, [BoolOutput | BoolPositiveInputs])
  ),
  add_reversible_constraint(InfluenceDoubles).


:- dynamic(object_number/2).

%! associate_number_to_objects(+Objects)
%
% Given a list of objects associate a unique ID to each and store that in
% object_number/2
%
% @arg Objects  the list of objects to use
associate_number_to_objects(Objects) :-
  retractall(object_number(_, _)),
  associate_number_to_objects(Objects, 1).

associate_number_to_objects([], _).

associate_number_to_objects([H | T], I) :-
  assertz(object_number(H, I)),
  II is I + 1,
  associate_number_to_objects(T, II).


%! number_state(+Number, -(-Species, -Value))
%
% using the sign of Number, decide of Value (0 for false, 1 for true) and
% using the absolute value of Number look up in object_number the
% corresponding Species
%
% @arg Number   A litteral as in DIMACS, i.e. signed number
% @arg Species  the corresponding biocham object
% @arg Value    0/1 for true/false
number_state(N, S-V) :-
  (
    N > 0
  ->
    NN = N,
    V = 1
  ;
    NN is -N,
    V = 0
  ),
  object_number(S, NN).


write_bool_state(BoolState) :-
  maplist(number_state, BoolState, BoolList),
  display_vector(BoolList).


enumerate_objects(Objects) :-
  setof(
    Object,
    enumerate_object(Object),
    Objects
  ).


enumerate_object(Object) :-
  item([kind: influence, item: Item]),
  influence(Item, [
    positive_inputs: PositiveInputs,
    negative_inputs: NegativeInputs,
    output: Output
  ]),
  (
    member(Object, PositiveInputs)
  ;
    member(Object, NegativeInputs)
  ;
    Object = Output
  ).


has_enabled_effective(Sign, BoolState) :-
  item([kind: influence, item: Item]),
  influence(Item, [
    positive_inputs: PositiveInputs,
    sign: Sign,
    output: Output
  ]),
  \+ subsumed_influence(Item),
  maplist(object_number, PositiveInputs, BoolInputs),
  object_number(Output, NOutput),
  (
    Sign = '+'
  ->
    invert(NOutput, Out)
  ;
    Out = NOutput
  ),
  forall(
    member(Thing, [Out | BoolInputs]),
    member(Thing, BoolState)
  ).


candidates_in_positive_tscc(NCandidates, NTSCCs) :-
  devdoc('
    Hide initial_state, then ask NuSMV if everywhere State => AG(EF(State))
    for each State given by candidate/1. if the result is true we have a T-SCC
    '
  ),
  debug(tscc, "for tscc candidates, look for neg stable states", []),
  list_stable_states(negative),
  debug(tscc, "neg stable states done", []),
  findall(C, candidate(C), Candidates),
  length(Candidates, NCandidates),
  maplist(state_to_spec, Candidates, Specs),
  maplist(spec_to_terminal, Specs, TSCCSpecs),
  setup_call_cleanup(
    hide_initial_state(Initial),
    (
      check_several_queries(TSCCSpecs, positive, Results),
      debug(tscc, "queries ~w give ~w", [TSCCSpecs, Results]),
      subtract(Results, [false], TSCCs),
      length(TSCCs, NTSCCs),
      forall(
        member(C, Candidates),
        (
          nth1(N, Candidates, C),
          nth1(N, Results, R),
          nth1(N, Specs, Spec),
          nth1(N, TSCCSpecs, TSCCSpec),
          (
            R == 'true'
          ->
            write_bool_state(C),
            write(' terminal (positive)\n'),
            (
              fail,   % too slow for negative semantics unfortunately
              with_option(
                [boolean_semantics: negative, boolean_trace: no],
                check_ctl_impl(TSCCSpec, all, Result)
              ),
              write(Result),nl,
              Result == true
            ->
              write('*** truly terminal\n')
            ;
              (
                can_reach_stable(Spec)
              ->
                write('positive TSCC contains a (negative) stable state\n')
              ;
                write('contains a complex attractor\n')
              )
            )
          ;
            true
          )
        )
      )
    ),
    restore_initial_state(Initial)
  ).


spec_to_terminal(Spec, (Spec -> 'AG'('EF'(Spec)))).


can_reach_stable(Spec) :-
  findall(
    StableSpec,
    (
      stable_state(Stable),
      state_to_spec(Stable, StableSpec)
    ),
    Stables
  ),
  maplist(reachable_spec(Spec), Stables, Reachables),
  join_with_op(Reachables, '\\/', AnyReachable),
  with_option(
    [boolean_trace: no, boolean_semantics: positive],
    check_ctl_impl(AnyReachable, all, Result)
  ),
  Result == true.


reachable_spec(From, To, (From -> 'EF'(To))).


hide_initial_state(Initial) :-
  enumerate_all_molecules(M),
  maplist(get_initial_state, M, Initial),
  maplist(initial_state:undefined_object, M).


restore_initial_state(Initial) :-
  enumerate_all_molecules(M),
  maplist(set_initial_state, M, Initial).


state_to_spec(State, Spec) :-
  maplist(number_state, State, List),
  maplist(literal_to_spec, List, Specs),
  join_with_op(Specs, '/\\', Spec).


literal_to_spec(A-0, not(A)).

literal_to_spec(A-1, A).


:- dynamic(subsumed_influence/1).


mark_subsumed_influences(BoolSem) :-
  retractall(subsumed_influence(_)),
  mark_subsumed_influences_aux(BoolSem).


mark_subsumed_influences_aux(BoolSem) :-
  item([kind: influence, item: Item]),
  influence(Item, [positive_inputs: PI, negative_inputs: NI, sign: S, output: O]),
  (
    item([kind: influence, item: I2]),
    I2 \= Item,
    \+ subsumed_influence(I2),
    influence(I2, [positive_inputs: PI2, negative_inputs: NI2, sign: S, output: O]),
    subset(PI2, PI),
    (
      BoolSem == positive
    ->
      true
    ;
      subset(NI2, NI)
    )
  ->
    assertz(subsumed_influence(Item))
  ;
    true
  ),
  fail.

mark_subsumed_influences_aux(_).


%! write_clause(+Positive, +Negative)
%
% Write a SAT clause in DIMACS format for the disjunction of all species in
% Positive and of all negations of all species in Negative
%
% @arg Positive   list of species appearing positively in the clause
% @arg Negative   list of species appearing negatively in the clause

% nothing to write for always true clause
write_clause(Positive, Negative) :-
  member(P, Positive),
  memberchk(P, Negative),
  !.

write_clause(Positive, Negative) :-
  maplist(invert, Negative, NegNegative),
  append(Positive, NegNegative, L),
  predsort(abscmp, L, Sorted),
  atomic_list_concat(Sorted, ' ', LineAtom),
  debug(tscc, "writing clause ~w", [LineAtom]),
  count(sat_clauses, _),
  format(infl_property_out_stream, "~w 0~n", [LineAtom]).
