:- module(
  junit, [
    run_junit_tests/0,
    run_junit_tests/1,
    run_tests_and_halt/0,
    run_tests_and_halt/1
  ]).

% :- reexport(library(statistics), [profile/1]).


%  main test runner
run_junit_tests :-
  run_junit_tests(all).


run_junit_tests(Spec) :-
  \+ is_list(Spec),
  Spec \= all,
  !,
  run_junit_tests([Spec]).

run_junit_tests(Spec) :-
  set_prolog_flag(verbose, normal),
  patch_show_coverage,
  nb_setval(seen, 0),
  nb_setval(covered, 0),
  with_output_to(
    string(Coverage),
    (
      (
        Spec == all
      ->
        (
          flag(slow_test, true, true)
        ->
          (run_tests, generate_doc)
        ;
          run_tests
        )
      ;
        our_show_coverage(run_tests(Spec))
      )
    ->
      true
    ;
      % we do not want to fail even if run_tests fails
      true
    )
  ),
  current_prolog_flag(version, Version),
  (
    Version >= 90200
  ->
    split_string(Coverage, "\n", "\r", CovLines),
    forall(
      (
        member(Line, CovLines),
        split_string(Line, "\t ", "\t ", [_File, Clauses, Percent, _Fail]),
        % number of clauses is formated with ~D, i.e. comma for thousands
        split_string(Clauses, ",", "", LClauses),
        atomics_to_string(LClauses, ClausesNoComma),
        number_string(NClauses, ClausesNoComma),
        number_string(NPercent, Percent)
      ),
      (
        Covered is round(NPercent*NClauses/100),
        nb_getval(seen, Seen),
        nb_getval(covered, Cover),
        NSeen is Seen + NClauses,
        NCover is Cover + Covered,
        nb_setval(seen, NSeen),
        nb_setval(covered, NCover)
      )
    ),
    write(Coverage),
    nb_getval(seen, Seen),
    nb_getval(covered, Cover),
    (
      Seen > 0
    ->
      Covered is Cover*100/Seen,
      format('TOTAL coverage~t ~D~64| ~t~1f~72|~n', [Seen, Covered])
    ;
      true
    ),
    open('junit.xml', write, _, [alias(junit)]),
    format(
      junit,
      "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<testsuites>\n", []
    ),
    forall(
      current_test_unit(Unit, _),
      (
        format(junit, "  <testsuite name=\"~w\">\n", [Unit]),
        output_unit_results(Unit),
        format(junit, "  </testsuite>\n", [])
      )
    ),
    format(junit, "</testsuites>\n", []),
    close(junit)
  ;
    true
  ),
  % Now we fail if all did not go right
  (
    current_predicate(plunit:check_for_test_errors/0)
  ->
    plunit:check_for_test_errors
  ;
    current_predicate(plunit:failed/5)
  ->
    % plunit from SWI 9.1.10
    \+ plunit:failed(_, _, _, _, _)
  ;
    \+ plunit:failed(_, _, _, _)
  ).


get_pl_module(Spec, Module) :-
  atom_concat('plunit_', Spec, TestModule),
  module_property(TestModule, file(TestFile)),
  atom_concat(PlFile, 't', TestFile),
  module_property(Module, file(PlFile)).


:- dynamic(our_show_coverage/1).

patch_show_coverage :-
  % try to call the correct version (test_cover/prolog_coverage/right arity) and to patch it if necessary
  current_prolog_flag(version, Version),
  (
    Version < 90200
  ->
    assertz(our_show_coverage(G) :- call(G))
  ;
    use_module(library(prolog_coverage)),
    assertz((our_show_coverage(G) :- coverage(G), show_coverage([roots(['.'])])))
  ).


run_tests_and_halt :-
  version(V1),
  current_prolog_flag(version, V2),
  format("Biocham v~w running on SWI-Prolog ~w~n", [V1, V2]),
  run_tests_and_halt(all).


run_tests_and_halt(Spec) :-
  call_cleanup(
    (
      run_junit_tests(Spec),
      halt(0)
    ),
    halt(1)
  ).


%  scans plunit dynamic predicates and outputs corresponding info to XML
output_unit_results(Unit) :-
  output_passed_results(Unit),
  output_failed_results(Unit).


%  outputs a successful testcase with its time for each plunit:passed/5 entry
output_passed_results(Unit) :-
  forall(
    plunit:passed(Unit, Name, _Line, _Det, Time),
    format(junit, "    <testcase name=\"~w\" time=\"~w\" />\n", [Name, Time])
  ).


%  outputs a failure inside a testcase for each plunit:failed/5 entry
output_failed_results(Unit) :-
  (
    current_predicate(plunit:failed/5)
  ->
    true
  ;
    dynamic(plunit:failed/5),
    plunit:assertz(plunit:failed(A, B, C, D, 0) :- plunit:failed(A, B, C, D))
  ),
  forall(
    plunit:failed(Unit, Name, _Line, Error, Time),
    (
      format(junit, "    <testcase name=\"~w\" time=\"~w\">\n", [Name, Time]),
      format(junit, "      <failure message=\"~w\" />\n", [Error]),
      format(junit, "    </testcase>\n", [])
    )
  ).
