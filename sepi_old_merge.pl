:- module(
  sepi_old_merge,
  [
    % Commands
    search_sepi_merge/3
  ]
).

:- use_module(biocham).
:- use_module(sepi_infos).
:- use_module(sepi_util).

/*  Eliott's merge restriction  */
/*  Optional, used with option(merge_restriction: old)  */

% DEPRECATED
% No header with old merge restriction
% Can't use MAX-SAT solver with old merge restriction

/*  search_sepi_merge  */
/*  search_sepi_merge(+G1, +G2, +Stream)
    +G1: graph 1 [NbVertexe, NbSpecie, EdgesList, Id]
    +G2: graph 2 [NbVertexe, NbSpecie, EdgesList, Id]
    +Stream: where to write clauses */
search_sepi_merge(G1, G2, Stream) :-
  merge_cond(G1, G2, Stream),
  restrict_merge(G1, G2, Stream), !.


merge_cond(G1, G2, Stream) :-
  G1 = [N1, _, E1, Id1],
  G2 = [N2, _, E2, _],
  length(E1, Na1),
  length(E2, Na2),
  forall(
    (
      item([parent: Id1, kind: vertex, item: VertexName1]), 
      not(get_attribute(Id1, VertexName1, kind = transition)),
      item([parent: Id1, kind: vertex, item: VertexName2]), 
      not(get_attribute(Id1, VertexName2, kind = transition)),
      species(VertexName1, X1, Id1), 
      species(VertexName2, X2, Id1), not(X1 = X2)
    ),
    (
      write_merge_cond(X1, X2, N1, N2, Na1, Na2, Stream), 
      write_merge_cond2(X1, X2, N1, N2, Na1, Na2, Stream), 
      merge(X1, X2, N1, N2, Na1, Na2, M1), 
      merge(X2, X1, N1, N2, Na1, Na2, M2), 
      format(Stream, "-~d ~d 0~n-~d ~d 0~n", [M1, M2, M2, M1])
    )
  ).


% write_merge_cond and write_merge_cond2 : writes the equivalance : merge(a,b) <-> exist x such that mu(a) = x and mu(b) = x

write_merge_cond(A, B, N1, N2, Narcs1, Narcs2, Stream) :-
  write_merge_cond(A, B, N1, N2, Narcs1, Narcs2, 0, Stream).

write_merge_cond(A, B, N1, N2, Narcs1, Narcs2, N2_1, Stream) :-
  N2_1 is N2 - 1,
  merge(A, B, N1, N2, Narcs1, Narcs2, X),
  m_ij(A, N2_1, N2, X1),
  m_ij(B, N2_1, N2, X2),
  tseitin(N2_1, A, B, N1, N2, Narcs1, Narcs2, X3),
  format(Stream, "-~d ~d 0~n", [X3, X1]),
  format(Stream, "-~d ~d 0~n", [X3, X2]),
  format(Stream, "~d -~d -~d 0~n", [X, X1, X2]), !.

write_merge_cond(A, B, N1, N2, Narcs1, Narcs2, I, Stream) :-
  merge(A, B, N1, N2, Narcs1, Narcs2, X),
  m_ij(A, I, N2, X1),
  m_ij(B, I, N2, X2),
  tseitin(I, A, B, N1, N2, Narcs1, Narcs2, X3),
  format(Stream, "-~d ~d 0~n", [X3, X1]),
  format(Stream, "-~d ~d 0~n", [X3, X2]),
  format(Stream, "~d -~d -~d 0~n", [X, X1, X2]),
  I1 is I + 1,
  write_merge_cond(A, B, N1, N2, Narcs1, Narcs2, I1, Stream).


write_merge_cond2(A, B, N1, N2, Narcs1, Narcs2, Stream) :-
  merge(A, B, N1, N2, Narcs1, Narcs2, X),
  format(Stream, "-~d ", [X]),
  write_merge_cond2(A, B, N1, N2, Narcs1, Narcs2, 0, Stream).

write_merge_cond2(A, B, N1, N2, Narcs1, Narcs2, N2_1, Stream) :-
  N2_1 is N2 - 1,
  tseitin(N2_1, A, B, N1, N2, Narcs1, Narcs2, X),
  format(Stream, "~d 0~n", [X]), !.

write_merge_cond2(A, B, N1, N2, Narcs1, Narcs2, I, Stream) :-
  tseitin(I, A, B, N1, N2, Narcs1, Narcs2, X),
  format(Stream, "~d ", [X]),
  I1 is I + 1,
  write_merge_cond2(A, B, N1, N2, Narcs1, Narcs2, I1, Stream).


% merge restriction conditions :
restrict_merge(G1, G2, Stream) :-
    ( 
      G1 = [_, Ns1, E1, Id1],
      G2 = [_, _, E2, _],
      length(E1, Na1),
      length(E2, Na2),
      forall(
        (
          item([parent: Id1, kind: vertex, item: VertexName1]),
          item([parent: Id1, kind: vertex, item: VertexName2]),
          not(VertexName1 = VertexName2),
          ( 
            species(VertexName1, X1, Id1),
            X1 < Ns1,
            species(VertexName2, X2, Id1),
            X2 < Ns1
          )
        ;
          ( 
            species(VertexName1, X1, Id1),
            X1 > Ns1,
            species(VertexName2, X2, Id1),
            X2 > Ns1
          )
        ),
        ( 
          ( 
            ( 
              (
                item([parent: Id1, kind: edge, item: (VertexName1 -> React)])
              ; 
                item([parent: Id1, kind: edge, item: (React -> VertexName1)])
              ),
              (
                item([parent: Id1, kind: edge, item: (VertexName2 -> React)])
              ; 
                item([parent: Id1, kind: edge, item: (React -> VertexName2)])
              )
            ) 
          -> 
            true
          )
        ;
          write_restrict_merge(VertexName1, VertexName2, G1, G2, Na1, Na2, Stream)
        ; 
          true
        )
      )
    )
  ; 
    true.


write_restrict_merge(Vertex1, Vertex2, G1, G2, Na1, Na2, Stream) :-
  G1 = [N1, _, _, Id1],
  G2 = [N2, _, _, _],
  species(Vertex1, V1, Id1),
  species(Vertex2, V2, Id1),
  merge(V1, V2, N1, N2, Na1, Na2, Me),
  format(Stream, "-~d ", [Me]),
  forall(
    ( 
      (
        item([parent: Id1, kind: edge, item: (Vertex1 -> React)])
      ;
        item([parent: Id1, kind: edge, item: (React -> Vertex1)])
      ),
      (
        item([parent: Id1, kind: edge, item: (Vertex3 -> React)])
      ;
        item([parent: Id1, kind: edge, item: (React -> Vertex3)])
      ),
      not(Vertex1 = Vertex3)
    ),
    ( 
      species(Vertex3, V3, Id1),
      merge(V1, V3, N1, N2, Na1, Na2, X),
      format(Stream, "~d ", [X])
    )
  ),
  format(Stream, "0~n", []), !.


label_constraints(G1, G2, Stream) :-
  G1 = [_, Ns1, _, Id1],
  G2 = [N2, Ns2, _, Id2],
  forall(
    (
      item([parent: Id1, kind: vertex, item: VertexName]), 
      not(get_attribute(Id1, VertexName, kind = transition)),
      item([parent: Id2, kind: vertex, item: VertexName]), 
      not(get_attribute(Id2, VertexName, kind = transition))
    ),
    (
      species(VertexName, X1, Id1), 
      species(VertexName, X2, Id2), 
      X1 < Ns1, X2 < Ns2, 
      m_ij(X1, X2, N2, X), 
      m_ij(X1, N2, N2, Dummy), 
      format(Stream, "~d ~d 0~n", [X, Dummy])
    )
  ), 
  !.
