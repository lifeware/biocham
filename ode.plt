:- use_module(library(plunit)).

% Only for separate compilation/linting
:- use_module(ode).
:- use_module(reaction_rules).


:- begin_tests(ode, [setup((clear_model, reset_options))]).


test('ode_system', [ODEs == [d(b)/dt = a, d(a)/dt = -a]]) :-
  clear_model,
  command(a => b),
  ode_system,
  all_odes(ODEs).

test(
  'add_reactions_from_ode_system_1',
  [true(Reactions == [a*v/(a+k) for a => b])]
) :-
  clear_model,
  new_ode_system,
  parameter([v=1, k=1]),
  add_ode(da/dt = -v*a/(a+k)),
  add_ode(db/dt = v*a/(a+k)),
  add_ode(dc/dt = 1),
  delete_ode([c]),
  add_reactions_from_ode_system,
  all_items([kind: reaction], Reactions).

test(
  'add_reactions_from_ode_system_2',
  [true(Reactions == [a*v/(a+k) for a => b])]
) :-
  clear_model,
  new_ode_system,
  parameter([v=1, k=1]),
  add_ode(da/dt = -v*a/(a+k)),
  add_ode(db/dt = v*a/(a+k)),
  add_ode(dc/dt = 1),
  delete_ode([c]),
  load_reactions_from_ode_system,
  all_items([kind: reaction], Reactions).

test(
  'add_reactions_from_ode_system_3',
  [true(Reactions == [a*v/(a+k) for a => b])]
) :-
  clear_model,
  new_ode_system,
  add_ode(da/dt = -v*a/(a+k)),
  add_ode(db/dt = v*a/(a+k)),
  add_ode(dc/dt = 1),
  delete_ode([c]),
  add_reactions_from_ode_system,
  all_items([kind: reaction], Reactions).

test(
  'add_reactions_from_ode_system_4',
  [true(Reactions == [a*v/(a+k) for a => b])]
) :-
  clear_model,
  new_ode_system,
  add_ode(da/dt = -v*a/(a+k)),
  add_ode(db/dt = v*a/(a+k)),
  add_ode(dc/dt = 1),
  delete_ode([c]),
  load_reactions_from_ode_system,
  all_items([kind: reaction], Reactions).



test(
  'export_ode_as_latex',
  [true(Lines == [
      '\\begin{align*}', '{b}_0 &= 0\\\\',
      '{a}_0 &= 1\\\\', 'k &= 2\\\\',
      '\\frac{db}{dt} &= a*k\\\\',
      '\\frac{da}{dt} &= - (a*k)\\\\',
      '\\end{align*}'
    ])]
) :-
  clear_model,
  command('MA'(k) for a => b),
  command(parameter(k = 2)),
  command(present(a)),
  export_ode_as_latex('test.tex'),
  list_file_lines('test.tex', Lines),
  delete_file('test.tex').

test(
  'remove_fraction',[]) :-
  remove_fraction([a], d(a)/dt = a/b, d(a)/dt = a),
  remove_fraction([a], d(a)/dt = a/(b+c) + d, d(a)/dt = a + b*d + c*d),
  remove_fraction([a], d(a)/dt = a/(b+c) + -1*d, d(a)/dt = a - b*d - c*d),
  remove_fraction([a], d(a)/dt = a/b + c/d + e, d(a)/dt = a*d + b*c + b*d*e),
  remove_fraction([a], d(a)/dt = a/b + c/d, d(a)/dt = a*d + b*c).

test(
  'normalize_ode1', []) :-
  clear_model,
  with_current_ode_system((
    add_ode(d(x)/dt = (1*(k9*k1*k2) + (-1)*(k9*k3*x1) + (-1)*(k9*k8*x*x/(x+k7)))/k9),
    normalize_ode([d(x)/dt = k1*k2*k9/k9+ - (k3*k9*x1)/k9+ - (k8*k9*x^2)/(k9*(k7+x))], [x])
  )).

test(
  'normalize_ode2', []) :-
  clear_model,
  with_current_ode_system((
    add_ode(d(x)/dt = k7+ -1*k8*x + -1*k9*x/(x+k1)),
    normalize_ode([d(x)/dt=k7/1+ - (k8*x)/1+ - (k9*x)/(k1+x)], [x])
  )).

:- end_tests(ode).
