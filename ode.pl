:- module(
  ode,
  [
    % Grammars
    oderef/1,
    ode/1,
    % Commands
    new_ode_system/0,
    delete_ode_system/1,
    clear_ode_systems/0,
    set_ode_system_name/1,
    list_ode_systems/0,
    select_ode_system/1,
    add_ode/1,
    ode_function/1,
    ode_parameter/1,
    delete_ode/1,
    list_ode/0,
    ode_system/0,
    import_ode/1,
    load_model_from_ode/1,
    add_model_from_ode/1,
    load_reactions_from_ode_system/0,
    load_influences_from_ode_system/0,
    add_reactions_from_ode_system/0,
    add_influences_from_ode_system/0,
    (init)/1,
    remove_fraction/0,
    infer_hidden_molecules/0,
    load_reactions_from_ode/1,
    add_reactions_from_ode/1,
    load_influences_from_ode/1,
    add_influences_from_ode/1,
    export_ode/1,
    export_ode_as_latex/1,
    % Public API
    new_ode_system/1,
    get_current_ode_system/1,
    set_current_ode_system/1,
    set_ode_system_name/2,
    set_ode_initial_value/2,
    set_ode_initial_value/3,
    get_ode_initial_state/2,
    get_ode_initial_state/3,
    ode_parameter_value/2,
    get_ode_function/2,
    add_ode/2,
    import_reactions_from_ode_system/1,
    import_influences_from_ode_system/1,
    with_current_ode_system/1,
    edit_current_ode_system/1,
    infer_hidden_molecules/1,
    all_odes/1,
    ode/2,
    ode/3,
    ode_add_expression_to_molecule/2,
    ode_predicate/1,
    remove_fraction/3,
    normalize_ode/2
  ]).

% Only for separate compilation/linking
:- use_module(arithmetic_rules).
:- use_module(doc). % also for using escape_tex
:- use_module(reaction_rules).
:- use_module(util).
:- use_module(library(yall)).

:- op(1010, fx, init).  % comma is 1000
:- op(1010, fx, par).
:- op(1010, fx, num).
:- op(1010, fx, func).

:- dynamic(flag_influences/0).

:- doc('Biocham can also manipulate ODE systems, import and export ODE systems in XPP syntax, and export them in LaTeX.').

:- doc('\\begin{remark} XPP format has restrictions on names and does not distinguish between upper case and lower case letters. For that reason, and in order to avoid misinterpretations, when an XPP file is imported, all names are read in lower case.
\\end{remark}').

:- doc('The ODE simulation of a Biocham model proceeds by creating an ODE system if there is none, and deleting it after the simulation.').

:- doc('It is worth noting that if there is an ODE system present, it is the current ODE system that is simulated, not the Biocham model.').

:- doc('Biocham can also infer an equivalent reaction network or influence network from the ODEs \\cite{FGS15tcs}. This is useful for importing MatLab models in SBML, and using Biocham analyzers on ODE models.').


:- devdoc('\\section{Grammars}').

:- grammar(ode).


ode(d(X)/dt = E) :-
  object(X),
  arithmetic_expression(E).

ode(DX/dt = E) :-
  name(DX),
  arithmetic_expression(E).


:- grammar(oderef).


oderef(X) :-
  name(X).




:- doc('\\section{Listing ODEs} ').

list_ode :-
  biocham_command,
  doc('
    returns the set of ordinary differential equations
    and initial concentrations (one line per molecule)
    associated to the current model or ODE system.
    \\begin{example}
  '),
  biocham_silent(clear_model),
  biocham(a => b),
  biocham(list_ode),
  doc('
    \\end{example}
  '),
  nb_getval(ode_viewer, Viewer),
  (
    Viewer == inline
  ->
    with_current_ode_system(
      (
        get_current_ode_system(Id),
        list_items([parent: Id, kind: ode]),
        list_items([parent: Id, kind: initial_concentration]),
        list_items([parent: Id, kind: parameter]),
        list_items([parent: Id, kind: function])
      )
    )
  ;
    export_ode_as_latex('ode.tex'),
    util:img_tag('ode.tex')
  ).

:- doc('Exporting ODEs').

export_ode(OutputFile) :-
  biocham_command,
  type(OutputFile, output_file),
  doc('exports the ODE system of the current model.'),
  with_current_ode_system(with_output_to_file(OutputFile, write_ode_as_xpp)).


export_ode_as_latex(OutputFile) :-
  biocham_command,
  type(OutputFile, output_file),
  doc('exports the ODE system of the current model as LaTeX code.'),
  with_current_ode_system(
    with_output_to_file(OutputFile, print_ode_system_as_latex)).

:- doc('\\section{Inferring reaction or influence networks from an ODE file}').

:- doc("
\\begin{example}
\\clearmodel
\\trace{
biocham: parameter(k=10). 
biocham: MA(k) for 2*a + b  => 3*c.
biocham: list_ode.
[0] d(c)/dt=3*k*a^2*b
[1] d(b)/dt= - (k*a^2*b)
[2] d(a)/dt= - (2*k*a^2*b)
biocham: export_ode('test2.ode').
biocham: load_reactions_from_ode('test2.ode').
biocham: list_model.
k*a^2*b for 2*a+b=>3*c.
present(c,0).
present(b,0).
present(a,0).
parameter(k = 10).
biocham: load_influences_from_ode('test2.ode').
biocham: list_model.
3* (k*a^2*b) for b,a -> c.
2* (k*a^2*b) for b,a -< a.
1* (k*a^2*b) for b,a -< b.
present(c,0).
present(b,0).
present(a,0).
parameter(
  k = 10
).
biocham: list_ode.
[0] d(b)/dt= - (k*a^2*b)
[1] d(a)/dt= - (2*k*a^2*b)
[2] d(c)/dt=3*k*a^2*b
}
\\end{example}
").

:- initial(option(import_reactions_with_inhibitors: yes)).

load_reactions_from_ode(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc("
    infers a set of reactions equivalent to an ODE system, and loads it as \\command{load_biocham/1}.
  "),
  option(import_reactions_with_inhibitors, yesno, _,
    'Add inhibitors when inferring reactions (default yes).'),
  retractall(flag_influences),
  models:load_all('ode', InputFile).


%add_ode(InputFile) :-
add_reactions_from_ode(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    infers a set of reactions equivalent to an ODE system, and adds it to the current model as \\command{add_biocham/1}.
  '),
  option(import_reactions_with_inhibitors, yesno, _,
    'Add inhibitors when inferring reactions.'),
  option(infer_hidden_molecules, yesno, _,
    'Run infer_hidden_molecules (1-X => X_m) before loading reactions'),
  retractall(flag_influences),
  models:add_all('ode', InputFile).


load_influences_from_ode(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    infers a set of influences equivalent to an ODE system, and loads it as \\command{load_biocham/1}.
  '),
  (assertz(flag_influences);retractall(flag_influences)),
  models:load_all('ode', InputFile),
  retractall(flag_influences).


%add_ode(InputFile) :-
add_influences_from_ode(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('
    infers a set of influences equivalent to an ODE system, and adds it to the current model as \\command{add_biocham/1}.
  '),
  (assertz(flag_influences);retractall(flag_influences)),
  models:add_all('ode', InputFile),
  retractall(flag_influences).



:- doc('\\section{ODE systems}').

:- initial(option(second_order_closure: no)).

ode_system :-
  biocham_command,
  doc('
    builds the set of ODEs of the current model.
  '),
  option(
      second_order_closure,
      yesno,
      _,
      'Compute (normal) second order moment closure ODEs. Introduce covariance
      variables and functions to visualize plus/minus standard deviation'
  ),
  delete_ode_system('ode_system'),
  new_ode_system,
  set_ode_system_name('ode_system'),
  with_clean(
    [ode:assoc/2],
    (
      ode:compute_ode,
      ode:simplify_ode,
      ode:put_ode_into_system
    )
  ),
  get_current_ode_system(Id),
  forall(
    item([kind: parameter, item: parameter(Parameter = Value)]),
    set_ode_parameters(Id, Parameter = Value)
  ),
  forall(
    item([no_inheritance, kind: function, item: function(Function = Definition)]),
    set_ode_function(Id, Function = Definition)
  ).

import_ode(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('imports a set of ODEs.'),
  read_xpp_as_ode(InputFile, Id),
  debug(ode, "new Id: ~w", [Id]),
  set_current_ode_system(Id).


new_ode_system :-
  biocham_command,
  doc('creates an ODE system.'),
  new_ode_system(Id),
  set_current_ode_system(Id).


delete_ode_system(Name) :-
  biocham_command,
  type(Name, name),
  doc('deletes an ODE system.'),
  delete_items([kind: ode_system, key: Name]).

clear_ode_systems :-
  biocham_command,
  doc('Remove all existing ODE systems.'),
  delete_items([kind: ode_system]).


set_ode_system_name(Name) :-
  biocham_command,
  type(Name, name),
  doc('sets the name of the current ODE system.'),
  get_current_ode_system(Id),
  set_ode_system_name(Id, Name).


list_ode_systems :-
  biocham_command,
  doc('lists the ODE systems of the current model.'),
  list_items([kind: ode_system]).


select_ode_system(Name) :-
  biocham_command,
  type(Name, name),
  doc('selects an ODE system'),
  find_item([kind: ode_system, key: Name, id: Id]),
  set_current_ode_system(Id).



add_ode(ODE) :-
  ( % This creates an ode_system if none exist
    \+( item([kind: ode_system]) )
  ->
    new_ode_system
  ;
    true
  ),
  ode(ODE),
  get_current_ode_system(Id),
  !,
  add_ode(Id, ODE).


add_ode(ODEList) :-
  biocham_command(*),
  type(ODEList, '*'(ode)),
  doc('
    If there is a current ODE system, adds the given set of ODEs to it.
    If there is no current ODE system,
    infers reactions equivalent to the given set of ODEs.
  '),
  edit_current_ode_system((
    \+ (
      member(ODE, ODEList),
      \+ (
        add_ode(ODE)
      )
    )
  )).


delete_ode(ODERefList) :-
  biocham_command(*),
  type(ODERefList, '*'(oderef)),
  doc('removes the given set of ODEs from the current ODE system.'),
  \+ (
    member(Name, ODERefList),
    \+ (
      delete_single_ode(Name)
    )
  ).


init(InitList) :-
  biocham_command(*),
  type(InitList, '*'(name = arithmetic_expression)),
  doc('sets the initial value of a variable in the current set of ODEs.'),
  \+ (
    member(Variable = Value, InitList),
    \+ (
            set_ode_initial_value(Variable, Value)
    )
  ).

:- initial(option(infer_hidden_molecules: no)).

add_reactions_from_ode_system :-
  biocham_command,
  doc('adds a set of reactions equivalent to the current ODE system.'),
  option(import_reactions_with_inhibitors, yesno, _,
    'Add inhibitors when inferring reactions.'),
  get_current_ode_system(Id),
  !,
  retractall(flag_influences),
  (
    get_option(infer_hidden_molecules, IHM),
    IHM = yes
  ->
    infer_hidden_molecules(Id)
  ;
    true
  ),
  import_reactions_from_ode_system(Id),
  delete_item(Id),
  correct_model.

add_reactions_from_ode_system :-
  print_message(warning,'there is no current ode system').



add_influences_from_ode_system :-
  biocham_command,
  doc('adds a set of influences equivalent to the current ODE system.'),
  get_current_ode_system(Id),
  !,
  (assertz(flag_influences);retractall(flag_influences)),
  import_influences_from_ode_system(Id),
  retractall(flag_influences).

add_influences_from_ode_system :-
  print_message(warning,'there is no current ode system').


load_reactions_from_ode_system :-
  biocham_command,
  doc('Replaces the current reactions with those from
    \\command{add_reactions_from_ode_system/0}.'),
  option(import_reactions_with_inhibitors, yesno, _,
    'Add inhibitors when inferring reactions.'),
  option(infer_hidden_molecules, yesno, _,
    'Run infer_hidden_molecules (1-X => X_m) before loading reactions'),
  delete_reactions,
  add_reactions_from_ode_system.


load_influences_from_ode_system :-
  biocham_command,
  doc('Replaces the current reactions with those from
    \\command{add_influences_from_ode_system/0}.'),
  delete_reactions,
  add_influences_from_ode_system.


remove_fraction :-
  biocham_command,
  doc('Remove the rational fraction in the current ODE system by multiplying the derivative
  by the GCD. WARNING: the resulting ODE system is NOT equivalent to the starting one.'),
  new_ode_system(NewId),
  set_ode_system_name(NewId, without_fraction_ode),
  normalize_ode(ListOde, ListVar),
  debug(remove_fraction, "List normalized:~n~w~n",[ListOde]),
  maplist(remove_fraction(ListVar), ListOde, NewListOde),
  debug(remove_fraction, "List without fraction:~n~w~n",[NewListOde]),
  maplist(add_ode(NewId), NewListOde),
  with_current_ode_system((
    get_current_ode_system(Id),
    forall(
      item([parent: Id, kind:parameter, item: Item]),
      add_item([parent: NewId, kind:parameter, item: Item])
    ),
    forall(
      item([parent: Id, kind: function, item: Item]),
      add_item([parent: NewId, kind: function, item: Item])
    ),
    forall(
      item([parent: Id, kind:initial_concentration, item: Item]),
      add_item([parent: NewId, kind:initial_concentration, item: Item])
    )
  )),
  select_ode_system(without_fraction_ode).

% For now a biocham command mainly to debug, remove also ode_system when switching to API
infer_hidden_molecules :-
  biocham_command,
  doc('Tries to infer hidden molecules eliminated by linear invariants.'),
  ode_system,
  with_current_ode_system((
    get_current_ode_system(Id),
    infer_hidden_molecules(Id)
  )).


:- devdoc('\\section{Public API}').


new_ode_system(Id) :-
  add_item([kind: ode_system, key: new_ode_system, id: Id]).


get_current_ode_system(Id) :-
  get_selection(current_model, current_ode_system, [Id]).


set_current_ode_system(Id) :-
  set_selection(current_model, current_ode_system, [Id]).


set_ode_system_name(Id, Name) :-
  replace_item(Id, ode_system, Name, Name).


all_odes(ODEs) :-
  get_current_ode_system(Id),
  all_items([parent: Id, kind: ode], ODEs).


ode(X, E) :-
  get_current_ode_system(Id),
  ode(Id, X, E).


ode(Id, X, E) :-
  (
    var(X)
  ->
    item([parent: Id, kind: ode, item: (d(X)/dt = E)]) % may fail
  ;
    find_item([parent: Id, kind: ode, key: X, item: (d(X)/dt = E)]) % raises an error if fails
  ).


ode_variables(Id, X) :-
  ode(Id, X, _).


ode_variables(Id, X) :-
  item([parent: Id, kind: initial_concentration, item: init(X = _)]),
  \+ item([parent: Id, kind: ode, key: X]).

:- devdoc('Currently, ODE systems have initial state, parameters and functions separated from the current "model" (i.e. Biocham model).
This is a source of confusion. Currently numerical simulation works for an ODE system but ignores its initial state.
Question is to unify what can be unified.').


set_ode_initial_value(Variable, Value) :-
  get_current_ode_system(Id),
  set_ode_initial_value(Id, Variable, Value). 


set_ode_initial_value(Id, Variable, Value) :-
  change_item([parent: Id], initial_concentration, Variable, init(Variable = Value)).



get_ode_initial_state(Variable, State) :- % to be compatible with model's get_initial_state
    get_current_ode_system(Id),
    get_ode_initial_state(Id, Variable, State). 


get_ode_initial_state(Id, Variable, initial_state(_V = Value)) :-
  (
    item([parent: Id, kind: initial_concentration, key: Variable, item: init(Variable = Value)])
  ->
    true
  ;
    Value = 0
  ).



ode_parameter(ParamList) :-
  biocham_command(*),
  type(ParamList, '*'(name = number)),
  doc('sets the value of a parameter in the current set of ODEs.'),
  get_current_ode_system(Id),
  forall(
    member(Variable = Value, ParamList),
    set_ode_parameters(Id, Variable = Value)
  ).


set_ode_parameters(Id, K = V) :-
  change_item([parent: Id], parameter, K, par(K = V)).

set_ode_parameters(Id, (K = V, L)) :-
  set_ode_parameters(Id, K = V),
  set_ode_parameters(Id, L).


ode_parameter_value(K, V) :-
    get_current_ode_system(Id),
    item([parent: Id, kind: parameter, key: K, item: par(K=V)]).


ode_function(FuncList) :-
  biocham_command(*),
  type(FuncList, '*'(name = arithmetic_expression)),
  doc('defines a (parameterless) function in the current set of ODEs.'),
  get_current_ode_system(Id),
  forall(
    member(Function = Definition, FuncList),
    set_ode_function(Id, Function = Definition)
  ).


set_ode_function(Id, Function = Definition) :-
  change_item([parent: Id], function, Function, func(Function = Definition)).

get_ode_function(Function, Definition):-
    get_current_ode_system(Id),
    item([parent: Id, kind: function, key: Function, item:func(Function = Definition)]).


add_ode(Id, ODE) :-
  (
    parse_ode(ODE, X, Item)
  ->
    true
  ;
    throw(error(illformed_ode(ODE)))
  ),
  (
    item([parent: Id, id: ODEId, kind: ode, key: X, item: OldItem])
  ->
    Item =.. [=, Deriv, Expr2],
    OldItem =.. [=, Deriv, Expr1],
    simplify(Expr1+Expr2, Expr12),
    NewItem =.. [=, Deriv, Expr12],
    delete_item(ODEId),
    add_item([parent: Id, kind: ode, key: X, item: NewItem])
    % print_message(warning, already_existing_variable(X))
  ;
    add_item([parent: Id, kind: ode, key: X, item: Item])
  ).


change_ode(Id, ODE) :-
  (
    parse_ode(ODE, X, Item)
  ->
    true
  ;
    throw(error(illformed_ode(ODE)))
  ),
  delete_item([parent: Id, kind: ode, key: X]),
  add_item([parent: Id, kind: ode, key: X, item: Item]).


import_reactions_from_ode_system(Id) :-
  forall(
    item([parent: Id, kind: initial_concentration, item: init(X = E)]),
    set_initial_concentration(X, E)
  ),
  forall(
    item([parent: Id, kind: parameter, item: par(K = V)]),
    set_parameter(K, V)
  ),
  forall(
    item([parent: Id, kind: function, item: func(F = D)]),
    set_macro(function, F, D)
  ),
  check_cleaned(ode:assoc/2),
  enumerate_terms_in_odes(Id),
  add_terms_as_reactions(Id),
  clean(ode:assoc/2).


import_influences_from_ode_system(Id) :-
  forall(
    item([parent: Id, kind: initial_concentration, item: init(X = E)]),
    set_initial_concentration(X, E)
  ),
  forall(
    item([parent: Id, kind: parameter, item: par(K = V)]),
    set_parameter(K, V)
  ),
  forall(
    item([parent: Id, kind: function, item: func(F = D)]),
    set_macro(function, F, D)
  ),
  check_cleaned(ode:assoc/2),
  enumerate_terms_in_odes(Id),
  add_terms_as_influences(Id),
  clean(ode:assoc/2).


ode_predicate(Command) :-
  parse_ode(Command, _, _).


:- devdoc('\\section{Private predicates}').


parse_ode(ODE, X, ODE) :-
  ODE = (d(X)/dt = _),
  !.

parse_ode(ODE, X, Item) :-
  ODE = (DX/dt = E),
  atom_concat(d, X, DX),
  !,
  Item = (d(X)/dt = E).


enumerate_terms_in_odes(Id) :-
  forall(
    ode(Id, X, E),
    enumerate_terms_in_ode(X, E)
  ).


enumerate_terms_in_ode(X, E) :-
  debug(
    ode,
    "Handling ODE ~w",
    [E]
  ),
  forall(
    non_decomposable_term_in_expression(Coefficient, Term, E),
    (
      (
        retract(assoc(Term, Occurrences))
      ->
        true
      ;
        Occurrences = []
      ),
      (
        select((X, OtherCoefficient), Occurrences, OtherOccurrences)
      ->
        NewCoefficient is Coefficient + OtherCoefficient
      ;
        NewCoefficient = Coefficient,
        OtherOccurrences = Occurrences
      ),
      (
        NewCoefficient = 0
      ->
        NewOccurrences = OtherOccurrences
      ;
        NewOccurrences = [(X, NewCoefficient) | OtherOccurrences]
      ),
      debug(
        ode,
        "Getting term ~w with occurrences ~w",
        [Term, NewOccurrences]
      ),
      assertz(assoc(Term, NewOccurrences))
    )
  ).


non_decomposable_term_in_expression(Coefficient, Term, E) :-
  substitute_functions(E, ENoFunc),
  debug(
    ode,
    "Substituted ~w to ~w",
    [E, ENoFunc]
  ),
  additive_normal_form(ENoFunc, ENormal),
  non_decomposable_term_in_additive_normal_form(Coefficient, Term, ENormal).


substitute_functions(E, ENoFunc) :-
  kinetics:eval_kinetics([], [], E, ENoFunc), % with kinetics/4 get some errors on simplification
  !.

substitute_functions(E, E). % case of add_function, there is no current system, but no functions


non_decomposable_term_in_additive_normal_form(Coefficient, Term, A + B) :-
  !,
  (
    non_decomposable_term_in_additive_normal_form(Coefficient, Term, A)
  ;
    non_decomposable_term_in_additive_normal_form(Coefficient, Term, B)
  ).

non_decomposable_term_in_additive_normal_form(Coefficient, Term, Coefficient * Term) :-
  integer(Coefficient),
  !.

non_decomposable_term_in_additive_normal_form(-1, NCoeff * Term, Coefficient *
  Term) :-
    number(Coefficient),
    Coefficient < 0,
    !,
    NCoeff is -Coefficient.

non_decomposable_term_in_additive_normal_form(1, Term, Term).


% infer_hidden_molecules(+Id)
%
% Recursively loop on the given ode system to remove implicit species

infer_hidden_molecules(Id) :-
  repeat,
  (
    ode(Id, _X, Expr),
    hidden_molecules(Expr, IS)
  ->
    format("Correcting: ~w~n", [IS]),
    correct_hidden_molecules(Id, IS),
    fail
  ;
    !
  ).



% correct_hidden_molecules(+Id, +Implicit_Species)
%
% modify the current ODE system to add the implicit species and insert it in the
% derivatives

correct_hidden_molecules(Id, K-X-Y) :-
  % Add the ODE
  ode(Id, X, Expr_X),
  ode(Id, Y, Expr_Y),
  atom_concat(X, Y, XY),
  atom_concat(XY, '_m', XY_m),
  distribute(-1*Expr_X + -1*Expr_Y, Der_Tempo),
  simplify(Der_Tempo, Expr_m),
  add_ode(Id, d(XY_m)/dt = Expr_m),
  % Set up the new initial state
  get_initial_state(X, initial_state(_CX = X0)),
  get_initial_state(Y, initial_state(_CY = Y0)),
  set_initial_state(XY_m, initial_state(_CXY = K-X0-Y0)),
  % And replace the expression by the variable
  forall(
    ode(Id, Var, Deriv),
    (
      delete_single_ode(Var),
      substitute_all(K, X, Y, XY_m, Deriv, NewDeriv), 
      add_ode(Id, d(Var)/dt = NewDeriv)
    )
  ).

correct_hidden_molecules(Id, K-X) :-
  atom_concat(X, '_m', X_m),
  % Replace the variable in the derivative expression
  forall(
    ode(Id, Var, Deriv),
    (
      delete_single_ode(Var),
      substitute_all(K, X, X_m, Deriv, NewDeriv), 
      add_ode(Id, d(Var)/dt = NewDeriv)
    )
  ),
  % Add the ODE
  ode(Id, X, Expr),
  distribute(-1*Expr, Der_Tempo),
  simplify(Der_Tempo, Expr_m),
  add_ode(Id, d(X_m)/dt = Expr_m),
  % Set up the new initial state
  get_initial_state(X, initial_state(_CX = X0)),
  set_initial_state(X_m, initial_state(_CXY = K-X0)).

substitute_all(K, X, Y, XYm, In, Out) :-
  substitute([K-X-Y],   [XYm],    In, E1),
  substitute([-X+K-Y],  [XYm],    E1, E2),
  substitute([-X-Y+K],  [XYm],    E2, E3),
  substitute([V1+K-X-Y],   [V1+XYm],    E3, E4),
  substitute([K+V2-X-Y],   [V2+XYm],    E4, E5),
  substitute([K-X+V3-Y],   [V3+XYm],    E5, E6),
  substitute([V4-X+K-Y],   [V4+XYm],    E6, E7),
  substitute([-X+V5+K-Y],  [V5+XYm],    E7, E8),
  substitute([-X+K+V6-Y],  [V6+XYm],    E8, E9),
  substitute([V7-X-Y+K],   [V7+XYm],    E9, E10),
  substitute([-X+V8-Y+K],  [V8+XYm],    E10, E11),
  substitute([-X-Y+V9+K],  [V9+XYm],    E11, Test),
  (
    In = Test
  ->
    Test = Out
  ;
    substitute_all(K, X, Y, XYm, Test, Out)
  ).

substitute_all(K, X, Xm, In, Out) :-
  substitute([K-X],     [Xm],    In, E1),
  substitute([-X+K],    [Xm],    E1, E2),
  substitute([V1+K-X],  [V1+Xm], E2, E3),
  substitute([K+V2-X],  [V2+Xm], E3, E4),
  substitute([V3-X+K],  [V3+Xm], E4, E5),
  substitute([-X+V4+K], [V4+Xm], E5, Test),
  (
    In = Test
  ->
    Test = Out
  ;
    substitute_all(K, X, Xm, Test, Out)
  ).


% hidden_molecules(+Expression, -Species)
%
% Detect the implicit molecules in an expression
% Note that we search for hidden molecules at the monomial level to avoid false alert with
% simple degradation.

hidden_molecules(Expr-Monom, S) :-
  !,
  (
    hidden_molecules(Expr, S)
  ;
    hidden_molecules(Monom, S)
  ).

hidden_molecules(Expr+Monom, S) :-
  !,
  (
    hidden_molecules(Expr, S)
  ;
    hidden_molecules(Monom, S)
  ).

hidden_molecules(Expression, K-X-Y) :-
  (
    contains_term(K-X-Y, Expression)
  ;
    contains_term(-X+K-Y, Expression)
  ;
    contains_term(-X-Y+K, Expression)
  ),
  is_numeric(K),
  is_molecule_like(X),
  is_molecule_like(Y).

hidden_molecules(Expression, K-X) :-
  (
    contains_term(K-X, Expression)
  ;
    contains_term(-X+K, Expression)
  ),
  format("candidate: ~w~n", [K-X]),
  is_numeric(K),
  writeln(ok),
  is_molecule_like(X).

is_molecule_like(X) :-
  is_molecule(X).
is_molecule_like(K*X) :-
  is_numeric(K),
  is_molecule(X).
is_molecule_like(X*K) :-
  is_numeric(K),
  is_molecule(X).

:- dynamic(reactant/2).


:- dynamic(product/2).


:- dynamic(inhibitor/2).

% add_terms_as_reactions(+Id)
%
% Scan an ODE system to construct the equivalent Biocham model

add_terms_as_reactions(Id) :-
  forall(
    assoc(Term, Occurrences),
    add_term_as_reactions(Id, Term, Occurrences)
  ).


% add_terms_as_reactions(+Id, +Term, +Occurences)
%
% Add one term of an ODE to the Biocham model

add_term_as_reactions(Id, Term, Occurrences) :-
  check_cleaned(ode:reactant/2),
  check_cleaned(ode:product/2),
  check_cleaned(ode:inhibitor/2),
  % Dispatch molecules as products and reactants
  forall(
    member((X, Coefficient), Occurrences),
    (
      Coefficient < 0
    ->
      OppCoefficient is - Coefficient,
      assertz(reactant(X, OppCoefficient))
    ;
      assertz(product(X, Coefficient))
    )
  ),
  % Recognize modifiers (catalysts and inhibitors)
  forall(
    ode_variables(Id, X),
    (
      (
        \+ reactant(X, _),
        partial_has_pos_val(Term, X)
      ->
        assertz(reactant(X, 1)),
        (
          retract(product(X, OldCoefficient))
        ->
          true
        ;
          OldCoefficient = 0
        ),
        NewCoefficient is OldCoefficient + 1,
        assertz(product(X, NewCoefficient))
      ;
        true
      ),
      (
        partial_has_neg_val(Term, X)
      ->
        assertz(inhibitor(X, 1))
      ;
        true
      )
    )
  ),
  % Build the Reactants part
  findall(
    Coefficient * Object,
    reactant(Object, Coefficient),
    Reactants
  ),
  get_option(import_reactions_with_inhibitors, AddInh),
  (
    AddInh = yes
  ->
    findall(
      Object,
      inhibitor(Object, _Coefficient),
      Inhibitors
    )
  ;
    Inhibitors = []
  ),
  % Build the Products part
  findall(
    Coefficient * Object,
    product(Object, Coefficient),
    Products
  ),
  % Build and add the reaction to the model
  % Test if it could be a mass action law
  (
    mass_action_law(Id, Reactants, Term, Rate, Catalysts)
  ->
    add_species(Reactants, Catalysts, Reactants_Cat),
    add_species(Products, Catalysts, Products_Cat),
    reaction(Reaction, [
      kinetics: 'MA'(Rate),
      reactants: Reactants_Cat,
      inhibitors: Inhibitors,
      products: Products_Cat
    ])
  ;
    reaction(Reaction, [
      kinetics: Term,
      reactants: Reactants,
      inhibitors: Inhibitors,
      products: Products
    ])
  ),
  add_item([kind: reaction, item: Reaction]),
  clean(ode:reactant/2),
  clean(ode:product/2),
  clean(ode:inhibitor/2).

%! mass_action_law(+Id, +Reactants, +Kinetic, -Rate, -Catalysts)
%
%

mass_action_raw([], 1).

mass_action_raw([N*S|Tail],S^N*MAT) :-
  mass_action_raw(Tail, MAT).

mass_action_law(Id, Reactants, Kinetic, Rate, Catalysts) :-
  mass_action_raw(Reactants, MAR),
  simplify(Kinetic/MAR, Reminder),
  mass_action_law(Id, Reminder, Rate_raw, Catalysts),
  simplify(Rate_raw, Rate).

mass_action_law(Id, R1*R2, Rate1*Rate2, Catalysts12) :-
  mass_action_law(Id, R1, Rate1, Catalysts1),
  mass_action_law(Id, R2, Rate2, Catalysts2),
  add_species(Catalysts1, Catalysts2, Catalysts12).

mass_action_law(_Id, N, N, []) :-
  number(N).

mass_action_law(_Id, K, K, []) :-
  item([kind:parameter, key:K]).

mass_action_law(Id, Species, 1, [1*Species]) :-
  item([parent:Id, kind:ode, key:Species, item:(d(Species)/dt=_Deriv)]).

add_species([], List, List).
add_species([N1*S|Tail1], List, [N12*S|Tail12]) :-
  (
    member(N2*S, List)
  ->
    delete(List, N2*S, Tail2),
    N12 is N1+N2,
    add_species(Tail1, Tail2, Tail12)
  ;
    N12 is N1,
    add_species(Tail1, List, Tail12)
  ).

:- dynamic(positive_target/2).

:- dynamic(negative_target/2).

:- dynamic(source/1).

:- dynamic(inhibitor/1).


add_terms_as_influences(Id) :-
  \+ (
    assoc(Term, Occurrences),
    \+ (
      add_term_as_influences(Id, Term, Occurrences)
    )
  ).


add_term_as_influences(Id, Term, Occurrences) :-
  check_cleaned(ode:positive_target/2),
  check_cleaned(ode:negative_target/2),
  check_cleaned(ode:source/1),
  check_cleaned(ode:inhibitor/1),
  \+ (
    member((X, Coefficient), Occurrences),
    \+ (
      (
        Coefficient < 0
      ->
        OppCoeff is -Coefficient,
        assertz(negative_target(X,OppCoeff))
      ;
        assertz(positive_target(X,Coefficient))
      )
    )
  ),
  \+ (
    ode_variables(Id, X),
    \+ (
      (
        \+ source(X),
        partial_has_pos_val(Term, X)
      ->
        assertz(source(X))
      ;
        true
      ),
      (
        partial_has_neg_val(Term, X)
      ->
        assertz(inhibitor(X))
      ;
        true
      )
    )
  ),
  findall(
    Object,
    source(Object),
    Sources
  ),
  findall(
    Object,
    inhibitor(Object),
    Inhibitors
  ),
  forall(
          positive_target(Object,Coeff),
          (influence(Influence, [
                         force: (Coeff*Term),
                         positive_inputs: Sources,
                         negative_inputs: Inhibitors,
                         sign: '+',
                         output: Object
                    ]),
           add_item([kind: influence, item: Influence]))),
  forall(
          negative_target(Object,Coeff),
          (influence(Influence, [
                         force: (Coeff*Term),
                         positive_inputs: Sources,
                         negative_inputs: Inhibitors,
                         sign: '-',
                         output: Object
                    ]),
           add_item([kind: influence, item: Influence]))),
  clean(ode:positive_target/2),
  clean(ode:negative_target/2),
  clean(ode:source/1),
  clean(ode:inhibitor/1).


partial_has_pos_val(Term, X) :- % >0
  derivate(Term, X, DTerm),
  (
    always_negative(DTerm)
  ->
    fail
  ;
    true
  ).

partial_has_neg_val(Term, X) :- % <0
  derivate(Term, X, DTerm),
  (
    always_positive(DTerm)
  ->
    fail
  ;
    true
  ).


:- dynamic(assoc/2).


compute_ode :-
  enumerate_molecules(Molecules),
  % get an empty ODE for molecules, even those that are constant
  % TODO transform them into parameters?
  forall(
    member(Molecule, Molecules),
    assertz(assoc(Molecule, 0))
  ),
  compute_ode_for_reactions,
  compute_ode_for_influences.


simplify_ode :-
  \+ (
    retract(assoc(Variable, Expression)),
    \+ (
      simplify(Expression, SimplifiedExpression),
      assertz(assoc(Variable, SimplifiedExpression))
    )
  ).


put_ode_into_system :-
  \+ (
    assoc(X, Expression),
    \+ (
      add_ode(d(X)/dt = Expression),
      get_initial_concentration(X, Concentration),
      set_ode_initial_value(X, Concentration)
    )
  ).


%! delete_single_ode(+Name:name) is det
%
% deletes the ODE associated with variable Name
% necessitates a current ODE system
delete_single_ode(Name) :-
  get_current_ode_system(Id),
  delete_item([parent: Id, kind: ode, key: Name]).


coefficient_species(Coefficient * Species, Coefficient, Species) :-
  !.

coefficient_species(Species, 1, Species).


models:add_file_suffix('ode', add_model_from_ode).

load_model_from_ode(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('Clean the current model and import the .ode/.xpp file then call \\command{add_model_from_ode/1}.'),
  clear_model,
  add_model_from_ode(InputFile).


add_model_from_ode(InputFile) :-
  biocham_command,
  type(InputFile, input_file),
  doc('Import the .ode/.xpp file and infer the corresponding reactions or influences system.'),
  read_xpp(InputFile),
  (
    flag_influences
  ->
    add_influences_from_ode_system
  ;
    add_reactions_from_ode_system
  ),
  get_current_ode_system(Id),
  delete_item(Id).


print_ode_system_as_latex :-
  get_current_ode_system(Id),
  format('\\begin{align*}\n'),
  forall(
	 (item([parent: Id, kind: initial_concentration, item: init(X = E)]), escape_tex(X, Xe), escape_tex(E, Ee)),
	 format('{~w}_0 &= ~w\\\\\n', [Xe, Ee])
	),
  forall(
	 (item([parent: Id, kind: parameter, item: par(K = V)]), escape_tex(K, Ke), escape_tex(V, Ve)),
	 format('~w &= ~w\\\\\n', [Ke, Ve])
  ),
  forall(
	 (item([parent: Id, kind: function, item: func(F = D)]), escape_tex(F, Fe), escape_tex(D, De)),
	 format('~w &= ~w\\\\\n', [Fe, De])
  ),
  forall(
	 (item([parent: Id, kind: ode, item: (d(X)/dt = E)]), escape_tex(X, Xe), escape_tex(E, Ee)),
	 format('\\frac{d~w}{dt} &= ~w\\\\\n', [Xe, Ee])
  ),
  format('\\end{align*}\n').


:- meta_predicate with_current_ode_system(0).


with_current_ode_system(Goal) :-
  (
    get_current_ode_system(_)
  ->
    Goal
  ;
    setup_call_cleanup(
      ode_system,
      Goal,
      (
        get_current_ode_system(Id),
        delete_item(Id)
      )
    )
  ).


:- meta_predicate edit_current_ode_system(0).


edit_current_ode_system(Goal) :-
  (
    get_current_ode_system(_)
  ->
    Goal
  ;
    setup_call_cleanup(
      new_ode_system,
      Goal,
      (
        get_current_ode_system(Id),
        import_reactions_from_ode_system(Id),
        delete_item(Id)
      )
    )
  ).


ode_add_expression_to_molecule(NewExpression, Molecule) :-
  (
    retract(assoc(Molecule, Expression))
  ->
    asserta(assoc(Molecule, Expression + NewExpression))
  ;
    asserta(assoc(Molecule, NewExpression))
  ).


%! remove_fraction(+ListVar, +Expr, -NewExpr)
%
% remove all the fraction in an expression by multiplying all terms by the common
% denominator

remove_fraction(_ListVar, d( X )/dt = Expr, d( X )/dt = NewExpr) :-
  debug(remove_fraction, "~w' = ~w~n", [X, Expr]),
  find_common_denominator(Expr, DenominatorTempo),
  avoid_duplicate(DenominatorTempo, Denominator),
  debug(remove_fraction, "Den -> ~w~n", [Denominator]),
  multiply_each_term(Expr, Denominator, Expr2),
  debug(remove_fraction, "Deriv. -> ~w~n", [Expr2]),
  simplify(Expr2, NewExpr).


%! find_common_denominator(+Expr, +ListVariable, -ListDenominator)
%
% list all the denominator present in Expr, ListVariable is given as context in order to
% be able to skip parameters

find_common_denominator(Expr1 + Expr2, Den12) :-
  !,
  find_common_denominator(Expr1, Den1),
  find_common_denominator(Expr2, Den2),
  ord_union(Den1, Den2, Den12).

find_common_denominator(_Term/Expr, Den) :-
  !,
  forge_denominator(Expr, DenRaw),
  list_to_ord_set(DenRaw, Den).

find_common_denominator(_Term, []).

forge_denominator(Expr*A, [A|Tail]) :- !,
  forge_denominator(Expr, Tail).
forge_denominator(Expr, [Expr]).

avoid_duplicate(L, A) :- avoid_duplicate(L, L, A).
avoid_duplicate([], _List, []).
avoid_duplicate([Head|Tail1], List, [Head|Tail2]) :-
  \+((
    Head = Term^N
  ->
    member(Term^NN, List),
    NN > N
  ;
    member(Head^N, List),
    N > 1
  )),!,
  avoid_duplicate(Tail1, List, Tail2).
avoid_duplicate([_Head|Tail1], List, Tail2) :-
  avoid_duplicate(Tail1, List, Tail2).


%! multiply_each_term(+Expr, +ListDenominator, -NexExpr)
%
%

multiply_each_term(Expr1 + Expr2, Denominator, NewExpr1 + NewExpr2) :-
  !,
  multiply_each_term(Expr1, Denominator, NewExpr1),
  multiply_each_term(Expr2, Denominator, NewExpr2).

multiply_each_term(Expr, Denominator, NewExpr) :-
  !,
  multiply_term(Expr, Denominator, NewExpr).

multiply_term(Num/DenRaw, Denominator, NewNum) :-
  !,
  forge_denominator(DenRaw, Den),
  subtract(Denominator, Den, ToMult),
  mult_by(Num, ToMult, NewNum).

multiply_term(Num, Denominator, NewNum) :-
  mult_by(Num, Denominator, NewNum).

mult_by(Num, [Head|Tail], NNum) :- !,
  mult_by(Num, Tail, NumTempo),
  distribute(NumTempo*Head, NNum).
mult_by(Num, [], Num).

%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ODE NORMALIZATION %%%
%%%%%%%%%%%%%%%%%%%%%%%%%

%! normalize_ode(-Assoc, -ListVar)
%
% Return the normal form of the current ode system as a list: [d( X )/dt = Expr, etc.]

normalize_ode(NormalForm, ListVar) :-
  with_current_ode_system((
    get_current_ode_system(Id),
    findall(X, ode(Id, X, Expr), ListVar),
    findall(
      d( X )/dt = NewExpr,
      (
        ode(Id, X, Expr),
        normalize_expression(Expr, ListVar, NewExpr)
      ),
      NormalForm
    )
  )).

normalize_expression(Expr, ListVar, NewExpr) :-
  distribute(Expr, ExprTempo),
  normalize_expr_sr(ExprTempo, ListVar, NewExpr).

normalize_expr_sr(Expr1 + Expr2, ListVar, NExpr1 + NExpr2) :- !,
  normalize_expr_sr(Expr1, ListVar, NExpr1),
  normalize_expr_sr(Expr2, ListVar, NExpr2).

normalize_expr_sr(Expr1 - Expr2, ListVar, NExpr1 + NExpr2) :- !,
  normalize_expr_sr(Expr1, ListVar, NExpr1),
  distribute(Expr2*(-1), TExpr2),
  normalize_expr_sr(TExpr2, ListVar, NExpr2).

normalize_expr_sr(-Expr, ListVar, NExpr) :- !,
  distribute(Expr*(-1), TExpr),
  normalize_expr_sr(TExpr, ListVar, NExpr).

normalize_expr_sr(Term, ListVar, NTerm) :-
  normalize_term(Term, ListVar, NTerm).


normalize_term(Term, ListVar, Numerator/Denominator) :-
  filter_denominator(Term, NumeratorTempo/DenominatorTempo),
  filter_other(NumeratorTempo, ListVar, OrdNumerator),
  simplify(OrdNumerator, Numerator),
  simplify(DenominatorTempo, Denominator).

%! filter_denominator(Term, Numerator/Denominator)
%

filter_denominator(T1*T2, (Num1*Num2)/(Den1*Den2)) :- !,
  filter_denominator(T1, Num1/Den1),
  filter_denominator(T2, Num2/Den2).

filter_denominator(T1/T2, (Num1*Den2)/(Num2*Den1)) :- !,
  filter_denominator(T1, Num1/Den1),
  filter_denominator(T2, Num2/Den2).

filter_denominator(T1+T2, (Num1*Den2 + Num2*Den1)/(Den1*Den2)) :- !,
  filter_denominator(T1, Num1/Den1),
  filter_denominator(T2, Num2/Den2).

filter_denominator(T1-T2, (Num1*Den2 - Num2*Den1)/(Den1*Den2)) :- !,
  filter_denominator(T1, Num1/Den1),
  filter_denominator(T2, Num2/Den2).

filter_denominator(X^Nr, Num/Den) :-
  (
    number(Nr)
  ->
    normalize_number(Nr, N)
  ;
    parameter_value(Nr, Nt),
    normalize_number(Nt, N)
  ),!,
  (
    N<0
  ->
    Nm is -N,
    (
      Nm =:= 1
    ->
      filter_denominator(X, Den/Num)
    ;
      Num = [],
      Den = [X^Nm]
    )
  ;
    N =:= 1
  ->
    filter_denominator(X, Num/Den)
  ;
    filter_denominator(X, NumT/DenT),
    Num = NumT^N,
    Den = DenT^N
  ).

filter_denominator(X, X/1).


filter_other(Num, LV, Numbers*Parameters*Variables) :-
  filter_other(Num, LV, Numbers, Parameters, Variables).

filter_other(Tail*Head, LV, Numbers*Head, Parameters, Variables) :-
  number(Head),!,
  filter_other(Tail, LV, Numbers, Parameters, Variables).

filter_other(Tail*Head, LV, Numbers, Parameters, Variables*Head) :-
  member(Head, LV),!,
  filter_other(Tail, LV, Numbers, Parameters, Variables).

filter_other(Tail*Head, LV, Numbers, Parameters*Head, Variables) :- !,
  filter_other(Tail, LV, Numbers, Parameters, Variables).

filter_other(Last,_LV,Last,1,1) :-
  number(Last),!.
filter_other(Last,LV,1,1,Last) :-
  member(Last, LV),!.
filter_other(Last,_LV,1,Last,1).


rewrite_term(Numbers,  Parameter_list, Variable_list, Denominator, Term) :-
  (
    multiply_them(Numbers, Rate_list),
    RateTempo is Rate_list
  ->
    Rate = [RateTempo]
  ;
    Rate = []
  ),
  append([Rate, Parameter_list,Variable_list], Stuff_list),
  (
    multiply_them_rev(Stuff_list, Stuff)
  ->
    true
  ;
    Stuff = 1
  ),
  (
    Denominator = []
  ->
    simplify(Stuff, Term)
  ;
    multiply_them(Denominator, Den),
    simplify(Stuff, SimpleStuff),
    Term = SimpleStuff/Den
  ).

multiply_them_rev(List, Term) :-
  reverse(List, RList),
  multiply_them(RList, Term).
multiply_them([Term], Term) :- !.
multiply_them([Head|Tail], Term*Head) :-
  multiply_them(Tail, Term).



