:- use_module(library(plunit)).

:- begin_tests(sepi_neigh_merge, [setup((clear_model, reset_options))]).


has_start(Start, String) :-
  atom_concat(Start, _, String).


count_start(Atom, List, Count) :-
  include(has_start(Atom), List, L),
  length(L, Count).


test('sepi_merge_neigh_fail') :-
  with_output_to(
    atom(Output),
    command('search_reduction("library:examples/sepi/N1.bc", "library:examples/sepi/N3.bc", merge_restriction: neighbours).')
  ),
  Output = 'no sepi found\nNumber of reductions: 0\n'.


test('sepi_merge_neigh_success') :-
  with_output_to(
    atom(Result),
    command('search_reduction("library:examples/sepi/N1.bc", "library:examples/sepi/N7.bc", merge_restriction: neighbours).')
  ),
  atomic_list_concat(Split, '\n', Result),
  count_start('sepi', Split, 1).


%%% TEST FOR THE NOT_SPECIES RESTRICTION
test('sepi_merge_not_species_fail') :-
  with_output_to(
    atom(Output),
    command('search_reduction("library:examples/sepi/N1.bc", "library:examples/sepi/N3.bc", merge_restriction: not_species).')
  ),
  Output = 'no sepi found\nNumber of reductions: 0\n'.


test('sepi_merge_not_species_success') :-
  with_output_to(
    atom(Result),
    command('search_reduction("library:examples/sepi/N1.bc", "library:examples/sepi/N7.bc", merge_restriction: not_species).')
  ),
  atomic_list_concat(Split, '\n', Result),
  count_start('sepi', Split, 1).


:- end_tests(sepi_neigh_merge).
