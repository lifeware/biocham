:- module(
  toplevel,
  [
    % Commands
    option/0,
    quit/0,
    list_options/0,
    prolog/1,
    delete_temporary_files/0,
    keep_temporary_files/0,
    seed/1,
    with_timer/1,
    % Public API
    toplevel/0,
    command/1,
    execute_command/1,
    prompt/1,
    set_prompt/1,
    get_option/2,
    option/4,
    with_option/2,
    set_option/2,
    set_options/1,
    list_model_options/0,
    have_to_delete_temporary_files/0,
    store_default_options/0,
    reset_options/0
  ]).


% linting 
:- use_module(doc).
:- use_module(library(system)).


:- biocham_silent('clear_model').
:- doc('
  The exhaustive list of Biocham commands available at toplevel is described in the rest of this manual.
New reactions and new influences can also be entered directly at toplevel, as a short hand for the commands \\command{add_reaction/1} and \\command{add_influence/1} respectively.
      Some commands (e.g., \\command{numerical_simulation/0} in the example below) can take named options
  as arguments.
').


:- doc('
  All the options can be defined either 
  globally for the whole model with the command
  \\texttt{option(Option: Value, ..., Option: Value)},
       or locally for a single command 
  by adding additional arguments of the form \\texttt{Option: Value}.
  Local options take precedence over global options.
').

:- doc('Caution, the command \\command{clear_model} does not change the current options and there is no command to restore the default options.').

:- devdoc('The command list options should sort them for the sake of readability. A command for restoring options to their default values should be added. Options should be indexed and listed in Biocham reference manual.').

:- devdoc('
le parsing suivant le contexte,

C est défini dans toplevel.pl pour la résolution des commandes, types.pl
pour la conversion des termes dirigée par les grammaires, puis les
différentes définitions de grammaires (essentiellement dans
reaction_rules.pl).

Ce quil faut surtout retenir :
- les commandes variadiques (avec une arité variable, comme
parameter) sont implémentées par des listes passées en dernier
argument du prédicat correspondant,
- les paramètres optionels des commandes ne sont pas passés en
argument des prédicats, ils sont récupérés par le prédicat
option(Name, Type, Value, Doc), qui déclare l option pour la
commande. Je viens d ajouter un prédicat get_option(Name, Value) pour
juste récupérer la valeur d une option sans la (re)déclarer et
with_option(Name: Value, Goal) pour exécuter un but avec une
option (ou une liste d’options) déclarée localement,
- en Prolog, il faut échapper les identificateurs qui commencent
par des majuscules, et il n y a pas de conversion de termes (sauf
si on passe par command/1).

').



:- devdoc('\\section{Commands}').


list_options :-
  biocham_command,
  doc('lists the set of options defined in the current model with their current values (default values below).'),
  list_items([kind: option]).

:- doc('\\begin{example}
\\clearmodel
\\trace{
biocham: list_options.
biocham: option(time:100).
}
\\end{example}').

:- doc('
\\begin{example}
    \\trace{
biocham: a=>b.
biocham: b=>a.
biocham: present(a).
biocham: list_ode.
biocham: option(time:5).
biocham: numerical_simulation(method:ssa). plot.
\\clearmodel
}
\\end{example}').

option :-
  biocham_command,
  doc('sets options globally.
\\begin{example}
    \\trace{
biocham: option(time:20, method:bsimp).
}
     \\end{example}'),
  devdoc('dummy command, never called because set_options need to be able to
    set any option…').


quit :-
  biocham_command,
  doc('quits the interpreter.'),
  halt.


:- devdoc('\\section{Public API}').


toplevel :-
  devdoc('
    executes the toplevel loop.
  '),
  (
    current_prolog_flag(stack_limit, _)
  ->
    % SWI version 7.7.14 and newer, new stack management
    Limit is 2*10**9,
    set_prolog_flag(stack_limit, Limit)
  ;
    set_prolog_stack(local, limit(1*10**9)),
    set_prolog_stack(global, limit(1*10**9))
  ),
  catch(
    use_module(library(readline)),
    _,
    true
  ),
  repeat,
  \+ read_execute_print.


command(Command) :-
  devdoc('
    executes the given command.
    \\argument{Command} can either be an atom that will be parsed using Biocham syntax (in this case, several commands can be given separated by a dot)
    or a term (in this case, capitalized functors should be quoted).
  '),
  begin_command,
  (
      atom(Command)
      ->
          atom_codes(Command, CommandCodes),
          command_codes(CommandCodes)
      ;
      command_term(Command)
  ).


command_codes(CommandCodes):-
    read_term_from_codes(CommandCodes, CommandTerm, [variable_names(VariableNames)]),
    name_variables_and_anonymous(CommandTerm, VariableNames),
    command_term(CommandTerm),
    (
        append(_, Tail, CommandCodes),
        append([46], Tail2, Tail), % atom_codes('.',[46])
        catch(command_codes(Tail2),_,false),
        !
        ;
        true
    ).


command_term(OriginalCommandTerm):-
  (
    OriginalCommandTerm =.. [CommandName, OriginalArgument]
  ->
    list_enumeration(AllArguments, OriginalArgument),
    CommandTerm =.. [CommandName | AllArguments]
  ;
    CommandTerm = OriginalCommandTerm
  ),
  extract_options(CommandTerm, CommandWithoutOptions, Options),
  functor(CommandWithoutOptions, Functor, Arity),
  (
    Functor/Arity = option/0
  ->
    set_options(Options)
  ;
    (
      predicate_info(Functor/Arity, ArgumentTypes, CommandOptions, yes, _)
    ->
      Command0 = CommandWithoutOptions
    ;
      between(1, Arity, Arity0),
      predicate_info(
        Functor/Arity0, ArgumentTypes, CommandOptions, variantargs, _
      )
    ->
      CommandWithoutOptions =.. [Functor | Arguments],
      PrefixLength is Arity0 - 1,
      length(Prefix, PrefixLength),
      append(Prefix, Tail, Arguments),
      append(Prefix, [Tail], NewArguments),
      Command0 =.. [Functor | NewArguments]
    ;
      Arity >= 1,
      predicate_info(Functor/1, ArgumentTypes, CommandOptions, yes, _)
    ->
      CommandWithoutOptions =.. [Functor | Arguments],
      merge_arguments(Arguments, Argument),
      Command0 =.. [Functor, Argument]
    )
  ->
    check_types(Command0, ArgumentTypes, NewCommand),
    % we can have nested "command" calls, so store and restore local options
    findall(
      (Option, Value),
      retract(local_option(Option, Value)),
      PreviousLocalOptions
    ),
    check_options(Options, CommandOptions),
    NewCommand, % call to Biocham
    retractall(local_option(_, _)),
    forall(
      member((Option, Value), PreviousLocalOptions),
      assertz(local_option(Option, Value))
    )
  ;
    reaction_predicate(CommandTerm)
  ->
    command(add_reaction(CommandTerm))
  ;
    influence_predicate(CommandTerm)
  ->
    command(add_influence(CommandTerm))
  ;
    ode_predicate(CommandTerm)
  ->
    command(add_ode(CommandTerm))
  ;
    throw(error(unknown_command(Functor/Arity)))
  ).


execute_command(Command) :-
  devdoc('
    executes the given command and prints an error message if it fails.
    The predicate itself always succeeds.
  '),
  catch(
    (
      command(Command)
    ->
      true
    ;
      throw(error(prolog_failure))
    ),
    Exception,
    print_message(error, Exception)
  ).


:- dynamic(prompt/1).


prompt('biocham: ') :-
  devdoc('unifies the argument with the current prompt.').


set_prompt(NewPrompt) :-
  devdoc('changes the prompt.'),
  retract(prompt(_OldPrompt)),
  asserta(prompt(NewPrompt)).

% warning: Value must not be instanciated
get_option(Name, Value) :-
  (
    local_option(Name, Value)
  ->
    true
  ;
    item([kind: option, key: Name, item: option(Name: Value)])
  ->
    true
  ;
    throw(error(option_required(Name), option))
  ).


option(Name, _Type, Value, _Doc) :-
  devdoc('defines an option for the command and gets its value.'),
  get_option(Name, Value).


:- meta_predicate with_option(+, 0).


with_option(Name: Value, Goal) :-
  setup_call_cleanup(
    assertz(local_option(Name, Value)),
    Goal,
    retract(local_option(Name, Value))
  ).

with_option([], Goal) :-
  Goal.

with_option([Head | Tail], Goal) :-
  with_option(Head, with_option(Tail, Goal)).


set_option(Option, Value) :-
  (
    predicate_property(Head, visible),
    functor(Head, Functor, Arity),
    predicate_info(Functor/Arity, _, CommandOptions, BiochamCommand, _),
    BiochamCommand \= no,
    member(option(Option, Type, _Doc), CommandOptions)
  ->
    check_type_with_error(Type, Value, TypedValue),
    change_item([], option, Option, option(Option: TypedValue))
  ;
    throw(error(unknown_option(Option), set_option))
  ).


set_options(List) :-
  \+ (
    member(Option: Value, List),
    \+ (
      set_option(Option, Value)
    )
  ).


list_model_options :-
  devdoc('
    lists all the options in a loadable syntax
    (auxiliary predicate of list_model).
  '),
  (
    item([no_inheritance, kind: option])
  ->
    write('option(\n'),
    write_successes(
      item([kind: option, item: option(Option: Value)]),
      write(',\n'),
      format('  ~w: ~w', [Option, Value])
    ),
    write('\n).\n')
  ;
    true
  ).


:- devdoc('\\section{Private predicates}').


:- dynamic(local_option/2).


read_execute_print :-
  set_counter(warnings, 0),
  read_command(Command),
  (
    Command = end_of_file
  ->
    nl,
    quit
  ;
    execute_command(Command)
  ).


read_command(Command) :-
  prompt(Prompt),
  read_history('', '', [], Prompt, Command, VariableNames),
  name_variables_and_anonymous(Command, VariableNames).


prolog:message(error(unknown_command(Command))) -->
  ['Unknown command: ~p'-[Command]].

prolog:message(error(prolog_failure)) -->
  ['Prolog failure'].


check_types(Command, ArgumentTypes, NewCommand) :-
  Command =.. [Functor | Arguments],
  findall(
    NewArgument,
    (
      nth0(Index, Arguments, Argument),
      nth0(Index, ArgumentTypes, ArgumentType),
      (
        var(ArgumentType)
      ->
        NewArgument = Argument
      ;
        check_type_with_error(ArgumentType, Argument, NewArgument)
      )
    ),
    NewArguments
  ),
  NewCommand =.. [Functor | NewArguments].


check_options(Options, CommandOptions) :-
  \+ (
    member(Option: UntypedValue, Options),
    \+ (
      (
        member(option(Option, Type, _Doc), CommandOptions)
      ->
        check_type_with_error(Type, UntypedValue, Value),
        (
          local_option(Option, _)
        ->
          throw(error(option_defined_twice(Option), check_options))
        ;
          assertz(local_option(Option, Value))
        )
      ;
        throw(error(option_not_applicable(Option), check_options))
      )
    )
  ).


check_type_with_error(ArgumentType, Argument, NewArgument) :-
  catch(
    (
      check_type(ArgumentType, Argument, NewArgument)
    ->
      true
    ;
      throw(error(failure))
    ),
    error(Type),
    throw(error(invalid_type(Argument, Type)))
  ).

merge_arguments(Arguments, Argument) :-
  maplist(term_to_atom, Arguments, ArgumentAtoms),
  atomic_list_concat(ArgumentAtoms, ',', ArgumentSequence),
  term_to_atom(Argument, ArgumentSequence).


extract_options(Command, CommandWithoutOptions, Options) :-
  Command =.. [Functor | Arguments],
  extract_options_arguments(Arguments, RealArguments, Options),
  CommandWithoutOptions =.. [Functor | RealArguments].


extract_options_arguments([], [], []).

extract_options_arguments(
  [OptionValue | Arguments], RealArguments, [OptionValue | Options]
) :-
  OptionValue = Option : _Value,
  Option \= library,
  !,
  extract_options_arguments(Arguments, RealArguments, Options).

extract_options_arguments(
  [Argument | Arguments], [Argument | RealArguments], Options
) :-
  extract_options_arguments(Arguments, RealArguments, Options).


% Extra commands


:- dynamic(random_seed/1).


seed(Number):-
  biocham_command,
  type(Number,number),
  doc('For the sake of reproducibility of the results with randomized algorithms, sets the seed of the random number generator to some integer. Note however than the results may still differ on different machines.'),
  retractall(random_seed(_)),
  assertz(random_seed(Number)),
  set_random(seed(Number)).


prolog(Term):-
  biocham_command,
  type(Term, name),
  doc('Just for development purposes, calls a Prolog term written between quotes and
    prints the result substitution. E.g. when you need to modify the SWI-prolog flags from
    the toplevel of Biocham: "prolog(\'set_prolog_flag(stack_limit, 2 000 000 000)\')."'),
  read_term_from_atom(Term, Goal, [variable_names(Bindings)]),
  Goal,
  (
    Bindings \= []
  ->
    print(Bindings),
    nl
  ;
    true
  ).


:- dynamic(have_to_delete_temporary_files/0).


have_to_delete_temporary_files.


keep_temporary_files :-
  biocham_command,
  doc('
    Switch to a mode where Biocham commands keep the temporary files they
    generate.
  '),
  retractall(have_to_delete_temporary_files).


delete_temporary_files :-
  biocham_command,
  doc('
    Switch to a mode where Biocham commands delete the temporary files they
    generate. (Default)
  '),
  (
    have_to_delete_temporary_files
  ->
    true
  ;
    assertz(have_to_delete_temporary_files)
  ).


:- dynamic(default_option/2).

store_default_options :-
  retractall(default_option(_, _)),
  all_items([kind: option], Items),
  forall(
    member(option(Option: Value), Items),
    assertz(default_option(Option, Value))
  ).


reset_options :-
  biocham_command,
  doc('Reset all options to their default values.'),
  forall(
    default_option(Option, Value),
    (
      get_option(Option, Current),
      Current == Value
    ->
      true
    ;
      set_option(Option, Value)
    )
  ).

with_timer(Goal, Time) :-
  biocham_command,
  doc('Execute a given Goal in a given Time (in ms). If no second argument is given, it displays the time taken to execute the command in the standard output.'),
  get_time(T0),
  command(Goal),
  get_time(T1),
  Time is (T1-T0)*1000.

with_timer(Goal) :-
  biocham_command,
  with_timer(Goal, Time),
  format("The previous command takes ~g ms~n", Time).

:- doc('\\begin{example}
\\trace{
biocham: with_timer(a+b => c).
biocham_silent(clear_model).
}
\\end{example}').
