:- module(
  parameters,
  [
    % Commands
    parameter/1,
    list_parameters/0,
    delete_parameter/1,
    % Public API
    parameter_value/2,
    list_model_parameters/0,
    set_parameter/2,
    set_parameters/1,
    copy_parameters/0,
    paste_parameters/0
  ]
).

% Only for separate compilation/linking
:- use_module(doc).

:- devdoc('\\section{Commands}').


parameter(ParameterList) :-
  biocham_command(*),
  %type(ParameterList, '*'(parameter_name = number)),
  %FF 
  type(ParameterList, '*'(parameter_name = arithmetic_expression)),
  doc('sets parameter values to numbers or to the result of ground arithmetic expressions (not involving user defined functions or other parameters). If a parameter has the same name as a \emph{location}, it will be used to define the volume of this compartment.'),
  forall(
    member(Parameter = Value, ParameterList),
    set_parameter(Parameter, Value)
  ).


list_parameters :-
  biocham_command,
  doc('shows the values of all known parameters.'),
  list_model_parameters.
  % list_items([kind: parameter]).


delete_parameter(ParameterSet) :-
  biocham_command(*),
  type(ParameterSet, '*'(parameter_name)),
  doc('deletes some parameters'),
  single_model(ModelId),
  forall(
    member(Parameter, ParameterSet),
    (
      delete_item([kind: parameter, key: Parameter]),
      retractall(identifier_kind(ModelId, Parameter, _))
    )
  ).


:- devdoc('\\section{Public API}').


parameter_value(Parameter, Value) :-
  devdoc('
    succeeds for every \\argument{Parameter}=\\argument{Value} pair.
  '),
  item([kind: parameter, key: Parameter, item: parameter(Parameter = Value)]).


list_model_parameters :-
  devdoc('
    lists all the parameters in a loadable syntax
    (auxiliary predicate of list_model).
  '),
  (
    item([no_inheritance, kind: parameter])
  ->
    write('parameter(\n'),
    assertz(first),
    write_successes(
      item([
        no_inheritance, kind: parameter, item: parameter(Parameter = Value)
      ]),
      write(',\n'),
      format('  ~w = ~w', [Parameter, Value])
    ),
    write('\n).\n')
  ;
    true
  ).

% parameter values can now be defined by a closed arithmetic expression (not involving other parameters, functions nor molecular concentrations)
set_parameter(Parameter, Value) :-
    check_identifier_kind(Parameter, parameter),
    Val is Value, 
  change_item([], parameter, Parameter, parameter(Parameter = Val)),
  single_model(ModelId),
  set_kind(ModelId, parameter, Parameter).


set_parameters([]).

set_parameters([Parameter = Value | Tail]) :-
  set_parameter(Parameter, Value),
  set_parameters(Tail).

:- dynamic(saved_parameter/1).

copy_parameters :-
  forall(
    item([kind: parameter, item:P]),
    assertz(saved_parameter(P))
  ).

paste_parameters :-
  forall(
    saved_parameter(parameter(P=V)),
    set_parameter(P, V)
  ),
  retractall(saved_parameter).
