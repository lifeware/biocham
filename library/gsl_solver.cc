#include <math.h>
#include "gsl_solver.hh"

void *
xcalloc(size_t count, size_t size)
{
    void *result = calloc(count, size);
    if (!result) {
        fprintf(stderr, "Memory failure\n");
        exit(EXIT_FAILURE);
    }
    return result;
}

void
init_gsl_solver(
    struct gsl_solver *solver, const struct gsl_solver_config *config)
{
    const gsl_odeiv2_step_type *method = config->method;
    size_t variable_count = config->variable_count;
    double *p = (double *) xcalloc(config->parameter_count, sizeof(double));
    gsl_odeiv2_system sys = {functions, jacobian, variable_count, p};
    solver->sys = sys;
    gsl_odeiv2_step *s = gsl_odeiv2_step_alloc(method, variable_count);
    double delta = config->time_final - config->time_initial;
    gsl_odeiv2_driver *d = gsl_odeiv2_driver_alloc_y_new(
        &solver->sys, method, fmax(config->initial_step_size, delta * config->minimum_step_size),
        config->error_epsilon_absolute,
        config->error_epsilon_relative);
    double maximum_step_size = delta * config->maximum_step_size;
    if (maximum_step_size <= 0) {
      maximum_step_size = delta;
    }
    gsl_odeiv2_step_set_driver(s, d);
    gsl_odeiv2_driver_set_nmax(d, 1);
    gsl_odeiv2_driver_set_hmax(d, maximum_step_size);
    gsl_odeiv2_driver_set_hmin(d, delta * config->minimum_step_size);
    solver->config = config;
    solver->p = p;
    solver->s = s;
    solver->d = d;
}

void
free_gsl_solver(struct gsl_solver *solver)
{
    gsl_odeiv2_driver_free(solver->d);
    gsl_odeiv2_step_free(solver->s);
    free(solver->p);
}

void
init_gsl_solver_state(
    struct gsl_solver_state *state, struct gsl_solver *solver)
{
    int i;
    const struct gsl_solver_config *config = solver->config;
    double *x = (double *) xcalloc(config->variable_count, sizeof(double));
    bool *e = (bool *) xcalloc(config->event_count, sizeof(bool));
    state->solver = solver;
    state->t = config->time_initial;
    for (i = 0; i < config->event_count; i++) {
        e[i] = false;
    }
    initial_parameter_values(solver->p);
    initial_values(x, solver->p);
    state->x = x;
    state->e = e;
}

void
free_gsl_solver_state(struct gsl_solver_state *state)
{
    free(state->x);
    free(state->e);
}

bool
gsl_solver_step(struct gsl_solver_state *state)
{
    int status;
    double t_upper;
    double *x = state->x;
    double next_time;
    double delta_t;
    const struct gsl_solver *solver = state->solver;
    const struct gsl_solver_config *config = solver->config;
    // fprintf(stderr, "starting step\n");
    if (state->t >= config->time_final) {
        return false;
    }
    // hmax not enough to ensure steps smaller than hmax
    delta_t = (config->time_final - config->time_initial)/config->steps;
    next_time = ceil((state->t -  config->time_initial)/delta_t) * delta_t + config->time_initial;
    t_upper = fmin(next_time, state->t + solver->d->hmax);
    events(state, &t_upper);
    if (t_upper < state->t + config->initial_step_size) {
        t_upper = state->t + config->initial_step_size;
    } else if (t_upper > config->time_final) {
        t_upper = config->time_final;
    }
    // fprintf(stderr, "%.17g - %g -> ", state->t, t_upper);
    // We run only one single step, so we call evolve directly
    status = gsl_odeiv2_evolve_apply(solver->d->e, solver->d->c, solver->d->s, solver->d->sys, &state->t, t_upper, &solver->d->h, x);
    if (solver->d->h > solver->d->hmax) {
            solver->d->h = solver->d->hmax;
    }
    if (solver->d->h < solver->d->hmin) {
            solver->d->h = solver->d->hmin;
    }
    // fprintf(stderr, "%.17g\n", state->t);
    // fprintf(stderr, "h: %f, hmin: %f, hmax: %f\n", solver->d->h, solver->d->hmin, solver->d->hmax);
    if (status != GSL_SUCCESS) {
        fprintf(stderr, "error %s\n", gsl_strerror(status));
        exit(EXIT_FAILURE);
    }
    return true;
}
