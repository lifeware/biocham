#ifndef _gsl_solver_h_
#define _gsl_solver_h_

#include <stdbool.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>

#include "gsl_types.hh"


struct gsl_solver_config {
    size_t variable_count;
    int parameter_count;
    int event_count;
    const gsl_odeiv2_step_type *method;
    double initial_step_size;
    double maximum_step_size;
    double minimum_step_size;
    double error_epsilon_absolute;
    double error_epsilon_relative;
    double time_initial;
    double time_final;
    int steps;
};


struct gsl_solver {
    const struct gsl_solver_config *config;
    double *p;
    gsl_odeiv2_system sys;
    gsl_odeiv2_step *s;
    gsl_odeiv2_driver *d;
};

struct gsl_solver_state {
    const struct gsl_solver *solver;
    double t;
    double *x;
    bool *e;
};


int
functions(double t, const double x[], double f[], void *data);


int
jacobian(double t, const double x[], double *dfdx, double dfdt[], void *data);


void
events(struct gsl_solver_state *state, double* t_upper);

void
initial_parameter_values(double p[]);


void
initial_values(double x[], double p[]);


void
init_gsl_solver(
    struct gsl_solver *solver, const struct gsl_solver_config *config);


void
free_gsl_solver(struct gsl_solver *solver);


void
init_gsl_solver_state(
    struct gsl_solver_state *state, struct gsl_solver *solver);


void
free_gsl_solver_state(struct gsl_solver_state *state);


bool
gsl_solver_step(struct gsl_solver_state *state);

void
no_filter(Table *table);

void
only_extrema(Table *table);

#endif /* _gsl_solver_h_ */
