#!/bin/bash
set -e
i="$1"
if [ "$i" = "" ]; then
    i=1
fi
while true; do
    filename=`printf BIOMD%.10d $i`
    url="https://www.ebi.ac.uk/biomodels-main/download?mid=$filename"
    if ! curl $url >$filename.xml || grep -q '<!DOCTYPE' $filename.xml; then
        rm -f $filename.xml
        break
    fi
    ((i++))
done
